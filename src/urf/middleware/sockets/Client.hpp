#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <atomic>
#include <condition_variable>
#include <chrono>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <thread>
#include <string>

#include "urf/middleware/messages/Message.hpp"
#include "urf/middleware/sockets/ISocket.hpp"
#include "urf/middleware/sockets/Server.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class URF_MIDDLEWARE_EXPORT Client : public ISocket {
 public:
    Client() = delete;
    explicit Client(const std::string& socket);
    Client(const Client&) = delete;
    Client(Client&&) = delete;
    ~Client() override;

    bool open() override;
    bool close() override;
    bool isOpen() override;

    std::optional<messages::Message> pull() override;
    std::optional<messages::Message> pull(const std::chrono::milliseconds& timeout) override;

    bool push(const messages::Message& message, bool requiresAck = true) override;
    bool onPush(const std::function<void(const messages::Message&)>&) override;
    
    std::vector<messages::Message> request(const messages::Message& message) override;
    std::vector<messages::Message> request(const messages::Message& message, const std::chrono::milliseconds& timeout) override;

    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message) override;
    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout) override;

    std::optional<messages::Message> receiveRequest() override;
    std::optional<messages::Message> receiveRequest(const std::chrono::milliseconds& timeout) override;
    bool respond(const messages::Message& message) override;

    bool subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate = std::chrono::milliseconds(0)) override;
    uint32_t subscriptionsCount(const std::string& topic) override;
    bool unsubscribe(const std::string& topic) override;

    bool keepUpdateHistory(bool value) override;
    std::optional<messages::Message> receiveUpdate(const std::string& topic) override;
    std::optional<messages::Message> receiveUpdate(const std::string& topic, const std::chrono::milliseconds& timeout) override;
    bool onUpdate(const std::string& topic, const std::function<void(const std::string& topic, const messages::Message&)>&) override;

    bool publish(const std::string& topic, const messages::Message& message, bool requiresAck = true) override;
    std::vector<std::string> availablePartnerTopics() override;

    bool addTopic(const std::string& topic) override;
    bool removeTopic(const std::string& topic) override;

   bool automaticallyDeserializeBody(bool value) override;

 private:
    friend class Server;

    class ClientImpl;
    std::unique_ptr<ClientImpl> impl_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
