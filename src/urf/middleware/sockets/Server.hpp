#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <atomic>
#include <condition_variable>
#include <chrono>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <string>
#include <thread>
#include <vector>

#include "urf/middleware/sockets/ISocket.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class URF_MIDDLEWARE_EXPORT Client;
class URF_MIDDLEWARE_EXPORT Server : public ISocket {
 public:
    Server() = delete;
    explicit Server(const std::string& serviceName, const std::string& server);
    explicit Server(const std::string& serviceName, const std::vector<std::string>& serversList);
    Server(const Server&) = delete;
    Server(Server&&) = delete;
    ~Server() override;

    bool open() override;
    bool close() override;
    bool isOpen() override;

    size_t connectedClientsCount();
    std::string serviceName() const;

    std::optional<messages::Message> pull() override;
    std::optional<messages::Message> pull(const std::chrono::milliseconds& timeout) override;

    bool push(const messages::Message& message, bool requiresAck = true) override;
    bool onPush(const std::function<void(const messages::Message&)>&) override;

    std::vector<messages::Message> request(const messages::Message& message) override;
    std::vector<messages::Message> request(const messages::Message& message, const std::chrono::milliseconds& timeout) override;

    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message) override;
    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout) override;

    std::optional<messages::Message> receiveRequest() override;
    std::optional<messages::Message> receiveRequest(const std::chrono::milliseconds& timeout) override;

    bool respond(const messages::Message& message) override;

    bool subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate = std::chrono::milliseconds(0)) override;
    uint32_t subscriptionsCount(const std::string& topic) override;
    bool unsubscribe(const std::string& topic) override;

    bool keepUpdateHistory(bool value) override;
    std::optional<messages::Message> receiveUpdate(const std::string& topic) override;
    std::optional<messages::Message> receiveUpdate(const std::string& topic, const std::chrono::milliseconds& timeout) override;
    bool onUpdate(const std::string& topic, const std::function<void(const std::string& topic, const messages::Message&)>&) override;

    bool publish(const std::string& topic, const messages::Message& message, bool requiresAck = true) override;
    std::vector<std::string> availablePartnerTopics() override;

    bool addTopic(const std::string& topic) override;
    bool removeTopic(const std::string& topic) override;

    bool automaticallyDeserializeBody(bool value) override;

    bool serviceMetadata(const nlohmann::json& metadata);
    nlohmann::json serviceMetadata() const;

 private:
    friend class Client;

    class ServerImpl;
    std::unique_ptr<ServerImpl> impl_;
};

}  // namespace socket
}  // namespace middleware
}  // namespace urf
