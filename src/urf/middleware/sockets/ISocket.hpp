#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <condition_variable>
#include <chrono>
#include <future>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <string>

#include <urf/common/threading/ThreadPool.hpp>

#include "urf/middleware/messages/Message.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class URF_MIDDLEWARE_EXPORT ISocket {
 public:
    virtual ~ISocket() = default;

    virtual bool open() = 0;
    virtual bool close() = 0;
    virtual bool isOpen() = 0;

    virtual std::optional<messages::Message> pull() = 0;
    virtual std::optional<messages::Message> pull(const std::chrono::milliseconds& timeout) = 0;

    virtual bool push(const messages::Message& message, bool requiresAck = true) = 0;
    virtual bool onPush(const std::function<void(const messages::Message&)>&) = 0;

    virtual std::vector<messages::Message> request(const messages::Message& message) = 0;
    virtual std::vector<messages::Message> request(const messages::Message& message, const std::chrono::milliseconds& timeout) = 0;

    virtual std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message) = 0;
    virtual std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout) = 0;

    virtual std::optional<messages::Message> receiveRequest() = 0;
    virtual std::optional<messages::Message> receiveRequest(const std::chrono::milliseconds& timeout) = 0;

    virtual bool respond(const messages::Message& message) = 0;

    virtual bool subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate) = 0;
    virtual uint32_t subscriptionsCount(const std::string& topic) = 0;
    virtual bool unsubscribe(const std::string& topic) = 0;

    virtual bool keepUpdateHistory(bool value) = 0;
    virtual std::optional<messages::Message> receiveUpdate(const std::string& topic) = 0;
    virtual std::optional<messages::Message> receiveUpdate(const std::string& topic, const std::chrono::milliseconds& timeout) = 0;
    virtual bool onUpdate(const std::string& topic, const std::function<void(const std::string& topic, const messages::Message&)>&) = 0;

    virtual bool publish(const std::string& topic, const messages::Message& message, bool requiresAck = true) = 0;
    virtual std::vector<std::string> availablePartnerTopics() = 0;

    virtual bool addTopic(const std::string& topic) = 0;
    virtual bool removeTopic(const std::string& topic) = 0;

    virtual bool automaticallyDeserializeBody(bool value) = 0;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
