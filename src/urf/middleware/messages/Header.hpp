#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <bitset>
#include <cstring>
#include <optional>
#include <vector>

#include "urf/middleware/messages/ISerializable.hpp"


namespace urf {
namespace middleware {
namespace messages {

class URF_MIDDLEWARE_EXPORT Header {
 public:
    Header();
    Header(uint32_t length, uint8_t writerId, uint64_t timestamp = 0,
        bool isCompressed = false, bool requiresAck = true);
    Header(const Header&) = default;
    Header(Header&&) = default;
    ~Header() = default;

    bool isCompressed() const;
    bool requiresAck() const;
    uint32_t length() const;
    uint8_t writerId() const;
    uint64_t timestamp() const;
    uint64_t timestampReceived() const;
    uint8_t version() const;
    uint64_t delay() const;
    bool isValid() const;

    void isCompressed(bool compressed);
    void requiresAck(bool requiresAck);
    void length(uint32_t length);
    void writerId(uint8_t writerId);
    void timestamp(uint64_t timestamp);
    void timestampReceived(uint64_t timestamp);
    void delay(uint64_t delay);

    std::vector<uint8_t> serialize();
    std::ostream& serialize(std::ostream&);
    bool deserialize();
    bool deserialize(const std::vector<uint8_t>& buffer);
    bool deserialize(const uint8_t* data, size_t length);

    bool setBytes(std::vector<uint8_t>&& bytes);

    Header& operator=(const Header&) = default;
    Header& operator=(Header&&) = default;

    inline static const uint32_t size = 14;

 private:
    template <typename T>
    inline void unpack(const std::vector<uint8_t>& src, int index, T& data) const {
        std::memcpy(&data, src.data()+index, sizeof(T));
    }

    uint8_t calculateChecksum() const;

 private:
    std::vector<uint8_t> bytes_;
    uint64_t timestampReceived_;
    uint64_t delay_;


};

}  // namespace messages
}  // namespace middleware
}  // namespace urf
