#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <optional>
#include <vector>

#include <nlohmann/json.hpp>

#include "urf/middleware/messages/ISerializable.hpp"
#include "urf/middleware/messages/Header.hpp"

namespace urf {
namespace middleware {
namespace messages {

class URF_MIDDLEWARE_EXPORT Body : public ISerializable {
 public:
    Body();
    explicit Body(const nlohmann::json&);
    Body(const Body&) = default;
    Body(Body&&) = default;
    ~Body() override = default;

    nlohmann::json& data();
    const nlohmann::json& data() const;

    std::vector<uint8_t> serialize() const override;
    std::ostream& serialize(std::ostream& os) const override;
    bool deserialize() override;
    bool deserialize(const std::vector<uint8_t>& buffer) override;
    bool deserialize(const uint8_t* data, size_t length);

    Header getHeader() const;

    template <typename T>
    T get() const;

    Body& operator=(const Body&) = default;
    Body& operator=(Body&&) = default;

    nlohmann::json& operator[](const std::string&);
    const nlohmann::json& operator[](const std::string&) const;

    nlohmann::json& operator[](size_t);
    const nlohmann::json& operator[](size_t) const;

    bool setBytes(std::vector<uint8_t>&& bytes) override;

    friend std::ostream& operator<<(std::ostream& os, const Body& b);

 private:
    nlohmann::json data_;
    std::optional<std::vector<uint8_t>> bytes_;
};

template <typename T>
T Body::get() const {
    return data_.get<T>();
}

}  // namespace messages
}  // namespace middleware
}  // namespace urf
