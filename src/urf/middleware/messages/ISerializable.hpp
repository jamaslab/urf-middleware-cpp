#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <vector>
#include <ostream>

namespace urf {
namespace middleware {
namespace messages {

class URF_MIDDLEWARE_EXPORT ISerializable {
 public:
    virtual ~ISerializable() = default;

    virtual std::vector<uint8_t> serialize() const = 0;
    virtual std::ostream& serialize(std::ostream&) const = 0;
    virtual bool deserialize() = 0;
    virtual bool deserialize(const std::vector<uint8_t>& buffer) = 0;

    virtual bool setBytes(std::vector<uint8_t>&& bytes) = 0;
};

}  // namespace messages
}  // namespace middleware
}  // namespace urf
