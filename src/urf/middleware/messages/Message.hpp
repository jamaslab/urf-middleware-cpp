#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <cstdint>

#include "urf/middleware/messages/ISerializable.hpp"
#include "urf/middleware/messages/Body.hpp"
#include "urf/middleware/messages/Header.hpp"

namespace urf {
namespace middleware {
namespace messages {

class URF_MIDDLEWARE_EXPORT Message : public ISerializable {
 public:
    Message();
    explicit Message(const nlohmann::json&);
    Message(const Body& body, const Header& header);
    Message(Body&& body, Header&& header);
    ~Message() override = default;

    Body& body();
    Header& header();

    const Body& body() const;
    const Header& header() const;

    void body(const Body& body);
    void body(Body&& body);
    void header(const Header& body);
    void header(Header&& body);

    std::vector<uint8_t> serialize() const override;
    std::ostream& serialize(std::ostream& os) const override;
    bool deserialize() override;
    bool deserialize(const std::vector<uint8_t>& buffer) override;

    template <typename T>
    T get() const;

    nlohmann::json& operator[](const std::string&);
    const nlohmann::json& operator[](const std::string&) const;

    nlohmann::json& operator[](size_t);
    const nlohmann::json& operator[](size_t) const;

    bool setBytes(std::vector<uint8_t>&& bytes) override;

 private:
    Body body_;
    Header header_;

    bool customHeader_;

    std::optional<std::vector<uint8_t>> bytes_;
};

template <typename T>
T Message::get() const {
    return body_.get<T>();
}

}  // namespace messages
}  // namespace middleware
}  // namespace urf
