#pragma once

#if defined(_WIN32) || defined(_WIN64)
    #include "urf/middleware/urf_middleware_export.h"
#else
    #define URF_MIDDLEWARE_EXPORT
#endif

#include <nlohmann/json.hpp>

#include <urf/common/containers/SharedObject.hpp>

namespace urf {
namespace middleware {
namespace services {

class URF_MIDDLEWARE_EXPORT LocalServices {
 public:
    LocalServices();
    ~LocalServices() = default;

    bool registerService(const std::string& name, const nlohmann::json& details = nlohmann::json());
    bool unregisterService(const std::string& name);

    bool refreshService(const std::string& name);

    nlohmann::json getServices();

 private:
    common::containers::SharedObject obj_;
};

}  // namespace services
}  // namespace middleware
}  // namespace urf
