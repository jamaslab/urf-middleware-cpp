#include "urf/middleware/services/LocalServices.hpp"

#include <chrono>
#include <iostream>

#define BUF_SIZE 65000

namespace urf {
namespace middleware {
namespace services {

LocalServices::LocalServices() :
    obj_("LocalServices", BUF_SIZE) { }

bool LocalServices::registerService(const std::string& name, const nlohmann::json& details) {
    if (!obj_.lock()) {
        return false;
    }

    auto str = obj_.read();
    nlohmann::json jsonObj;
    try {
        jsonObj = nlohmann::json::parse(str);
    } catch (const std::exception&) { }

    if (jsonObj.find(name) != jsonObj.end()) {
        jsonObj.erase(name);
    }

    jsonObj[name] = details;
    jsonObj[name]["lastUpdate"] = std::chrono::system_clock::now().time_since_epoch().count();

    str = jsonObj.dump();

    obj_.write(str);
    obj_.unlock();
    return true;
}

bool LocalServices::unregisterService(const std::string& name) {
    if (!obj_.lock()) {
        return false;
    }

    auto str = obj_.read();
    nlohmann::json jsonObj;
    try {
        jsonObj = nlohmann::json::parse(str);
    } catch (const std::exception&) {
        obj_.unlock();
        return false;
    }

    if (jsonObj.find(name) == jsonObj.end()) {
        obj_.unlock();
        return false;
    }

    jsonObj.erase(name);
    obj_.write(jsonObj.dump());
    obj_.unlock();
    return true;
}

bool LocalServices::refreshService(const std::string& name) {
    if (!obj_.lock()) {
        return false;
    }

    auto str = obj_.read();
    nlohmann::json jsonObj;
    try {
        jsonObj = nlohmann::json::parse(str);
    } catch (const std::exception&) {
        obj_.unlock();
        return false;
    }

    if (jsonObj.find(name) == jsonObj.end()) {
        obj_.unlock();
        return false;
    }

    jsonObj[name]["lastUpdate"] = std::chrono::system_clock::now().time_since_epoch().count();

    str = jsonObj.dump();

    obj_.write(str);
    obj_.unlock();
    return true;
}

nlohmann::json LocalServices::getServices() {
    if (!obj_.lock()) {
        return false;
    }

    auto str = obj_.read();
    nlohmann::json jsonObj;
    try {
        jsonObj = nlohmann::json::parse(str);
    } catch (const std::exception&) { }
    obj_.unlock();
    return jsonObj;
}

}  // namespace services
}  // namespace middleware
}  // namespace urf
