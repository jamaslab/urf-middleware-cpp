#include "common/VectorStreambuf.hpp"

#include <algorithm>
#include <cstring>

namespace urf {
namespace middleware {

VectorStreambuf::VectorStreambuf(size_t initialSize)
    : buffer_()
    , putOffPos_(0)
    , getOffPos_(0) {
    if (initialSize != 0) {
        reserve(initialSize);
    }
}

VectorStreambuf::VectorStreambuf(const std::vector<uint8_t>& buffer)
    : buffer_(buffer)
    , putOffPos_(buffer_.size())
    , getOffPos_(0) { }

void VectorStreambuf::reserve(size_t size) {
    buffer_.reserve(size);
}

size_t VectorStreambuf::size() {
    return buffer_.size();
}

uint8_t* VectorStreambuf::data() {
    return buffer_.data();
}

std::streamsize VectorStreambuf::xsputn(const char_type* s, std::streamsize n) {
    if (putOffPos_ == buffer_.size()) {
        buffer_.insert(buffer_.end(),
                       reinterpret_cast<const uint8_t*>(s),
                       reinterpret_cast<const uint8_t*>(s) + n);
        putOffPos_ = buffer_.size();
    } else {
        if (putOffPos_ + n < buffer_.size()) {
            std::memcpy(buffer_.data() + putOffPos_, reinterpret_cast<const uint8_t*>(s), n);
            putOffPos_ += n;
        } else {
            // Not really sure about this logic but it's not used
            std::memcpy(buffer_.data() + putOffPos_,
                        reinterpret_cast<const uint8_t*>(s),
                        buffer_.size() - putOffPos_);
            buffer_.insert(buffer_.end(),
                           reinterpret_cast<const uint8_t*>(s + (buffer_.size() - putOffPos_)),
                           reinterpret_cast<const uint8_t*>(s + (buffer_.size() - putOffPos_)) + n -
                               (buffer_.size() - putOffPos_));
            putOffPos_ = buffer_.size();
        }
    }
    return n;
}

std::streambuf::int_type VectorStreambuf::overflow(std::streambuf::int_type ch) {
    if (putOffPos_ == buffer_.size()) {
        buffer_.push_back(static_cast<uint8_t>(ch));
        putOffPos_ = buffer_.size();
    } else {
        buffer_[putOffPos_] = static_cast<uint8_t>(ch);
        putOffPos_++;
    }
    return ch;
}

std::streamsize VectorStreambuf::xsgetn(char* s, std::streamsize n) {
    if (getOffPos_ < buffer_.size()) {
        const size_t num = std::min<size_t>(n, buffer_.size() - getOffPos_);
        std::memcpy(s, &buffer_[getOffPos_], num);
        getOffPos_ += num;
        return num;
    }
    return 0;
}

int VectorStreambuf::underflow() {
    if (getOffPos_ < buffer_.size()) {
        return static_cast<unsigned char>(buffer_[getOffPos_]);
    } else {
        return EOF;
    }
}

int VectorStreambuf::uflow() {
    if (getOffPos_ < buffer_.size())
        return static_cast<unsigned char>(buffer_[getOffPos_++]);
    else
        return EOF;
}

std::streampos VectorStreambuf::seekoff(std::streamoff off,
                                        std::ios_base::seekdir way,
                                        std::ios_base::openmode which) {
    if (which == std::ios_base::out) {
        if (way == std::ios_base::cur) {
            if (((static_cast<long long>(putOffPos_) + off) > static_cast<long long>(buffer_.size())) ||
                ((static_cast<long long>(putOffPos_) + off) < 0)) {
                return -1;
            }

            putOffPos_ += off;
            return putOffPos_;
        } else if (way == std::ios_base::beg) {
            if (off > static_cast<long long>(buffer_.size())) {
                return -1;
            }
            putOffPos_ = off;
            return putOffPos_;
        } else if (way == std::ios_base::end) {
            if ((static_cast<long long>(buffer_.size()) + off) < 0) {
                return -1;
            }
            putOffPos_ = buffer_.size() + off;
            return putOffPos_;
        }
    }

    return -1;
}

std::vector<uint8_t>& VectorStreambuf::ref() {
    return buffer_;
}

void VectorStreambuf::clear() {
    buffer_.erase(buffer_.begin(), buffer_.end());
    putOffPos_ = 0;
}

} // namespace middleware
} // namespace urf
