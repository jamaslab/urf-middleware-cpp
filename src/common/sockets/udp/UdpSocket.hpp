#pragma once

#include <atomic>
#include <chrono>
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#    include <winsock2.h>
#    include <ws2tcpip.h>
#endif

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/udp/UdpServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class UdpSocket : public ITransportSocket {
 public:
    UdpSocket() = delete;
    UdpSocket(const sockaddr_in& sockaddr);
    UdpSocket(const std::string& hostname, int port);
    UdpSocket(const UdpSocket&) = delete;
    UdpSocket(UdpSocket&&) = delete;
    ~UdpSocket() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;
    bool allowsFragmentedRead() override { return false; }
    
    bool write(const std::vector<uint8_t>& bytes, bool requiresAck = false) override;
    std::vector<uint8_t> read(int length) override;
    std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) override;

    bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) override;

 private:
    bool clientConnectionHandshake();
    bool serverConnectionHandshake();
    bool sendAck();
    bool sendPacket(UdpHeader& header, const char* buffer, size_t size, bool reqAck);

 private:
    friend class UdpServer;

    enum Roles { Client, Server };

#ifdef __linux__
    int socket_;
    int getSocketError();
#elif _WIN32 || _WIN64
    WSADATA wsaData_;
    SOCKET socket_;
#endif

    Roles role_;
    std::atomic<bool> isOpen_;

    bool isBlocking_;

    std::string hostname_;
    int port_;
    uint16_t localPort_;

    sockaddr_in sockaddr_;
    std::atomic<uint16_t> senderSequenceNumber_;
    uint16_t lastSequenceNumber_;

    bool setNonBlockingFlag(bool value);
    std::vector<uint8_t> nonBlockingRead(int length, const std::chrono::milliseconds& timeout);
    std::vector<uint8_t> blockingRead(int length);

    std::vector<std::function<void(ITransportSocket*)>> connectionLostHandlers_;
    void handleConnectionLost();

    std::vector<uint8_t> inputBuffer_;
};

} // namespace sockets
} // namespace middleware
} // namespace urf
