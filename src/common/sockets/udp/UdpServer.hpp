#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <string>
#include <thread>
#include <vector>

#ifdef __linux__
#    include <netinet/in.h>
#    include <netinet/ip.h>
#    include <sys/socket.h>
#    include <sys/types.h>
#endif

#if defined(_WIN32) || defined(_WIN64)
#    include <winsock2.h>
#    include <ws2tcpip.h>
#endif

#include <urf/common/containers/ThreadSafeQueue.hpp>

#include "common/sockets/ITransportSocketServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

#define UDP_CONNECTION_CONTROL 0x41
#define UDP_CLOSE_CONNECTION 0x42
#define UDP_ACK 0x20
#define UDP_PACKET 0x80

#pragma pack(push, 1)
typedef struct UdpHeader {
    uint8_t controls;
} UdpHeader;
#pragma pack(pop)

class UdpServer : public ITransportSocketServer {
 public:
    UdpServer() = delete;
    explicit UdpServer(uint16_t port);
    UdpServer(const UdpServer&) = delete;
    UdpServer(UdpServer&&) = delete;
    ~UdpServer() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;

    std::shared_ptr<ITransportSocket> acceptConnection() override;
    bool
    onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) override;

 private:
    void listener();

 private:
    uint16_t port_;

#ifdef __linux__
    int socket_;
    int getSocketError();
#elif _WIN32 || _WIN64
    WSADATA data_;
    SOCKET socket_;
#endif

    bool isOpen_;
    std::vector<std::shared_ptr<ITransportSocket>> connectedClients_;

    std::atomic<bool> stopListenerThread_;
    std::thread listenerThread_;
    common::containers::ThreadSafeQueue<std::shared_ptr<ITransportSocket>> connectionRequestQueue_;
};

} // namespace sockets
} // namespace middleware
} // namespace urf
