#include <future>
#include <string>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <poll.h>
#include <unistd.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/udp/UdpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("UdpSocket");
const size_t UDP_MAX_SIZE = 65507 - sizeof(urf::middleware::sockets::UdpHeader);
} // namespace

namespace urf {
namespace middleware {
namespace sockets {

UdpSocket::UdpSocket(const std::string& hostname, int port)
    : socket_(-1)
    , role_(Roles::Client)
    , isOpen_(false)
    , isBlocking_(true)
    , hostname_(hostname)
    , port_(port)
    , localPort_(0)
    , sockaddr_()
    , connectionLostHandlers_() {
    LOGGER.trace("CTor");
}

UdpSocket::UdpSocket(const sockaddr_in& sockaddr)
    : socket_(-1)
    , role_(Roles::Server)
    , isOpen_(false)
    , hostname_()
    , port_(static_cast<uint16_t>(sockaddr.sin_port))
    , localPort_(0)
    , sockaddr_(sockaddr)
    , connectionLostHandlers_() { }

UdpSocket::~UdpSocket() {
    LOGGER.trace("DTor");

    ::shutdown(socket_, SHUT_RDWR);
    ::close(socket_);
}

bool UdpSocket::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    if (role_ == Roles::Client) {
        struct hostent* ent = gethostbyname(hostname_.c_str());
        if (ent == NULL) {
            LOGGER.warn("Something wrong with the hostname");
            return false;
        }

        auto localIP = inet_ntoa(*(struct in_addr*)*ent->h_addr_list);
        sockaddr_.sin_family = AF_INET;
        sockaddr_.sin_addr.s_addr = inet_addr(localIP);
        sockaddr_.sin_port = htons(port_);
    }

    if ((socket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        LOGGER.warn("Could not create socket: {}", strerror(errno));
        return false;
    }

    if (role_ == Roles::Server) {
        struct sockaddr_in servaddr;
        std::memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = AF_INET;
        servaddr.sin_addr.s_addr = INADDR_ANY;
        servaddr.sin_port = 0;

        if (bind(socket_, reinterpret_cast<sockaddr*>(&servaddr), sizeof(servaddr)) == -1) {
            LOGGER.warn("Failed to bind socket: {}", strerror(errno));
            return false;
        }

        struct sockaddr_in sin;
        socklen_t addrlen = sizeof(sin);
        if (getsockname(socket_, (struct sockaddr*)&sin, &addrlen) == 0 &&
            sin.sin_family == AF_INET && addrlen == sizeof(sin)) {
            localPort_ = ntohs(sin.sin_port);
        }

        if (localPort_ == 0) {
            LOGGER.info("Failed to get local port");
            return false;
        }
    }

    if (connect(socket_, reinterpret_cast<sockaddr*>(&sockaddr_), sizeof(sockaddr_)) == -1) {
        LOGGER.warn("Failed to connect to socket: {}", strerror(errno));
        return false;
    }

    if ((role_ == Roles::Client) && !clientConnectionHandshake()) {
        LOGGER.warn("Could not connect to partner socket");
        return false;
    }

    setNonBlockingFlag(false);

    inputBuffer_.resize(UDP_MAX_SIZE);
    isOpen_ = true;
    return true;
}

bool UdpSocket::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    UdpHeader header;
    header.controls = UDP_CLOSE_CONNECTION;
    sendPacket(header, NULL, 0, false);

    if (::shutdown(socket_, SHUT_RDWR) < 0) {
        if (errno != ENOTCONN && errno != EINVAL)
            LOGGER.warn("Could not make shutdown: {}", strerror(errno));
    }
    if (::close(socket_) < 0)
        LOGGER.warn("Could not close server: {}", strerror(errno));

    isOpen_ = false;
    return true;
}

bool UdpSocket::isOpen() {
    return isOpen_;
}

bool UdpSocket::write(const std::vector<uint8_t>& buffer, bool) {
    if (!isOpen_) {
        return false;
    }

    if (buffer.size() > UDP_MAX_SIZE) {
        LOGGER.info("Can't send packages bigger than {}", UDP_MAX_SIZE);
        return false;
    }

    if (sendto(socket_,
               buffer.data(),
               buffer.size(),
               0,
               reinterpret_cast<sockaddr*>(&sockaddr_),
               sizeof(sockaddr_)) == -1) {
        LOGGER.warn("Could not sendto: {}", strerror(errno));
        return false;
    };

    return true;
}

std::vector<uint8_t> UdpSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (length > static_cast<int>(UDP_MAX_SIZE)) {
        LOGGER.warn("Cant' receive package bigger than {}", UDP_MAX_SIZE);
        return std::vector<uint8_t>();
    }

    if (!isBlocking_) {
        setNonBlockingFlag(false);
    }

    return blockingRead(length);
}

std::vector<uint8_t> UdpSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (length > static_cast<int>(UDP_MAX_SIZE)) {
        LOGGER.warn("Cant' receive package bigger than {}", UDP_MAX_SIZE);
        return std::vector<uint8_t>();
    }

    if (isBlocking_) {
        setNonBlockingFlag(true);
    }
    return nonBlockingRead(length, timeout);
}

bool UdpSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onPartnerDisconnect()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

bool UdpSocket::setNonBlockingFlag(bool value) {
    LOGGER.trace("Changing blocking flag to {}", value);
    if (value) {
        int flags = fcntl(socket_, F_GETFL);
        if (fcntl(socket_, F_SETFL, flags | O_NONBLOCK) != 0) {
            LOGGER.warn("Could not set O_NONBLOCK flag: {}", strerror(errno));
            return false;
        }
        isBlocking_ = false;
    } else {
        int flags = fcntl(socket_, F_GETFL);
        if (fcntl(socket_, F_SETFL, flags & (~O_NONBLOCK)) != 0) {
            LOGGER.warn("Could not reset O_NONBLOCK flag: {}", strerror(errno));
            return false;
        }
        isBlocking_ = true;
    }

    return true;
    ;
}

std::vector<uint8_t> UdpSocket::nonBlockingRead(int length,
                                                const std::chrono::milliseconds& timeout) {
    struct pollfd fd;
    fd.fd = socket_;
    fd.events = POLLIN;
    auto retval = ::poll(&fd, 1, timeout.count());
    if (retval <= 0) {
        LOGGER.info("Read timeout");
        return std::vector<uint8_t>();
    }

    return blockingRead(length);
}

std::vector<uint8_t> UdpSocket::blockingRead(int) {
    auto receivedLength = recv(socket_, inputBuffer_.data(), UDP_MAX_SIZE, 0);
    if (receivedLength < static_cast<int>(sizeof(UdpHeader))) {
        LOGGER.info("Error during recv: {}", strerror(errno));
        return std::vector<uint8_t>();
    }

    if (receivedLength == static_cast<int>(sizeof(UdpHeader))) {
        UdpHeader header;
        std::memcpy(&header, inputBuffer_.data(), sizeof(UdpHeader));

        if (header.controls == UDP_CLOSE_CONNECTION) {
            handleConnectionLost();
        }

        return std::vector<uint8_t>();
    } else {
        return std::vector<uint8_t>(inputBuffer_.begin(), inputBuffer_.begin() + receivedLength);
    }
}

void UdpSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

bool UdpSocket::sendAck() {
    UdpHeader header;
    header.controls = UDP_ACK;

    return sendPacket(header, NULL, 0, false);
}

bool UdpSocket::sendPacket(UdpHeader& header, const char* buffer, size_t size, bool) {
    if (size > 0) {
        if (sendto(socket_,
                   reinterpret_cast<char*>(&header),
                   sizeof(header),
                   MSG_MORE,
                   reinterpret_cast<sockaddr*>(&sockaddr_),
                   sizeof(sockaddr_)) == -1) {
            LOGGER.warn("Could not sendto: {}", strerror(errno));
            return false;
        }

        if (sendto(socket_,
                   buffer,
                   size,
                   0,
                   reinterpret_cast<sockaddr*>(&sockaddr_),
                   sizeof(sockaddr_)) == -1) {
            LOGGER.warn("Could not sendto: {}", strerror(errno));
            return false;
        }

    } else {
        if (sendto(socket_,
                   reinterpret_cast<char*>(&header),
                   sizeof(header),
                   0,
                   reinterpret_cast<sockaddr*>(&sockaddr_),
                   sizeof(sockaddr_)) == -1) {
            LOGGER.warn("Could not sendto: {}", strerror(errno));
            return false;
        }
    }

    return true;
}

bool UdpSocket::clientConnectionHandshake() {
    LOGGER.trace("clientConnectionHandshake");

    UdpHeader connectionHeader;
    connectionHeader.controls = UDP_CONNECTION_CONTROL;

    auto connectionAckReceived = std::async(std::launch::async, [this]() {
        char buf[sizeof(UdpHeader) + 2];
        if (recv(socket_, buf, sizeof(UdpHeader) + 2, 0) < 0) {
            LOGGER.info("Error during client handshake recvfrom: {}", strerror(errno));
            return (uint16_t)0;
        }

        uint16_t newPort = 0;
        std::memcpy(&newPort, buf + sizeof(UdpHeader), 2);
        return newPort;
    });

    int numberOfAttempts = 0;
    while (numberOfAttempts < 10) {
        if (!sendPacket(connectionHeader, NULL, 0, true)) {
            numberOfAttempts = 10;
            break;
        }
        auto status = connectionAckReceived.wait_for(std::chrono::milliseconds(100));
        if (status == std::future_status::timeout) {
            numberOfAttempts++;
        } else if (status == std::future_status::ready) {
            break;
        }
    }

    if (numberOfAttempts == 10) {
        LOGGER.info("Timeout while attempting to connect");
        ::shutdown(socket_, SHUT_RDWR);
        ::close(socket_);
        connectionAckReceived.get();
        return false;
    }

    // Check here if what you received actually makes sense
    auto partnerPort = connectionAckReceived.get();
    sockaddr_.sin_port = partnerPort;

    if (connect(socket_, reinterpret_cast<sockaddr*>(&sockaddr_), sizeof(sockaddr_)) == -1) {
        LOGGER.warn("Failed to connect to socket: {}", strerror(errno));
        return false;
    }

    if (!sendAck()) {
        LOGGER.warn("Could not send ack");
        return false;
    }

    return true;
}

bool UdpSocket::serverConnectionHandshake() {
    LOGGER.trace("serverConnectionHandshake");
    UdpHeader connectionHeader;
    auto connectionAckReceived = std::async(std::launch::async, [this, &connectionHeader]() {
        if (recv(socket_, reinterpret_cast<char*>(&connectionHeader), sizeof(connectionHeader), 0) <
            0) {
            LOGGER.warn("Error during server handshake recvfrom: {}", strerror(errno));
            return -1;
        }
        return 0;
    });

    auto status = connectionAckReceived.wait_for(std::chrono::seconds(1));
    if ((status == std::future_status::timeout) || (connectionAckReceived.get() == -1)) {
        ::shutdown(socket_, SHUT_RDWR);
        ::close(socket_);
        LOGGER.warn("Didn't receive connection ack from client");
        return false;
    }

    return true;
}

} // namespace sockets
} // namespace middleware
} // namespace urf
