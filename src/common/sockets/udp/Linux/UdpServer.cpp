#define MAX_PENDING_CONNECTIONS 5

#include <cstring>
#include <memory>
#include <string>

#include <unistd.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/tcp/TcpSocket.hpp"
#include "common/sockets/udp/UdpServer.hpp"
#include "common/sockets/udp/UdpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("UdpServer");
}

namespace urf {
namespace middleware {
namespace sockets {

UdpServer::UdpServer(uint16_t port)
    : port_(port)
    , socket_(-1)
    , isOpen_(false)
    , connectedClients_()
    , stopListenerThread_(false)
    , listenerThread_()
    , connectionRequestQueue_() {
    LOGGER.trace("CTor");
}

UdpServer::~UdpServer() {
    LOGGER.trace("DTor");
    ::shutdown(socket_, SHUT_RDWR);
    ::close(socket_);
}

bool UdpServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    connectionRequestQueue_ =
        common::containers::ThreadSafeQueue<std::shared_ptr<ITransportSocket>>();

    if ((socket_ = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
        LOGGER.warn("Could not open socket: {}", strerror(errno));
        return false;
    }

    struct sockaddr_in si_me;
    memset((char*)&si_me, 0, sizeof(si_me));

    si_me.sin_family = AF_INET;
    si_me.sin_port = htons(port_);
    si_me.sin_addr.s_addr = htonl(INADDR_ANY);

    //bind socket to port
    if (bind(socket_, (struct sockaddr*)&si_me, sizeof(si_me)) == -1) {
        LOGGER.warn("Could not open socket: {}", strerror(errno));
        ::close(socket_);
        return false;
    }

    stopListenerThread_ = true;
    listenerThread_ = std::thread(&UdpServer::listener, this);

    while (stopListenerThread_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    };

    isOpen_ = true;
    return true;
}

bool UdpServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (size_t i = 0; i < connectedClients_.size(); i++) {
        connectedClients_[i]->close();
    }

    stopListenerThread_ = true;
    connectedClients_.clear();
    if (::shutdown(socket_, SHUT_RDWR) < 0) {
        if (errno != ENOTCONN && errno != EINVAL)
            LOGGER.warn("Could not make shutdown: {}", strerror(errno));
    }
    if (::close(socket_) < 0)
        LOGGER.warn("Could not close server: {}", strerror(errno));
    listenerThread_.join();
    connectionRequestQueue_.dispose();

    isOpen_ = false;
    return true;
}

bool UdpServer::isOpen() {
    LOGGER.trace("isOpen");
    return isOpen_;
}

std::shared_ptr<ITransportSocket> UdpServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    auto socketOpt = connectionRequestQueue_.pop();
    if (!socketOpt) {
        return nullptr;
    }

    auto socket = std::dynamic_pointer_cast<UdpSocket>(socketOpt.value());
    if (!socket->open()) {
        return nullptr;
    } else {
        UdpHeader connectionHeader;
        connectionHeader.controls = UDP_CONNECTION_CONTROL;

        if (sendto(socket_,
                   reinterpret_cast<char*>(&connectionHeader),
                   sizeof(connectionHeader),
                   MSG_MORE,
                   reinterpret_cast<sockaddr*>(&socket->sockaddr_),
                   sizeof(socket->sockaddr_)) == -1) {
            LOGGER.warn("Could not sendto: {}", strerror(errno));
        }

        uint16_t port = htons(socket->localPort_);
        if (sendto(socket_,
                   reinterpret_cast<char*>(&port),
                   sizeof(port),
                   0,
                   reinterpret_cast<sockaddr*>(&socket->sockaddr_),
                   sizeof(socket->sockaddr_)) == -1) {
            LOGGER.warn("Could not sendto: {}", strerror(errno));
        }

        if (!socket->serverConnectionHandshake()) {
            return nullptr;
        }

        connectedClients_.push_back(socket);
        LOGGER.info("Client connected");
        return socket;
    }

    return nullptr;
}

bool UdpServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>&) {
    return true;
}

void UdpServer::listener() {
    stopListenerThread_ = false;
    std::vector<char> buffer;
    buffer.resize(std::numeric_limits<uint16_t>::max());

    struct sockaddr_in client;
    socklen_t size_sockaddr = (uint32_t)sizeof(client);
    UdpHeader header;

    while (!stopListenerThread_) {
        std::memset(reinterpret_cast<char*>(&client), 0, sizeof(client));
        auto received = recvfrom(socket_,
                                 buffer.data(),
                                 std::numeric_limits<uint16_t>::max(),
                                 0,
                                 reinterpret_cast<sockaddr*>(&client),
                                 &size_sockaddr);
        if (received < 0) {
            LOGGER.warn("Something wrong in recvfrom: {}", strerror(errno));
            continue;
        }

        std::memcpy(&header, buffer.data(), sizeof(header));
        if (header.controls == UDP_CONNECTION_CONTROL) {
            connectionRequestQueue_.push(std::make_shared<UdpSocket>(client));
        }
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
