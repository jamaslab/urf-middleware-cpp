#define MAX_PENDING_CONNECTIONS 5

#include <cstring>
#include <limits>
#include <memory>
#include <string>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/tcp/TcpSocket.hpp"
#include "common/sockets/udp/UdpServer.hpp"
#include "common/sockets/udp/UdpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("UdpServer");
}

namespace urf {
namespace middleware {
namespace sockets {

UdpServer::UdpServer(uint16_t port)
    : port_(port)
    , data_()
    , socket_(INVALID_SOCKET)
    , isOpen_(false)
    , connectedClients_()
    , stopListenerThread_(false)
    , listenerThread_()
    , connectionRequestQueue_() {
    LOGGER.trace("CTor");
}

UdpServer::~UdpServer() {
    LOGGER.trace("DTor");
    closesocket(socket_);
    WSACleanup();
}

bool UdpServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    connectionRequestQueue_ =
        common::containers::ThreadSafeQueue<std::shared_ptr<ITransportSocket>>();

    auto retval = WSAStartup(MAKEWORD(2, 2), &data_);
    if (retval != 0) {
        LOGGER.warn("WSAStartup failed with error: {}", retval);
        return false;
    }

    struct addrinfo* result = NULL;
    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_DGRAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the server address and port
    retval = getaddrinfo(NULL, std::to_string(port_).c_str(), &hints, &result);
    if (retval != 0) {
        LOGGER.warn("getaddrinfo failed with error: {}", retval);
        WSACleanup();
        return false;
    }

    // Create a SOCKET for connecting to server
    socket_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (socket_ == INVALID_SOCKET) {
        LOGGER.warn("socket failed with error: {}", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return false;
    }

    // Setup the TCP listening socket
    retval = bind(socket_, result->ai_addr, (int)result->ai_addrlen);
    if (retval == SOCKET_ERROR) {
        LOGGER.warn("bind failed with error: {}", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(socket_);
        WSACleanup();
        return false;
    }

    stopListenerThread_ = true;
    listenerThread_ = std::thread(&UdpServer::listener, this);

    while (stopListenerThread_) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    };

    isOpen_ = true;
    return true;
}

bool UdpServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (size_t i = 0; i < connectedClients_.size(); i++) {
        connectedClients_[i]->close();
    }

    stopListenerThread_ = true;
    connectedClients_.clear();
    closesocket(socket_);
    listenerThread_.join();
    connectionRequestQueue_.dispose();

    isOpen_ = false;
    return true;
}

bool UdpServer::isOpen() {
    LOGGER.trace("isOpen");
    return isOpen_;
}

std::shared_ptr<ITransportSocket> UdpServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    auto socketOpt = connectionRequestQueue_.pop();
    if (!socketOpt) {
        return nullptr;
    }

    auto socket = std::dynamic_pointer_cast<UdpSocket>(socketOpt.value());
    if (!socket->open()) {
        return nullptr;
    } else {
        UdpHeader connectionHeader;
        connectionHeader.controls = UDP_CONNECTION_CONTROL;

        DWORD sent = 0;
        char* localBuf = new char[sizeof(connectionHeader) + sizeof(socket->localPort_)];

        uint16_t port = htons(socket->localPort_);

        std::memcpy(localBuf, &connectionHeader, sizeof(connectionHeader));
        std::memcpy(localBuf + sizeof(connectionHeader), &port, sizeof(port));

        WSABUF buf;
        buf.buf = (CHAR*)localBuf;
        buf.len = sizeof(connectionHeader) + sizeof(socket->localPort_);

        if (WSASendTo(socket_,
                      &buf,
                      1,
                      &sent,
                      0,
                      reinterpret_cast<sockaddr*>(&socket->sockaddr_),
                      sizeof(socket->sockaddr_),
                      NULL,
                      NULL) != 0) {
            LOGGER.warn("Failed to send header: {}", WSAGetLastError());
            return nullptr;
        };

        if (!socket->serverConnectionHandshake()) {
            return nullptr;
        }

        connectedClients_.push_back(socket);
        return socket;
    }

    return nullptr;
}

bool UdpServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) {
    return true;
}

void UdpServer::listener() {
    stopListenerThread_ = false;
    std::vector<char> buffer;
    buffer.resize(std::numeric_limits<uint16_t>::max());

    struct sockaddr_in client;
    socklen_t size_sockaddr = (uint32_t)sizeof(client);
    UdpHeader header;

    while (!stopListenerThread_) {
        std::memset(reinterpret_cast<char*>(&client), 0, sizeof(client));
        auto received = recvfrom(socket_,
                                 buffer.data(),
                                 std::numeric_limits<uint16_t>::max(),
                                 0,
                                 reinterpret_cast<sockaddr*>(&client),
                                 &size_sockaddr);

        if (received < 0) {
            LOGGER.warn("Something wrong in recvfrom: {}", strerror(errno));
            continue;
        }

        std::memcpy(&header, buffer.data(), sizeof(header));
        if (header.controls == UDP_CONNECTION_CONTROL) {
            connectionRequestQueue_.push(std::make_shared<UdpSocket>(client));
        }
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
