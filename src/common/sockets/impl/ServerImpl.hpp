#pragma once

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <shared_mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

#include "urf/middleware/messages/Message.hpp"
#include "urf/middleware/sockets/ISocket.hpp"
#include "urf/middleware/sockets/Server.hpp"

#include "common/sockets/impl/ClientImpl.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>
#include <urf/common/events/events.hpp>

#include "common/sockets/ITransportSocketServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class Server::ServerImpl {
 public:
    ServerImpl() = delete;
    explicit ServerImpl(const std::string& serviceName,
                        std::shared_ptr<ITransportSocketServer> server);
    explicit ServerImpl(const std::string& serviceName,
                        const std::vector<std::shared_ptr<ITransportSocketServer>>& servers);
    ServerImpl(const ServerImpl&) = delete;
    ServerImpl(ServerImpl&&) = delete;
    ~ServerImpl();

    bool open();
    bool close();
    bool isOpen();

    size_t connectedClientsCount();
    std::string serviceName() const;

    std::optional<messages::Message> pull();
    std::optional<messages::Message> pull(const std::chrono::milliseconds& timeout);

    bool push(const messages::Message& message, bool requiresAck = true);
    bool onPush(const std::function<void(const messages::Message&)>&);

    std::vector<messages::Message> request(const messages::Message& message);
    std::vector<messages::Message> request(const messages::Message& message,
                                           const std::chrono::milliseconds& timeout);

    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message);
    std::future<std::vector<messages::Message>>
    requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout);

    std::optional<messages::Message> receiveRequest();
    std::optional<messages::Message> receiveRequest(const std::chrono::milliseconds& timeout);

    bool respond(const messages::Message& message);

    bool subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate);
    uint32_t subscriptionsCount(const std::string& topic);
    bool unsubscribe(const std::string& topic);

    bool keepUpdateHistory(bool value);
    std::optional<messages::Message> receiveUpdate(const std::string& topic);
    std::optional<messages::Message> receiveUpdate(const std::string& topic,
                                                   const std::chrono::milliseconds& timeout);
    bool onUpdate(const std::string& topic,
                  const std::function<void(const std::string& topic, const messages::Message&)>&);

    bool
    publish(const std::string& topic, const messages::Message& message, bool requiresAck = true);
    std::vector<std::string> availablePartnerTopics();

    bool addTopic(const std::string& topic);
    bool removeTopic(const std::string& topic);

    bool automaticallyDeserializeBody(bool value);

 private:
    void acceptHandler(std::shared_ptr<ITransportSocketServer> server);

    void onPushHandler(const messages::Message& msg);
    void onRequestHandler(Client::ClientImpl* socket, messages::Message&& msg);
    void onUpdateHandler(const std::string& topic, const messages::Message& message);
    void onClientDisconnectHandler(const std::shared_ptr<Client::ClientImpl>& channel,
                                   sockets::ITransportSocket* socket);

    void updateServiceRegistration();

 private:
    std::string serviceName_;

    std::vector<std::shared_ptr<ITransportSocketServer>> servers_;
    std::atomic<bool> deserializeBody_;
    std::atomic<bool> isOpen_;
    std::atomic<bool> isClosing_;

    std::shared_mutex connectedClientsMutex_;
    std::vector<std::shared_ptr<Client::ClientImpl>> connectedClients_;

    // Push-Pull members
    common::containers::ThreadSafeQueue<messages::Message> pullQueue_;
    common::events::event<messages::Message> pushEvent_;

    // Request-Response members
    std::atomic<bool> pendingResponse_;
    std::atomic<bool> pendingRequest_;
    Client::ClientImpl* responseClient_;
    common::containers::ThreadSafeQueue<std::pair<Client::ClientImpl*, messages::Message>>
        requestsQueue_;

    // Publish-Subscribe members
    std::atomic<bool> keepUpdateHistory_;
    std::shared_mutex onUpdateHandlersMtx_;
    std::unordered_map<std::string, common::events::event<std::string, messages::Message>>
        onUpdateHandlers_;
    std::unordered_map<std::string,
                       std::shared_ptr<common::containers::ThreadSafeQueue<messages::Message>>>
        updateQueues_;

    std::shared_mutex currentSubscriptionMutex_;
    bool subscribedToAll_;
    std::vector<std::string> currentSubscriptions_;
    std::vector<std::string> currentUnsubscriptions_;
    std::vector<std::string> availableTopics_;

    std::atomic<bool> stopThread_;
    std::atomic<uint32_t> threadStartedFlag_;
    std::vector<std::thread> acceptThreads_;
};

} // namespace sockets
} // namespace middleware
} // namespace urf
