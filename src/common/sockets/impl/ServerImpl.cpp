#include <iostream>

#include <urf/common/logger/Logger.hpp>

#include "urf/middleware/services/LocalServices.hpp"

#include "common/sockets/impl/ServerImpl.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("Server");
}

namespace urf {
namespace middleware {
namespace sockets {

Server::ServerImpl::ServerImpl(const std::string& serviceName,
                               std::shared_ptr<ITransportSocketServer> server)
    : ServerImpl(serviceName, std::vector<std::shared_ptr<ITransportSocketServer>>({server})) { }

Server::ServerImpl::ServerImpl(const std::string& serviceName,
                               const std::vector<std::shared_ptr<ITransportSocketServer>>& servers)
    : serviceName_(serviceName)
    , servers_(servers)
    , deserializeBody_(true)
    , isOpen_(false)
    , isClosing_(false)
    , connectedClientsMutex_()
    , connectedClients_()
    , pullQueue_()
    , pushEvent_()
    , pendingResponse_(false)
    , pendingRequest_(false)
    , responseClient_()
    , requestsQueue_()
    , keepUpdateHistory_(false)
    , onUpdateHandlersMtx_()
    , onUpdateHandlers_()
    , updateQueues_()
    , currentSubscriptionMutex_()
    , subscribedToAll_(false)
    , currentSubscriptions_()
    , currentUnsubscriptions_()
    , availableTopics_()
    , stopThread_(false)
    , threadStartedFlag_(false) {
    LOGGER.trace("CTor");
}

Server::ServerImpl::~ServerImpl() {
    LOGGER.trace("DTor");
    if (isOpen_)
        close();
}

bool Server::ServerImpl::open() {
    LOGGER.trace("open()");
    if (isOpen()) {
        LOGGER.warn("Socket already opened");
        return false;
    }

    stopThread_ = false;
    connectedClients_.clear();
    acceptThreads_.clear();
    threadStartedFlag_ = 0;

    pullQueue_ = common::containers::ThreadSafeQueue<messages::Message>();
    requestsQueue_ =
        common::containers::ThreadSafeQueue<std::pair<Client::ClientImpl*, messages::Message>>();

    for (size_t i = 0; i < servers_.size(); i++) {
        if (!servers_[i]->isOpen() && !servers_[i]->open()) {
            LOGGER.warn("Could not open socket");
            for (size_t k = 0; k < i; k++) {
                if (servers_[k]->isOpen())
                    servers_[k]->close();
            }
            return false;
        }
    }

    for (size_t i = 0; i < servers_.size(); i++) {
        acceptThreads_.push_back(
            std::thread(&Server::ServerImpl::acceptHandler, this, servers_[i]));
    }

    while (threadStartedFlag_ < servers_.size()) {
    };

    isOpen_ = true;
    return true;
}

bool Server::ServerImpl::close() {
    LOGGER.trace("close()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return false;
    }

    isClosing_ = true;
    stopThread_ = true;

    for (size_t i = 0; i < servers_.size(); i++) {
        if (servers_[i]->isOpen()) {
            servers_[i]->close();
            acceptThreads_[i].join();
        }
    }

    pullQueue_.dispose();
    requestsQueue_.dispose();
    {
        std::unique_lock guard(onUpdateHandlersMtx_);
        for (auto queue : updateQueues_) {
            queue.second->dispose();
        }
    }

    for (auto client : connectedClients_) {
        client->close();
    }

    connectedClients_.clear();
    acceptThreads_.clear();
    {
        std::unique_lock guard(onUpdateHandlersMtx_);
        updateQueues_.clear();
    }

    isOpen_ = false;
    isClosing_ = false;
    return true;
}

bool Server::ServerImpl::isOpen() {
    return isOpen_;
}

size_t Server::ServerImpl::connectedClientsCount() {
    std::shared_lock clientsGuard(connectedClientsMutex_);
    return connectedClients_.size();
}

std::string Server::ServerImpl::serviceName() const {
    LOGGER.trace("serviceName()");
    return serviceName_;
}

std::optional<messages::Message> Server::ServerImpl::pull() {
    LOGGER.trace("pull()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::nullopt;
    }

    return pullQueue_.pop();
}

std::optional<messages::Message>
Server::ServerImpl::pull(const std::chrono::milliseconds& timeout) {
    LOGGER.trace("pull()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::nullopt;
    }

    return pullQueue_.pop(timeout);
}

bool Server::ServerImpl::push(const messages::Message& message, bool requiresAck) {
    LOGGER.trace("push()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return false;
    }

    auto copiable = message;
    copiable.body().setBytes(copiable.body().serialize());

    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        client->push(copiable, requiresAck);
    }

    return true;
}

bool Server::ServerImpl::onPush(const std::function<void(const messages::Message&)>& handler) {
    LOGGER.trace("onPush()");
    pushEvent_.subscribe(handler, common::events::event_policy::synchronous);
    return true;
}

std::vector<messages::Message> Server::ServerImpl::request(const messages::Message& message) {
    LOGGER.trace("request()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::vector<messages::Message>();
    }

    bool f = false;
    if (!pendingResponse_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Socket is still waiting for a response, can't send a new request");
        return std::vector<messages::Message>();
    }
    std::vector<std::future<std::vector<messages::Message>>> repFutures;
    {

        std::shared_lock clientsGuard(connectedClientsMutex_);
        for (auto client : connectedClients_) {
            repFutures.push_back(client->requestAsync(message));
        }
    }

    std::vector<messages::Message> responses;
    for (auto it = repFutures.begin(); it != repFutures.end(); it++) {
        auto response = (*it).get();
        if (response.empty())
            continue;
        responses.push_back(std::move(response[0]));
    }

    pendingResponse_ = false;

    return responses;
}

std::vector<messages::Message>
Server::ServerImpl::request(const messages::Message& message,
                            const std::chrono::milliseconds& timeout) {
    LOGGER.trace("request()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::vector<messages::Message>();
    }

    bool f = false;
    if (!pendingResponse_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Socket is still waiting for a response, can't send a new request");
        return std::vector<messages::Message>();
    }

    std::vector<std::future<std::vector<messages::Message>>> repFutures;
    {

        std::shared_lock clientsGuard(connectedClientsMutex_);
        for (auto client : connectedClients_) {
            repFutures.push_back(client->requestAsync(message, timeout));
        }
    }

    std::vector<messages::Message> responses;
    for (auto it = repFutures.begin(); it != repFutures.end(); it++) {
        auto response = (*it).get();
        if (response.empty())
            continue;
        responses.push_back(std::move(response[0]));
    }

    pendingResponse_ = false;

    return responses;
}

std::future<std::vector<messages::Message>>
Server::ServerImpl::requestAsync(const messages::Message& message) {
    LOGGER.trace("requestAsync()");
    return std::async(std::launch::async, [this, &message]() { return request(message); });
}

std::future<std::vector<messages::Message>>
Server::ServerImpl::requestAsync(const messages::Message& message,
                                 const std::chrono::milliseconds& timeout) {
    LOGGER.trace("requestAsync()");
    return std::async(std::launch::async,
                      [this, &message, &timeout]() { return request(message, timeout); });
}

std::optional<messages::Message> Server::ServerImpl::receiveRequest() {
    LOGGER.trace("receiveRequest()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::nullopt;
    }

    bool f = false;
    if (!pendingRequest_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Can't wait for another request before responding to the previous one");
        return std::nullopt;
    }

    auto message = requestsQueue_.pop();
    if (!message) {
        pendingRequest_ = false;
        return std::nullopt;
    }

    responseClient_ = message.value().first;
    responseClient_->pendingRequest_ = true;

    return message.value().second;
}

std::optional<messages::Message>
Server::ServerImpl::receiveRequest(const std::chrono::milliseconds& timeout) {
    LOGGER.trace("receiveRequest()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return std::nullopt;
    }

    bool f = false;
    if (!pendingRequest_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Can't wait for another request before responding to the previous one");
        return std::nullopt;
    }
    auto message = requestsQueue_.pop(timeout);
    if (!message) {
        pendingRequest_ = false;
        return std::nullopt;
    }

    responseClient_ = message.value().first;
    responseClient_->pendingRequest_ = true;

    return message.value().second;
}

bool Server::ServerImpl::respond(const messages::Message& message) {
    LOGGER.trace("respond()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    if (!pendingRequest_) {
        LOGGER.warn("No request was received, so can't respond");
        return false;
    }

    auto retval = responseClient_->respond(message);
    pendingRequest_ = false;
    responseClient_ = nullptr;

    return retval;
}

bool Server::ServerImpl::subscribe(const std::string& topic,
                                   const std::chrono::milliseconds& maxRate) {
    LOGGER.trace("subscribe()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    std::unique_lock subscriptionGuard(currentSubscriptionMutex_);
    if (topic == "*") {
        subscribedToAll_ = true;
        currentUnsubscriptions_.clear();
    }

    if (std::find(currentSubscriptions_.begin(), currentSubscriptions_.end(), topic) ==
        currentSubscriptions_.end()) {
        currentSubscriptions_.push_back(topic);
        std::shared_lock clientsGuard(connectedClientsMutex_);
        for (auto client : connectedClients_) {
            client->subscribe(topic, maxRate);
        }
    }

    return true;
}

uint32_t Server::ServerImpl::subscriptionsCount(const std::string& topic) {
    uint32_t total = 0;
    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        total += client->subscriptionsCount(topic);
    }

    return total;
}

bool Server::ServerImpl::unsubscribe(const std::string& topic) {
    LOGGER.trace("unsubscribe()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    std::unique_lock subscriptionGuard(currentSubscriptionMutex_);
    if (topic == "*") {
        subscribedToAll_ = false;
        currentUnsubscriptions_.clear();
    }

    if (auto it = std::find(currentSubscriptions_.begin(), currentSubscriptions_.end(), topic);
        it != currentSubscriptions_.end()) {
        currentSubscriptions_.erase(it);
        std::shared_lock clientsGuard(connectedClientsMutex_);
        for (auto client : connectedClients_) {
            client->unsubscribe(topic);
        }
    } else if (subscribedToAll_) {
        std::shared_lock clientsGuard(connectedClientsMutex_);
        for (auto client : connectedClients_) {
            client->unsubscribe(topic);
        }
        currentUnsubscriptions_.push_back(topic);
    }

    return true;
}

bool Server::ServerImpl::keepUpdateHistory(bool value) {
    keepUpdateHistory_ = value;
    return true;
}

std::optional<messages::Message> Server::ServerImpl::receiveUpdate(const std::string& topic) {
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    if (topic == "*") {
        LOGGER.warn("You can subscribe to all topics but not receive from all topics");
        return std::nullopt;
    }

    {
        std::unique_lock guard(onUpdateHandlersMtx_);
        if (updateQueues_.count(topic) == 0) {
            updateQueues_.insert(
                {topic,
                 std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
        }
    }

    return updateQueues_[topic]->pop();
}

std::optional<messages::Message>
Server::ServerImpl::receiveUpdate(const std::string& topic,
                                  const std::chrono::milliseconds& timeout) {
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    if (topic == "*") {
        LOGGER.warn("You can subscribe to all topics but not receive from all topics");
        return std::nullopt;
    }

    {
        std::unique_lock guard(onUpdateHandlersMtx_);
        if (updateQueues_.find(topic) == updateQueues_.end()) {
            updateQueues_.insert(
                {topic,
                 std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
        }
    }

    return updateQueues_[topic]->pop(timeout);
}

bool Server::ServerImpl::onUpdate(
    const std::string& topic,
    const std::function<void(const std::string& topic, const messages::Message&)>& handler) {
    LOGGER.trace("onUpdate()");
    std::unique_lock guard(onUpdateHandlersMtx_);
    if (onUpdateHandlers_.find(topic) != onUpdateHandlers_.end()) {
        onUpdateHandlers_[topic].subscribe(handler);
    } else {
        common::events::event<std::string, messages::Message> event;
        event.subscribe(handler);
        onUpdateHandlers_.insert({topic, event});
    }
    
    return true;
}

bool Server::ServerImpl::publish(const std::string& topic,
                                 const messages::Message& message,
                                 bool requiresAck) {
    LOGGER.trace("publish()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        client->publish(topic, message, requiresAck);
    }
    return true;
}

std::vector<std::string> Server::ServerImpl::availablePartnerTopics() {
    LOGGER.trace("availablePartnerTopics()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::vector<std::string>();
    }

    std::vector<std::string> topics;
    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        auto clientTopics = client->availablePartnerTopics();
        for (auto topic : clientTopics) {
            if (std::find(topics.begin(), topics.end(), topic) == topics.end()) {
                topics.push_back(topic);
            }
        }
    }

    return topics;
}

bool Server::ServerImpl::addTopic(const std::string& topic) {
    LOGGER.trace("addTopic()");

    {
        std::scoped_lock guard(currentSubscriptionMutex_);
        if (std::find(availableTopics_.begin(), availableTopics_.end(), topic) !=
            availableTopics_.end()) {
            LOGGER.warn("Topic already added");
            return false;
        }
        availableTopics_.push_back(topic);
    }

    updateServiceRegistration();

    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        client->addTopic(topic);
    }
    return true;
}

bool Server::ServerImpl::removeTopic(const std::string& topic) {
    LOGGER.trace("removeTopic()");
    {
        std::scoped_lock guard(currentSubscriptionMutex_);
        auto it = std::find(availableTopics_.begin(), availableTopics_.end(), topic);
        if (it == availableTopics_.end()) {
            LOGGER.warn("Topic was not added");
            return false;
        }
        availableTopics_.erase(it);
    }

    {
        std::unique_lock guard(onUpdateHandlersMtx_);
        onUpdateHandlers_.erase(topic);
    }

    updateServiceRegistration();

    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        client->removeTopic(topic);
    }
    return true;
}

bool Server::ServerImpl::automaticallyDeserializeBody(bool value) {
    deserializeBody_ = value;
    std::shared_lock clientsGuard(connectedClientsMutex_);
    for (auto client : connectedClients_) {
        client->automaticallyDeserializeBody(value);
    }
    return true;
}

void Server::ServerImpl::acceptHandler(std::shared_ptr<ITransportSocketServer> server) {
    LOGGER.trace("acceptHandler()");
    threadStartedFlag_++;
    while (!stopThread_) {
        if (!server->isOpen()) {
            break;
        }

        auto rawSocket = server->acceptConnection();
        if (rawSocket == nullptr) {
            LOGGER.warn("Failed to accept client connection");
            continue;
        }

        auto client = std::make_shared<Client::ClientImpl>(rawSocket);
        {
            std::shared_lock guard(currentSubscriptionMutex_);
            client->availableTopics_ = availableTopics_;
        }

        client->onPush(std::bind(&Server::ServerImpl::onPushHandler, this, std::placeholders::_1));
        client->onRequestReceived(std::bind(&Server::ServerImpl::onRequestHandler,
                                            this,
                                            std::placeholders::_1,
                                            std::placeholders::_2));
        client->onUpdate("*",
                         std::bind(&Server::ServerImpl::onUpdateHandler,
                                   this,
                                   std::placeholders::_1,
                                   std::placeholders::_2));
        client->automaticallyDeserializeBody(deserializeBody_.load());
        rawSocket->onConnectionLost(std::bind(
            &Server::ServerImpl::onClientDisconnectHandler, this, client, std::placeholders::_1));

        if (!client->open()) {
            LOGGER.warn("Failed to open client");
            continue;
        }

        {
            std::shared_lock subscriptionGuard(currentSubscriptionMutex_);
            if (!subscribedToAll_) {
                for (auto topic : currentSubscriptions_)
                    client->subscribe(topic, std::chrono::milliseconds(0));
            } else {
                for (auto topic : currentUnsubscriptions_)
                    client->unsubscribe(topic);
                client->subscribe("*", std::chrono::milliseconds(0));
            }
        }

        std::unique_lock clientsGuard(connectedClientsMutex_);
        connectedClients_.push_back(client);
    }

    LOGGER.trace("acceptHandler() exit");
}

void Server::ServerImpl::onPushHandler(const messages::Message& msg) {
    if (pushEvent_) {
        pushEvent_.emit(msg);
    } else {
        pullQueue_.push(msg);
    }
}

void Server::ServerImpl::onRequestHandler(Client::ClientImpl* socket, messages::Message&& msg) {
    requestsQueue_.push({socket, msg});
}

void Server::ServerImpl::onUpdateHandler(const std::string& topic,
                                         const messages::Message& message) {
    {
        std::scoped_lock guard(onUpdateHandlersMtx_);
        if (onUpdateHandlers_.find(topic) != onUpdateHandlers_.end()) {
            onUpdateHandlers_[topic].emit(topic, message);
            return;
        } else if (onUpdateHandlers_.find("*") != onUpdateHandlers_.end()) {
            onUpdateHandlers_["*"].emit(topic, message);
            return;
        }

        if (updateQueues_.count(topic) == 0) {
            updateQueues_.insert(
                {topic,
                 std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
        }
    }

    if (!keepUpdateHistory_) {
        updateQueues_[topic]->clear();
    }

    updateQueues_[topic]->push(std::move(message));
}

void Server::ServerImpl::onClientDisconnectHandler(
    const std::shared_ptr<Client::ClientImpl>& channel, sockets::ITransportSocket*) {
    if (isClosing_)
        return;

    std::unique_lock clientsGuard(connectedClientsMutex_);
    auto it = std::find_if(
        connectedClients_.begin(),
        connectedClients_.end(),
        [&](const std::shared_ptr<Client::ClientImpl>& c) { return (c.get() == channel.get()); });

    if (it != connectedClients_.end()) {
        connectedClients_.erase(it);
    }
}

void Server::ServerImpl::updateServiceRegistration() {
    std::scoped_lock guard(currentSubscriptionMutex_);
    services::LocalServices localServices;
    auto currentRegistration = localServices.getServices()[serviceName_];
    currentRegistration["topics"] = availableTopics_;
    localServices.registerService(serviceName_, currentRegistration);
}

} // namespace sockets
} // namespace middleware
} // namespace urf
