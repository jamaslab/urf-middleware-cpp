#include <algorithm>
#include <iostream>
#include <random>

#include <urf/common/logger/Logger.hpp>

#include "common/messages/ControlPacket.hpp"
#include "common/sockets/impl/ClientImpl.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("Client");

constexpr uint64_t
toEpochMs(const std::chrono::time_point<std::chrono::high_resolution_clock>& time) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch()).count();
}

} // namespace

using urf::middleware::messages::ControlPacket;

namespace urf {
namespace middleware {
namespace sockets {

Client::ClientImpl::ClientImpl(std::shared_ptr<sockets::ITransportSocket> socket)
    : rawSocket_(socket)
    , socket_(new sockets::PacketSocket(rawSocket_))
    , isOpen_(false)
    , deserializeBody_(true)
    , stopThreads_(false)
    , threadStartedFlag_(false)
    , receiveThread_()
    , writerMutex_()
    , pullQueue_()
    , pushEvent_()
    , requestId_(0)
    , pendingResponse_(false)
    , responseQueue_()
    , pendingRequest_(false)
    , requestQueue_()
    , requestIdsQueue_()
    , requestHandlerSet_(false)
    , requestHandler_()
    , keepUpdateHistory_(false)
    , onUpdateHandlersMtx_()
    , onUpdateHandlers_()
    , updateQueues_()
    , subscriptionMutex_()
    , availableTopics_()
    , subscribedToAll_(false)
    , allTopicsMaxRate_(0)
    , partnerSubscriptions_()
    , lastTimestampUpdate_()
    , timestampUpdateFrequency_(std::chrono::seconds(1))
    , timestampDifferenceMs_(0)
    , timestampP_(100)
    , timestampQ_(1e-3)
    , timestampR_(0.1) {
    LOGGER.trace("CTor");
}

Client::ClientImpl::~ClientImpl() {
    LOGGER.trace("DTor");
    close();
}

bool Client::ClientImpl::open() {
    LOGGER.trace("open()");
    if (isOpen()) {
        LOGGER.warn("Socket already opened");
        return false;
    }

    if (!socket_->isOpen() && !socket_->open()) {
        LOGGER.warn("Could not open socket");
        return false;
    }

    pullQueue_ = common::containers::ThreadSafeQueue<messages::Message>();
    requestQueue_ = common::containers::ThreadSafeQueue<messages::Message>();
    requestIdsQueue_ = common::containers::ThreadSafeQueue<uint8_t>();

    stopThreads_ = false;
    threadStartedFlag_ = false;

    timestampDifferenceMs_ = 0;

    receiveThread_ = std::thread(&Client::ClientImpl::receiveHandler, this);
    while (!threadStartedFlag_) {
    };

    requestTimestampSynchronization();

    isOpen_ = true;
    return true;
}

bool Client::ClientImpl::close() {
    LOGGER.trace("close()");
    if (!isOpen()) {
        LOGGER.warn("Socket not open");
        return false;
    }

    stopThreads_ = true;
    {
        std::scoped_lock<std::mutex> guard(writerMutex_);
        if (socket_->isOpen()) {
            socket_->close();
        }
    }

    pullQueue_.dispose();
    responseQueue_.dispose();
    requestQueue_.dispose();
    requestIdsQueue_.dispose();

    {
        std::scoped_lock guard(onUpdateHandlersMtx_);
        for (auto queue : updateQueues_) {
            queue.second->dispose();
        }
    }

    receiveThread_.join();
    {
        std::scoped_lock guard(onUpdateHandlersMtx_);
        updateQueues_.clear();
    }

    isOpen_ = false;
    return true;
}

bool Client::ClientImpl::isOpen() {
    return isOpen_;
}

std::optional<messages::Message> Client::ClientImpl::pull() {
    LOGGER.trace("pull()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    return pullQueue_.pop();
}

std::optional<messages::Message>
Client::ClientImpl::pull(const std::chrono::milliseconds& timeout) {
    LOGGER.trace("pull()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }
    return pullQueue_.pop(timeout);
}

bool Client::ClientImpl::onPush(const std::function<void(const messages::Message&)>& handler) {
    LOGGER.trace("onPush()");
    pushEvent_.subscribe(handler, common::events::event_policy::synchronous);
    return true;
}

bool Client::ClientImpl::push(const messages::Message& message, bool requiresAck) {
    LOGGER.trace("push()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    std::scoped_lock<std::mutex> guard(writerMutex_);
    return socket_->write(ControlPacket(ControlPacket::Patterns::Push), message, requiresAck);
}

std::vector<messages::Message> Client::ClientImpl::request(const messages::Message& message) {
    LOGGER.trace("request()");
    std::vector<messages::Message> retval;
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return retval;
    }
    ControlPacket cp(ControlPacket::Patterns::Request);
    return request(cp, message, false);
}

std::vector<messages::Message>
Client::ClientImpl::request(const messages::Message& message,
                            const std::chrono::milliseconds& timeout) {
    LOGGER.trace("request()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::vector<messages::Message>();
    }

    ControlPacket cp(ControlPacket::Patterns::Request);
    return request(cp, message, true, timeout);
}

std::future<std::vector<messages::Message>>
Client::ClientImpl::requestAsync(const messages::Message& message) {
    LOGGER.trace("requestAsync()");
    return std::async(std::launch::async, [this, &message]() { return request(message); });
}
std::future<std::vector<messages::Message>>
Client::ClientImpl::requestAsync(const messages::Message& message,
                                 const std::chrono::milliseconds& timeout) {
    LOGGER.trace("requestAsync()");
    return std::async(std::launch::async,
                      [this, &message, &timeout]() { return request(message, timeout); });
}

std::optional<messages::Message> Client::ClientImpl::receiveRequest() {
    LOGGER.trace("receiveRequest()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    bool f = false;
    if (!pendingRequest_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Can't wait for another request before responding to the previous one");
        return std::nullopt;
    }

    auto message = requestQueue_.pop();
    if (!message) {
        pendingRequest_ = false;
        return std::nullopt;
    }
    return message;
}

std::optional<messages::Message>
Client::ClientImpl::receiveRequest(const std::chrono::milliseconds& timeout) {
    LOGGER.trace("receiveRequest()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    bool f = false;
    if (!pendingRequest_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Can't wait for another request before responding to the previous one");
        return std::nullopt;
    }

    auto message = requestQueue_.pop(timeout);
    if (!message) {
        LOGGER.warn("Timeout while waiting for request");
        pendingRequest_ = false;
        return std::nullopt;
    }
    return message;
}

bool Client::ClientImpl::onRequestReceived(
    const std::function<void(Client::ClientImpl*, messages::Message&&)>& handler) {
    LOGGER.trace("onRequestReceived()");
    requestHandlerSet_ = true;
    requestHandler_ = handler;
    return true;
}

bool Client::ClientImpl::respond(const messages::Message& message) {
    LOGGER.trace("respond()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    bool f = true;
    if (!pendingRequest_.compare_exchange_strong(f, false)) {
        LOGGER.warn("No request was received, so can't respond");
        return false;
    }

    ControlPacket cp(ControlPacket::Patterns::Response, requestIdsQueue_.pop().value());
    std::scoped_lock<std::mutex> guard(writerMutex_);
    return socket_->write(cp, message, true);
}

bool Client::ClientImpl::subscribe(const std::string& topic,
                                   const std::chrono::milliseconds& maxRate) {
    LOGGER.trace("subscribe()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    messages::Message msg;
    ControlPacket control(ControlPacket::Patterns::Request, ControlPacket::Patterns::Subscribe);
    control.setTopic(topic);

    msg["_maxRate"] = maxRate.count();

    auto response = request(control, msg, false);
    if (response.empty())
        return false;

    if (!deserializeBody_ && !response[0].body().deserialize()) {
        LOGGER.warn("Failed to deserialize body");
        return false;
    }

    try {
        return response[0]["_result"].get<bool>();
    } catch (const std::exception&) {
        return false;
    }
}

uint32_t Client::ClientImpl::subscriptionsCount(const std::string& topic) {
    return partnerSubscriptions_.find(topic) == partnerSubscriptions_.end() ? 0 : 1;
}

bool Client::ClientImpl::unsubscribe(const std::string& topic) {
    LOGGER.trace("unsubscribe()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    messages::Message msg;
    ControlPacket control(ControlPacket::Patterns::Request, ControlPacket::Patterns::Unsubscribe);
    control.setTopic(topic);

    auto response = request(control, msg, false);
    if (response.empty())
        return false;

    if (!deserializeBody_ && !response[0].body().deserialize()) {
        LOGGER.warn("Failed to deserialize body");
        return false;
    }

    try {
        return response[0]["_result"].get<bool>();
    } catch (const std::exception&) {
        return false;
    }
}

bool Client::ClientImpl::keepUpdateHistory(bool value) {
    LOGGER.trace("keepUpdateHistory()");
    keepUpdateHistory_ = value;
    return true;
}

std::optional<messages::Message> Client::ClientImpl::receiveUpdate(const std::string& topic) {
    LOGGER.trace("receiveUpdate()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    if (topic == "*") {
        LOGGER.warn("You can subscribe to all topics but not receive from all topics");
        return std::nullopt;
    }

    {
        std::scoped_lock<std::mutex> guard(onUpdateHandlersMtx_);
        if (updateQueues_.find(topic) == updateQueues_.end()) {
            updateQueues_.insert(
                {topic,
                 std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
        }
    }

    return updateQueues_[topic]->pop();
}

std::optional<messages::Message>
Client::ClientImpl::receiveUpdate(const std::string& topic,
                                  const std::chrono::milliseconds& timeout) {
    LOGGER.trace("receiveUpdate()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::nullopt;
    }

    if (topic == "*") {
        LOGGER.warn("You can subscribe to all topics but not receive from all topics");
        return std::nullopt;
    }

    {
        std::scoped_lock<std::mutex> guard(onUpdateHandlersMtx_);
        if (updateQueues_.find(topic) == updateQueues_.end()) {
            updateQueues_.insert(
                {topic,
                 std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
        }
    }

    return updateQueues_[topic]->pop(timeout);
}

bool Client::ClientImpl::onUpdate(
    const std::string& topic,
    const std::function<void(const std::string&, const messages::Message&)>& handler) {
    LOGGER.trace("onUpdate()");
    std::scoped_lock<std::mutex> guard(onUpdateHandlersMtx_);
    if (onUpdateHandlers_.find(topic) != onUpdateHandlers_.end()) {
        onUpdateHandlers_[topic].subscribe(handler);
    } else {
        common::events::event<std::string, messages::Message> event;
        event.subscribe(handler);
        onUpdateHandlers_.insert({topic, event});
    }

    return true;
}

bool Client::ClientImpl::publish(const std::string& topic,
                                 const messages::Message& message,
                                 bool requiresAck) {
    LOGGER.trace("publish()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return false;
    }

    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    auto it = partnerSubscriptions_.find(topic);
    if (it == partnerSubscriptions_.end()) {
        return true;
    }

    auto now = std::chrono::high_resolution_clock::now();
    auto& ps = it->second;
    if ((now - ps.lastSent) < ps.maxRate) {
        return true;
    }

    ps.lastSent = now;

    ControlPacket control(ControlPacket::Patterns::Publish, topic);
    std::scoped_lock<std::mutex> writerGuard(writerMutex_);
    return socket_->write(control, message, requiresAck);
}

std::vector<std::string> Client::ClientImpl::availablePartnerTopics() {
    LOGGER.trace("availablePartnerTopics()");
    if (!isOpen()) {
        LOGGER.warn("Socket is not open");
        return std::vector<std::string>();
    }

    messages::Message msg;
    ControlPacket control(ControlPacket::Patterns::Request, ControlPacket::Patterns::TopicList);

    auto response = request(control, msg, false);
    if (response.empty())
        return std::vector<std::string>();

    if (!deserializeBody_ && !response[0].body().deserialize()) {
        LOGGER.warn("Failed to deserialize body");
        return std::vector<std::string>();
    }

    try {
        return response[0]["_topics"].get<std::vector<std::string>>();
    } catch (const std::exception& ex) {
        LOGGER.warn("Failed to return topics: {}", ex.what());
        return std::vector<std::string>();
    }
}

bool Client::ClientImpl::addTopic(const std::string& topic) {
    LOGGER.trace("addTopic()");
    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    if (std::find(availableTopics_.begin(), availableTopics_.end(), topic) !=
        availableTopics_.end()) {
        LOGGER.warn("Topic already added");
        return false;
    }
    availableTopics_.push_back(topic);

    if (subscribedToAll_) {
        PartnerSubscription subscription;
        subscription.maxRate = allTopicsMaxRate_;
        partnerSubscriptions_.insert({topic, subscription});
    }

    return true;
}

bool Client::ClientImpl::removeTopic(const std::string& topic) {
    LOGGER.trace("removeTopic()");
    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    if (auto it = std::find(availableTopics_.begin(), availableTopics_.end(), topic);
        it != availableTopics_.end()) {
        availableTopics_.erase(it);

        {
            std::scoped_lock<std::mutex> guard(onUpdateHandlersMtx_);
            onUpdateHandlers_.erase(topic);
        }

        return true;
    }
    LOGGER.warn("Topic was not added");
    return false;
}

bool Client::ClientImpl::automaticallyDeserializeBody(bool value) {
    deserializeBody_ = value;
    socket_->deserializeBody(value);
    return true;
}

void Client::ClientImpl::receiveHandler() {
    LOGGER.trace("receiveHandler()");

    threadStartedFlag_ = true;

    while (!stopThreads_) {
        auto messageOpt = socket_->read();
        if (!socket_->isOpen())
            break;
        if (!messageOpt)
            continue;

        auto [control, message] = messageOpt.value();
        auto delay = static_cast<double>(message.header().timestampReceived()) -
                     static_cast<double>(message.header().timestamp()) + timestampDifferenceMs_;

        if (delay < 0) {
            timestampDifferenceMs_ -= delay;
        } else {
            message.header().delay(static_cast<uint64_t>(delay));
        }

        if (control.getPattern() == ControlPacket::Patterns::Push) {
            if (pushEvent_) {
                pushEvent_.emit(message);
                continue;
            }

            pullQueue_.push(std::move(message));
        } else if (control.getPattern() == ControlPacket::Patterns::Response) {
            if (!pendingResponse_)
                continue;

            if (control.getRequestId() != requestId_) {
                continue;
            }

            responseQueue_.push(std::move(message));
        } else if (control.getPattern() == ControlPacket::Patterns::Request) {
            if (control.getSubpattern() == ControlPacket::Patterns::Subscribe) {
                handleSubscription(control, message);
            } else if (control.getSubpattern() == ControlPacket::Patterns::Unsubscribe) {
                handleUnsubscription(control);
            } else if (control.getSubpattern() == ControlPacket::Patterns::TopicList) {
                handleTopicListRequest(control);
            } else {
                requestIdsQueue_.push(control.getRequestId());
                if (requestHandlerSet_) {
                    requestHandler_(this, std::move(message));
                } else {
                    requestQueue_.push(std::move(message));
                }
            }
        } else if (control.getPattern() == ControlPacket::Patterns::Publish) {
            if (!control.hasTopic()) {
                LOGGER.warn("Missing topic in message");
                continue;
            }

            auto topic = control.getTopic();
            std::scoped_lock<std::mutex> guard(onUpdateHandlersMtx_);
            if (onUpdateHandlers_.find(topic) != onUpdateHandlers_.end()) {
                onUpdateHandlers_[topic].emit(topic, message);
                continue;
            } else if (onUpdateHandlers_.find("*") != onUpdateHandlers_.end()) {
                onUpdateHandlers_["*"].emit(topic, message);
                continue;
            }

            if (updateQueues_.find(topic) == updateQueues_.end()) {
                updateQueues_.insert(
                    {topic,
                     std::make_shared<common::containers::ThreadSafeQueue<messages::Message>>()});
            }

            if (!keepUpdateHistory_) {
                updateQueues_[topic]->clear();
            }
            updateQueues_[topic]->push(std::move(message));
        } else if (control.getPattern() == messages::ControlPacket::Patterns::Timestamp) {
            if (control.getSubpattern() == messages::ControlPacket::Patterns::Request) {
                messages::Message msg;
                msg.header().timestamp(toEpochMs(std::chrono::high_resolution_clock::now()));

                std::scoped_lock<std::mutex> guard(writerMutex_);
                socket_->write(messages::ControlPacket(messages::ControlPacket::Patterns::Timestamp,
                                                       messages::ControlPacket::Patterns::Response),
                               msg,
                               true);
            } else if (control.getSubpattern() == messages::ControlPacket::Patterns::Response) {
                auto now =
                    static_cast<double>(toEpochMs(std::chrono::high_resolution_clock::now()));
                auto rtt = now - static_cast<double>(toEpochMs(lastTimestampUpdate_));
                auto serverTimestamp = static_cast<double>(message.header().timestamp());
                auto estimatedOffset = serverTimestamp + rtt / 2 - now;
                if (timestampDifferenceMs_ == 0) {
                    timestampDifferenceMs_ = estimatedOffset;
                } else {
                    auto k = timestampP_ / (timestampP_ + timestampR_);
                    auto statePred =
                        timestampDifferenceMs_ + k * (estimatedOffset - timestampDifferenceMs_);
                    timestampP_ = (1 - k) * timestampP_ +
                                  abs(timestampDifferenceMs_ - statePred) * timestampQ_;
                    timestampDifferenceMs_ = statePred;
                }
            }
        }

        // Send timestamp synchronization if 5 seconds passed
        if (std::chrono::high_resolution_clock::now() - lastTimestampUpdate_ >
            timestampUpdateFrequency_) {
            requestTimestampSynchronization();
        }
    }

    // Wake-up all the pending requests
    pullQueue_.notifyAll();
    responseQueue_.notifyAll();
    requestQueue_.notifyAll();

    for (auto queue : updateQueues_) {
        queue.second->notifyAll();
    }

    LOGGER.trace("receiveHandler() exit");
}

std::vector<messages::Message>
Client::ClientImpl::request(ControlPacket& control,
                            const messages::Message& message,
                            bool hasTimeout,
                            const std::chrono::milliseconds& timeout) {
    std::vector<messages::Message> retval;
    bool f = false;
    if (!pendingResponse_.compare_exchange_strong(f, true)) {
        LOGGER.warn("Socket is still waiting for a response, can't send a new request");
        return retval;
    }

    requestId_++;
    control.setRequestId(requestId_);

    {
        std::scoped_lock<std::mutex> guard(writerMutex_);
        if (!socket_->write(control, message, true)) {
            LOGGER.warn("Failed to write request");
            pendingResponse_ = false;
            return retval;
        }
    }

    auto response = hasTimeout ? responseQueue_.pop(timeout) : responseQueue_.pop();
    if (response)
        retval.push_back(response.value());
    pendingResponse_ = false;
    return retval;
}

void Client::ClientImpl::handleSubscription(ControlPacket& packet, messages::Message& msg) {
    messages::Message response;

    std::string topic = packet.getTopic();
    msg.body().deserialize();

    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    if (topic == "*") {
        partnerSubscriptions_.clear();
        subscribedToAll_ = true;
        allTopicsMaxRate_ = std::chrono::milliseconds(msg["_maxRate"].get<int>());

        for (auto topic : availableTopics_) {
            PartnerSubscription subscr;
            subscr.maxRate = std::chrono::milliseconds(msg["_maxRate"].get<int>());
            subscr.lastSent = std::chrono::time_point<std::chrono::high_resolution_clock>();
            partnerSubscriptions_.insert({topic, subscr});
        }
        response["_result"] = true;
    } else if (std::find(availableTopics_.begin(), availableTopics_.end(), topic) ==
               availableTopics_.end()) {
        response["_result"] = false;
        response["_error"] = "Requested topic does not exists";
    } else {
        msg.deserialize();
        PartnerSubscription subscr;
        subscr.maxRate = std::chrono::milliseconds(msg["_maxRate"].get<int>());
        subscr.lastSent = std::chrono::time_point<std::chrono::high_resolution_clock>();

        if (partnerSubscriptions_.count(topic) != 0) {
            partnerSubscriptions_[topic] = subscr;
        } else {
            partnerSubscriptions_.insert({topic, subscr});
        }

        response["_result"] = true;
    }

    packet.setPattern(ControlPacket::Patterns::Response);
    packet.setSubpattern(ControlPacket::Patterns::Subscribe);

    std::scoped_lock<std::mutex> writerGuard(writerMutex_);
    socket_->write(packet, response, true);
}

void Client::ClientImpl::handleUnsubscription(ControlPacket& packet) {
    messages::Message response;
    std::string topic = packet.getTopic();

    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    if (topic == "*") {
        partnerSubscriptions_.clear();
    } else if (auto it = partnerSubscriptions_.find(topic); it != partnerSubscriptions_.end()) {
        partnerSubscriptions_.erase(it);
    }

    subscribedToAll_ = false;
    response["_result"] = true;
    packet.setPattern(ControlPacket::Patterns::Response);
    packet.setSubpattern(ControlPacket::Patterns::Unsubscribe);

    std::scoped_lock<std::mutex> writerGuard(writerMutex_);
    socket_->write(packet, response, true);
}

void Client::ClientImpl::handleTopicListRequest(ControlPacket& controlPacket) {
    std::scoped_lock<std::mutex> guard(subscriptionMutex_);
    messages::Message response;
    response["_topics"] = availableTopics_;
    std::scoped_lock<std::mutex> writerGuard(writerMutex_);
    controlPacket.setPattern(ControlPacket::Patterns::Response);
    socket_->write(controlPacket, response, true);
}

void Client::ClientImpl::requestTimestampSynchronization() {
    std::scoped_lock<std::mutex> guard(writerMutex_);
    lastTimestampUpdate_ = std::chrono::high_resolution_clock::now();
    socket_->write(messages::ControlPacket(messages::ControlPacket::Patterns::Timestamp,
                                           messages::ControlPacket::Patterns::Request),
                   messages::Message(),
                   true);
}

} // namespace sockets
} // namespace middleware
} // namespace urf
