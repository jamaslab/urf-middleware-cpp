#pragma once

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <memory>
#include <mutex>
#include <optional>
#include <queue>
#include <string>
#include <thread>
#include <unordered_map>

#include "urf/middleware/messages/Message.hpp"
#include "urf/middleware/sockets/Client.hpp"
#include "urf/middleware/sockets/ISocket.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>
#include <urf/common/events/events.hpp>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/PacketSocket.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class Client::ClientImpl {
 public:
    ClientImpl() = delete;
    explicit ClientImpl(std::shared_ptr<sockets::ITransportSocket> socket);
    ClientImpl(const ClientImpl&) = delete;
    ClientImpl(ClientImpl&&) = delete;
    ~ClientImpl();

    bool open();
    bool close();
    bool isOpen();

    std::optional<messages::Message> pull();
    std::optional<messages::Message> pull(const std::chrono::milliseconds& timeout);

    bool push(const messages::Message& message, bool requiresAck = true);
    bool onPush(const std::function<void(const messages::Message&)>&);

    std::vector<messages::Message> request(const messages::Message& message);
    std::vector<messages::Message> request(const messages::Message& message,
                                           const std::chrono::milliseconds& timeout);

    std::future<std::vector<messages::Message>> requestAsync(const messages::Message& message);
    std::future<std::vector<messages::Message>>
    requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout);

    std::optional<messages::Message> receiveRequest();
    std::optional<messages::Message> receiveRequest(const std::chrono::milliseconds& timeout);

    bool respond(const messages::Message& message);

    bool subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate);
    uint32_t subscriptionsCount(const std::string& topic);
    bool unsubscribe(const std::string& topic);

    bool keepUpdateHistory(bool value);
    std::optional<messages::Message> receiveUpdate(const std::string& topic);
    std::optional<messages::Message> receiveUpdate(const std::string& topic,
                                                   const std::chrono::milliseconds& timeout);
    bool onUpdate(const std::string& topic,
                  const std::function<void(const std::string& topic, const messages::Message&)>&);

    bool
    publish(const std::string& topic, const messages::Message& message, bool requiresAck = true);
    std::vector<std::string> availablePartnerTopics();

    bool addTopic(const std::string& topic);
    bool removeTopic(const std::string& topic);

    bool automaticallyDeserializeBody(bool value);

 private:
    struct PartnerSubscription {
        std::chrono::milliseconds maxRate;
        std::chrono::time_point<std::chrono::high_resolution_clock> lastSent;
    };

 private:
    void receiveHandler();
    bool onRequestReceived(const std::function<void(Client::ClientImpl*, messages::Message&&)>&);

    void handleSubscription(messages::ControlPacket& control, messages::Message& msg);
    void handleUnsubscription(messages::ControlPacket& message);
    void handleTopicListRequest(messages::ControlPacket& controlPacket);

    void requestTimestampSynchronization();

    std::vector<messages::Message>
    request(messages::ControlPacket& control,
            const messages::Message& message,
            bool hasTimeout,
            const std::chrono::milliseconds& timeout = std::chrono::milliseconds(0));

 private:
    friend class Server::ServerImpl;

    std::shared_ptr<sockets::ITransportSocket> rawSocket_;
    std::unique_ptr<sockets::PacketSocket> socket_;

    bool isOpen_;

    // Socket operations members
    std::atomic<bool> deserializeBody_;
    std::atomic<bool> stopThreads_;
    std::atomic<bool> threadStartedFlag_;
    std::thread receiveThread_;

    std::mutex writerMutex_;

    // Push-Pull members
    common::containers::ThreadSafeQueue<messages::Message> pullQueue_;
    common::events::event<messages::Message> pushEvent_;

    // Request-Response members
    uint8_t requestId_;
    std::atomic<bool> pendingResponse_;
    common::containers::ThreadSafeQueue<messages::Message> responseQueue_;

    std::atomic<bool> pendingRequest_;
    common::containers::ThreadSafeQueue<messages::Message> requestQueue_;
    common::containers::ThreadSafeQueue<uint8_t> requestIdsQueue_;

    std::atomic<bool> requestHandlerSet_;
    std::function<void(Client::ClientImpl*, messages::Message&&)> requestHandler_;

    // Publish-Subscribe members
    std::atomic<bool> keepUpdateHistory_;
    std::mutex onUpdateHandlersMtx_;
    std::unordered_map<std::string, common::events::event<std::string, messages::Message>>
        onUpdateHandlers_;
    std::unordered_map<std::string,
                       std::shared_ptr<common::containers::ThreadSafeQueue<messages::Message>>>
        updateQueues_;

    std::mutex subscriptionMutex_;
    std::vector<std::string> availableTopics_;

    bool subscribedToAll_;
    std::chrono::milliseconds allTopicsMaxRate_;
    std::unordered_map<std::string, PartnerSubscription> partnerSubscriptions_;

    // Timestamp synchronization members
    std::chrono::time_point<std::chrono::high_resolution_clock> lastTimestampUpdate_;
    std::chrono::milliseconds timestampUpdateFrequency_;
    double timestampDifferenceMs_;
    double timestampP_;
    double timestampQ_;
    double timestampR_;
};

} // namespace sockets
} // namespace middleware
} // namespace urf
