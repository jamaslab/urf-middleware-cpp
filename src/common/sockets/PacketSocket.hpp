#pragma once

#include <chrono>
#include <memory>
#include <mutex>
#include <optional>
#include <string>
#include <utility>

#include "urf/middleware/messages/Message.hpp"
#include "common/sockets/ITransportSocket.hpp"
#include "common/messages/ControlPacket.hpp"
#include "common/VectorStreambuf.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class PacketSocket {
 public:
    PacketSocket() = delete;
    explicit PacketSocket(std::shared_ptr<ITransportSocket> socket);
    PacketSocket(const PacketSocket&) = delete;
    PacketSocket(PacketSocket&&) = delete;
    ~PacketSocket();

    bool open();
    bool close();

    bool isOpen();

    bool write(const messages::ControlPacket& control, const messages::Message& packet, bool requiresAck = true);

    std::optional<std::tuple<messages::ControlPacket, messages::Message>> read();
    std::optional<std::tuple<messages::ControlPacket, messages::Message>>
        read(const std::chrono::milliseconds& timeout);

    std::shared_ptr<ITransportSocket> transportSocket();

    void deserializeBody(bool value);

 private:
    std::vector<uint8_t> separator_;
    std::shared_ptr<ITransportSocket> socket_;

    std::mutex outputBufferMtx_;
    VectorStreambuf outputBuffer_;

    bool deserializeBody_;

    bool syncLost_;
    bool resync(bool blocking, const std::chrono::milliseconds& timeout);

    std::optional<std::tuple<messages::ControlPacket, messages::Message>> parsePacket(const std::vector<uint8_t> bytes);
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
