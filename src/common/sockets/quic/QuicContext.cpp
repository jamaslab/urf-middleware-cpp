#include "common/sockets/quic/QuicContext.hpp"

#include <iostream>
#include <stdexcept>

namespace urf {
namespace middleware {
namespace sockets {

const QUIC_REGISTRATION_CONFIG REGCONFIG = {"urfquic", QUIC_EXECUTION_PROFILE_TYPE_MAX_THROUGHPUT};

QUIC_API_TABLE* QuicContext::_msQuic = nullptr;
HQUIC QuicContext::_registration;
uint32_t QuicContext::_refCount = 0;
std::mutex QuicContext::_mtx;

QuicContext::QuicContext() {
    std::scoped_lock lock(_mtx);
    if (_msQuic == nullptr) {
        if (QUIC_FAILED(MsQuicOpen2(const_cast<const QUIC_API_TABLE**>(&_msQuic)))) {
            throw std::runtime_error("Could not open QUIC context");
        }
    }

    if (_registration == nullptr) {
        if (QUIC_FAILED(_msQuic->RegistrationOpen(&REGCONFIG, &_registration))) {
            throw std::runtime_error("Could not open QUIC registration");
        }
    }

    _refCount++;
}

QuicContext::~QuicContext() {
    std::scoped_lock lock(_mtx);
    _refCount--;

    if (_refCount == 0) {
        if (_msQuic != nullptr) {
            if (_registration != nullptr) {
                _msQuic->RegistrationClose(_registration);
                _registration = nullptr;
            }

            MsQuicClose(_msQuic);
            _msQuic = nullptr;
        }
    }
}

QUIC_API_TABLE* QuicContext::api() {
    return _msQuic;
}

HQUIC QuicContext::registration() {
    return _registration;
}

} // namespace sockets
} // namespace middleware
} // namespace urf
