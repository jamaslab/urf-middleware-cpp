#pragma once

#include <chrono>
#include <memory>
#include <functional>
#include <mutex>
#include <string>
#include <vector>

#include "common/sockets/ITransportSocketServer.hpp"

#include <msquic.h>

namespace urf {
namespace middleware {
namespace sockets {

class QuicContext {
 public:
    QuicContext();
    ~QuicContext();

    QUIC_API_TABLE* api();
    HQUIC registration();

 private:
    static QUIC_API_TABLE* _msQuic;
    static HQUIC _registration;
    static uint32_t _refCount;
    static std::mutex _mtx;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
