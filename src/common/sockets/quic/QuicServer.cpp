#include "common/sockets/quic/QuicServer.hpp"
#include "common/sockets/quic/QuicSocket.hpp"

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("QuicServer");

const QUIC_BUFFER ALPN = {sizeof("urf") - 1, (uint8_t*)"urf"};
const int IdleTimeoutMs = 1000;

QUIC_STATUS serverListenerCallback(HQUIC, void* context, QUIC_LISTENER_EVENT* event) {
    QUIC_STATUS status = QUIC_STATUS_NOT_SUPPORTED;
    auto srvCtx = reinterpret_cast<urf::middleware::sockets::QuicServerContext*>(context);
    switch (event->Type) {
    case QUIC_LISTENER_EVENT_NEW_CONNECTION:
        {
            srvCtx->connectionQueue.push(event->NEW_CONNECTION.Connection);
            auto statusOpt = srvCtx->connectionStatusQueue.pop(std::chrono::milliseconds(IdleTimeoutMs));
            if (!statusOpt) {
                status = QUIC_STATUS_USER_CANCELED;
            } else {
                status = statusOpt.value();
            }
        }
        break;
    default:
        break;
    }
    return status;
}

} // namespace

namespace urf {
namespace middleware {
namespace sockets {

QuicServer::QuicServer(uint16_t port, const std::string& certificateFilePath, const std::string& privateKeyFilePath, const std::string& privateKeyPassword)
    : port_(port)
    , certificateFilePath_(certificateFilePath)
    , privateKeyFilePath_(privateKeyFilePath)
    , privateKeyPassword_(privateKeyPassword)
    , quicCtx_(new QuicContext)
    , serverCtx_(new QuicServerContext)
    , isOpen_(false)
    , connectedClients_() {
    LOGGER.trace("CTor");
}

QuicServer::~QuicServer() {
    LOGGER.trace("DTor");
    if (isOpen_)
        close();
}

bool QuicServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    serverCtx_->connectionQueue = common::containers::ThreadSafeQueue<HQUIC>();
    serverCtx_->connectionStatusQueue = common::containers::ThreadSafeQueue<QUIC_STATUS>();

    QUIC_SETTINGS settings = {0};
    //
    // Configures the server's idle timeout.
    //
    settings.IdleTimeoutMs = IdleTimeoutMs;
    settings.IsSet.IdleTimeoutMs = true;
    //
    // Configures the server's settings to allow for the peer to open a single
    // bidirectional stream. By default connections are not configured to allow
    // any streams from the peer.
    //
    settings.PeerBidiStreamCount = 1;
    settings.IsSet.PeerBidiStreamCount = true;
    //
    // Configures the server's to accept unreliable datagrams
    //
    settings.DatagramReceiveEnabled = 1;
    settings.IsSet.DatagramReceiveEnabled = TRUE;

    if (QUIC_FAILED(quicCtx_->api()->ConfigurationOpen(quicCtx_->registration(),
                                                       &ALPN,
                                                       1,
                                                       &settings,
                                                       sizeof(settings),
                                                       NULL,
                                                       &serverCtx_->configuration))) {
        LOGGER.warn("Failed to open configuration");
        serverCtx_->configuration = nullptr;
        return false;
    }

    if (!privateKeyPassword_.empty()) {
        QUIC_CERTIFICATE_FILE_PROTECTED certFileProtected;
        std::memset(&certFileProtected, 0, sizeof(certFileProtected));
        certFileProtected.CertificateFile = certificateFilePath_.c_str();
        certFileProtected.PrivateKeyFile = privateKeyFilePath_.c_str();
        certFileProtected.PrivateKeyPassword = privateKeyPassword_.c_str();

        QUIC_CREDENTIAL_CONFIG credConfig;
        memset(&credConfig, 0, sizeof(credConfig));
        credConfig.Flags = QUIC_CREDENTIAL_FLAG_NONE;
        credConfig.Type = QUIC_CREDENTIAL_TYPE_CERTIFICATE_FILE_PROTECTED;
        credConfig.CertificateFileProtected = &certFileProtected;

        if (QUIC_FAILED(
                quicCtx_->api()->ConfigurationLoadCredential(serverCtx_->configuration, &credConfig))) {
            LOGGER.warn("Failed to configuration load protected credential");

            quicCtx_->api()->ConfigurationClose(serverCtx_->configuration);
            serverCtx_->configuration = nullptr;

            return false;
        }
    } else {
        QUIC_CERTIFICATE_FILE certFile;
        std::memset(&certFile, 0, sizeof(certFile));
        certFile.CertificateFile = certificateFilePath_.c_str();
        certFile.PrivateKeyFile = privateKeyFilePath_.c_str();

        QUIC_CREDENTIAL_CONFIG credConfig;
        memset(&credConfig, 0, sizeof(credConfig));
        credConfig.Flags = QUIC_CREDENTIAL_FLAG_NONE;
        credConfig.Type = QUIC_CREDENTIAL_TYPE_CERTIFICATE_FILE;
        credConfig.CertificateFile = &certFile;

        if (QUIC_FAILED(
                quicCtx_->api()->ConfigurationLoadCredential(serverCtx_->configuration, &credConfig))) {
            LOGGER.warn("Failed to configuration load credential");
            quicCtx_->api()->ConfigurationClose(serverCtx_->configuration);
            serverCtx_->configuration = nullptr;

            return false;
        }
    }

    if (QUIC_FAILED(quicCtx_->api()->ListenerOpen(quicCtx_->registration(),
                                                  serverListenerCallback,
                                                  serverCtx_.get(),
                                                  &serverCtx_->listener))) {
        LOGGER.warn("Failed to open listener");
        quicCtx_->api()->ListenerClose(serverCtx_->listener);
        serverCtx_->listener = nullptr;

        quicCtx_->api()->ConfigurationClose(serverCtx_->configuration);
        serverCtx_->configuration = nullptr;

        return false;
    }

    QUIC_ADDR addr;
    QuicAddrSetFamily(&addr, QUIC_ADDRESS_FAMILY_UNSPEC);
    QuicAddrSetPort(&addr, port_);

    if (QUIC_FAILED(quicCtx_->api()->ListenerStart(serverCtx_->listener, &ALPN, 1, &addr))) {
        LOGGER.warn("Failed to start listener");
        quicCtx_->api()->ListenerClose(serverCtx_->listener);
        serverCtx_->listener = nullptr;

        quicCtx_->api()->ConfigurationClose(serverCtx_->configuration);
        serverCtx_->configuration = nullptr;
        return false;
    }

    isOpen_ = true;
    return true;
}

bool QuicServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (auto client : connectedClients_) {
        client->close();
    }

    serverCtx_->connectionQueue.dispose();
    serverCtx_->connectionStatusQueue.dispose();

    if (serverCtx_->listener != nullptr) {
        quicCtx_->api()->ListenerStop(serverCtx_->listener);
        quicCtx_->api()->ListenerClose(serverCtx_->listener);
        serverCtx_->listener = nullptr;
    }

    if (serverCtx_->configuration != nullptr) {
        quicCtx_->api()->ConfigurationClose(serverCtx_->configuration);
        serverCtx_->configuration = nullptr;
    }

    isOpen_ = false;
    return true;
}

bool QuicServer::isOpen() {
    return isOpen_;
}

std::shared_ptr<ITransportSocket> QuicServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    auto connection = serverCtx_->connectionQueue.pop();
    if (!connection) {
        return nullptr;
    }

    std::shared_ptr<ITransportSocket> sockPtr(
        new QuicSocket(port_, connection.value(), serverCtx_->configuration));
    auto quicSocket = std::dynamic_pointer_cast<QuicSocket>(sockPtr);
    auto status = quicSocket->quicOpen();
    serverCtx_->connectionStatusQueue.push(status);

    if (QUIC_FAILED(status)) {
        LOGGER.warn("Failed to accept connection");
        return nullptr;
    }

    for (int i = 0; i < 200; i++) {
        if (quicSocket->socketCtx_->isStreamStarted) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (!quicSocket->socketCtx_->isStreamStarted) {
        LOGGER.warn("Client did not open stream on time");
        return nullptr;
    }
    
    connectedClients_.push_back(sockPtr);

    LOGGER.info("Client connected on {}", port_);
    return sockPtr;
}

bool QuicServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>&) {
    return false;
}

} // namespace sockets
} // namespace middleware
} // namespace urf
