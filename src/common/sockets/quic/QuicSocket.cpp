#include "common/sockets/quic/QuicSocket.hpp"

#include <urf/common/logger/Logger.hpp>

#include <iostream>

namespace {
auto LOGGER = urf::common::getLoggerInstance("QuicSocket");

const char* QuicStatusToString(QUIC_STATUS Status) {
    switch (Status) {
    case QUIC_STATUS_SUCCESS:
        return "SUCCESS";
    case QUIC_STATUS_PENDING:
        return "PENDING";
    case QUIC_STATUS_OUT_OF_MEMORY:
        return "OUT_OF_MEMORY";
    case QUIC_STATUS_INVALID_PARAMETER:
        return "INVALID_PARAMETER";
    case QUIC_STATUS_INVALID_STATE:
        return "INVALID_STATE";
    case QUIC_STATUS_NOT_SUPPORTED:
        return "NOT_SUPPORTED";
    case QUIC_STATUS_NOT_FOUND:
        return "NOT_FOUND";
    case QUIC_STATUS_BUFFER_TOO_SMALL:
        return "BUFFER_TOO_SMALL";
    case QUIC_STATUS_HANDSHAKE_FAILURE:
        return "HANDSHAKE_FAILURE";
    case QUIC_STATUS_ABORTED:
        return "ABORTED";
    case QUIC_STATUS_ADDRESS_IN_USE:
        return "ADDRESS_IN_USE";
    case QUIC_STATUS_CONNECTION_TIMEOUT:
        return "CONNECTION_TIMEOUT";
    case QUIC_STATUS_CONNECTION_IDLE:
        return "CONNECTION_IDLE";
    case QUIC_STATUS_UNREACHABLE:
        return "UNREACHABLE";
    case QUIC_STATUS_INTERNAL_ERROR:
        return "INTERNAL_ERROR";
    case QUIC_STATUS_CONNECTION_REFUSED:
        return "CONNECTION_REFUSED";
    case QUIC_STATUS_PROTOCOL_ERROR:
        return "PROTOCOL_ERROR";
    case QUIC_STATUS_VER_NEG_ERROR:
        return "VER_NEG_ERROR";
    case QUIC_STATUS_USER_CANCELED:
        return "USER_CANCELED";
    case QUIC_STATUS_ALPN_NEG_FAILURE:
        return "ALPN_NEG_FAILURE";
    case QUIC_STATUS_STREAM_LIMIT_REACHED:
        return "STREAM_LIMIT_REACHED";
    }

    return "UNKNOWN";
}

const QUIC_BUFFER ALPN = {sizeof("urf") - 1, (uint8_t*)"urf"};
const int IdleTimeoutMs = 1000;

QUIC_STATUS streamCallback(HQUIC, void* context, QUIC_STREAM_EVENT* event) {
    auto ctx = reinterpret_cast<urf::middleware::sockets::QuicSocketContext*>(context);
    urf::middleware::sockets::QuicContext quicCtx;

    if (!ctx)
        return QUIC_STATUS_SUCCESS;

    switch (event->Type) {
    case QUIC_STREAM_EVENT_START_COMPLETE:
        ctx->isStreamStarted = true;
        break;
    case QUIC_STREAM_EVENT_SEND_COMPLETE:
        delete[] reinterpret_cast<uint8_t*>(event->SEND_COMPLETE.ClientContext);
        break;
    case QUIC_STREAM_EVENT_RECEIVE: {
        bool pushed = false;
        for (int i = 0; i < event->RECEIVE.BufferCount; i++) {
            if (event->RECEIVE.Buffers[i].Length != 0) {
                ctx->inputBuffer.push(
                    {event->RECEIVE.Buffers[i].Buffer, event->RECEIVE.Buffers[i].Length});
                pushed = true;
            }
        }

        if (pushed) {
            event->RECEIVE.TotalBufferLength = 0;
            return QUIC_STATUS_PENDING;
        }
    } break;
    case QUIC_STREAM_EVENT_SHUTDOWN_COMPLETE:
        quicCtx.api()->StreamClose(ctx->stream);
        ctx->isStreamStarted = false;
        break;
    case QUIC_STREAM_EVENT_SEND_SHUTDOWN_COMPLETE:
        ctx->isStreamStarted = false;
        break;
    case QUIC_STREAM_EVENT_PEER_SEND_ABORTED:
    case QUIC_STREAM_EVENT_PEER_RECEIVE_ABORTED:
    case QUIC_STREAM_EVENT_PEER_SEND_SHUTDOWN:
        ctx->isStreamStarted = false;
        break;
    default:
        break;
    }
    return QUIC_STATUS_SUCCESS;
}

QUIC_STATUS connectionCallback(HQUIC, void* context, QUIC_CONNECTION_EVENT* event) {
    auto ctx = reinterpret_cast<urf::middleware::sockets::QuicSocketContext*>(context);
    urf::middleware::sockets::QuicContext quicCtx;

    if (!ctx)
        return QUIC_STATUS_INVALID_STATE;

    switch (event->Type) {
    case QUIC_CONNECTION_EVENT_CONNECTED:
        ctx->isConnectionOpen = true;
        break;
    case QUIC_CONNECTION_EVENT_RESUMED:
        quicCtx.api()->ConnectionSendResumptionTicket(
            ctx->connection, QUIC_SEND_RESUMPTION_FLAG_NONE, 0, NULL);
        ctx->isConnectionOpen = true;
        break;
    case QUIC_CONNECTION_EVENT_DATAGRAM_RECEIVED:
        if (event->DATAGRAM_RECEIVED.Buffer->Length != 0) {
            ctx->inputBuffer.push(
                {event->DATAGRAM_RECEIVED.Buffer->Buffer, event->DATAGRAM_RECEIVED.Buffer->Length});
            return QUIC_STATUS_PENDING;
        }
        break;
    case QUIC_CONNECTION_EVENT_SHUTDOWN_COMPLETE:
        ctx->isConnectionOpen = false;
        break;
    case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_TRANSPORT:
    case QUIC_CONNECTION_EVENT_SHUTDOWN_INITIATED_BY_PEER:
        ctx->isConnectionOpen = false;
        ctx->inputBuffer.dispose();
        break;
    case QUIC_CONNECTION_EVENT_PEER_STREAM_STARTED:
        ctx->stream = event->PEER_STREAM_STARTED.Stream;
        quicCtx.api()->SetCallbackHandler(ctx->stream, (void*)streamCallback, context);
        ctx->isStreamStarted = true;
        break;
    default:
        break;
    }
    return QUIC_STATUS_SUCCESS;
}

} // namespace

namespace urf {
namespace middleware {
namespace sockets {

QuicSocketContext::QuicSocketContext()
    : connection(nullptr)
    , configuration(nullptr)
    , stream(nullptr)
    , isConnectionOpen(false)
    , isStreamStarted(false)
    , inputBuffer()
    , partialInputBuffer()
    , partialInputBufferPosition(0) { }

QuicSocket::QuicSocket(const std::string hostname, uint16_t port)
    : role_(Roles::Client)
    , isOpen_(false)
    , port_(port)
    , hostname_(hostname)
    , quicCtx_(new QuicContext)
    , socketCtx_()
    , connectionLostHandlers_() {
    LOGGER.trace("Client CTor");

    socketCtx_.reset(new QuicSocketContext);
}

QuicSocket::QuicSocket(uint16_t port, HQUIC connection, HQUIC configuration)
    : role_(Roles::Server)
    , isOpen_(true)
    , port_(port)
    , hostname_()
    , quicCtx_(new QuicContext)
    , socketCtx_()
    , connectionLostHandlers_() {

    LOGGER.trace("Server CTor");
    socketCtx_.reset(new QuicSocketContext);
    socketCtx_->connection = connection;
    socketCtx_->configuration = configuration;
}

QuicSocket::~QuicSocket() {
    LOGGER.trace("DTor");
    if (isOpen_)
        close();

    if ((role_ == Roles::Client) && (socketCtx_->configuration)) {
        quicCtx_->api()->ConfigurationClose(socketCtx_->configuration);
    }
}

bool QuicSocket::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    socketCtx_->isConnectionOpen = false;
    socketCtx_->isStreamStarted = false;

    if (socketCtx_->configuration == nullptr) {
        QUIC_SETTINGS settings = {0};
        settings.IdleTimeoutMs = IdleTimeoutMs;
        settings.IsSet.IdleTimeoutMs = TRUE;
        settings.DatagramReceiveEnabled = 1;
        settings.IsSet.DatagramReceiveEnabled = TRUE;

        QUIC_CREDENTIAL_CONFIG credConfig;
        memset(&credConfig, 0, sizeof(credConfig));
        credConfig.Flags = QUIC_CREDENTIAL_FLAG_CLIENT;
        credConfig.Type = QUIC_CREDENTIAL_TYPE_NONE;
        credConfig.Flags |= QUIC_CREDENTIAL_FLAG_NO_CERTIFICATE_VALIDATION;

        if (QUIC_FAILED(quicCtx_->api()->ConfigurationOpen(quicCtx_->registration(),
                                                           &ALPN,
                                                           1,
                                                           &settings,
                                                           sizeof(settings),
                                                           NULL,
                                                           &socketCtx_->configuration))) {
            LOGGER.warn("ConfigurationOpen failed");
            return false;
        }

        if (QUIC_FAILED(quicCtx_->api()->ConfigurationLoadCredential(socketCtx_->configuration,
                                                                     &credConfig))) {
            LOGGER.warn("ConfigurationLoadCredential failed");
            return false;
        }
    }

    if (QUIC_FAILED(quicCtx_->api()->ConnectionOpen(quicCtx_->registration(),
                                                    &connectionCallback,
                                                    socketCtx_.get(),
                                                    &socketCtx_->connection))) {
        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("ConnectionOpen failed");
        return false;
    }

    if (QUIC_FAILED(quicCtx_->api()->ConnectionStart(socketCtx_->connection,
                                                     socketCtx_->configuration,
                                                     QUIC_ADDRESS_FAMILY_UNSPEC,
                                                     hostname_.c_str(),
                                                     port_))) {

        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("Failed to start connection");
        return false;
    }

    // STD Atomic wait whenever available
    for (int i = 0; i < 200; i++) {
        if (socketCtx_->isConnectionOpen) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (!socketCtx_->isConnectionOpen) {
        quicCtx_->api()->ConnectionShutdown(
            socketCtx_->connection, QUIC_CONNECTION_SHUTDOWN_FLAG_NONE, 0);
        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("Failed to connect to server");
        return false;
    }

    socketCtx_->inputBuffer = common::containers::ThreadSafeQueue<std::pair<uint8_t*, size_t>>();
    socketCtx_->partialInputBuffer = {nullptr, 0};
    socketCtx_->partialInputBufferPosition = 0;

    if (QUIC_FAILED(quicCtx_->api()->StreamOpen(socketCtx_->connection,
                                                QUIC_STREAM_OPEN_FLAG_NONE,
                                                streamCallback,
                                                socketCtx_.get(),
                                                &socketCtx_->stream))) {
        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("Failed to StreamOpen");
        return false;
    }

    if (QUIC_FAILED(
            quicCtx_->api()->StreamStart(socketCtx_->stream, QUIC_STREAM_START_FLAG_IMMEDIATE))) {
        quicCtx_->api()->StreamClose(socketCtx_->stream);
        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("Failed to StreamStart");

        return false;
    }

    for (int i = 0; i < 200; i++) {
        if (socketCtx_->isStreamStarted) {
            break;
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }

    if (!socketCtx_->isStreamStarted) {
        quicCtx_->api()->StreamClose(socketCtx_->stream);
        quicCtx_->api()->ConnectionClose(socketCtx_->connection);
        LOGGER.warn("Timeout waiting for stream to start");
        return false;
    }

    isOpen_ = true;
    return true;
}

bool QuicSocket::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    if (socketCtx_->connection && socketCtx_->isConnectionOpen) {
        quicCtx_->api()->ConnectionShutdown(
            socketCtx_->connection, QUIC_CONNECTION_SHUTDOWN_FLAG_NONE, 0);

        for (int i = 0; i < 20; i++) {
            if (!socketCtx_->isConnectionOpen) {
                break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        if (socketCtx_->isConnectionOpen) {
            LOGGER.warn("Could not shutdown connection");
            return false;
        }
    }

    quicCtx_->api()->ConnectionClose(socketCtx_->connection);
    socketCtx_->inputBuffer.dispose();

    isOpen_ = false;
    return true;
}

bool QuicSocket::isOpen() {
    return isOpen_;
}

bool QuicSocket::write(const std::vector<uint8_t>& bytes, bool requiresAck) {
    if (!isOpen_ || !socketCtx_->isStreamStarted) {
        return false;
    }

    if (!socketCtx_->isConnectionOpen) {
        // handleConnectionLost();
        return false;
    }

    // We can't send unreliable data of a size bigger than the UDP packet size, so we send it in a reliable way
    if (!requiresAck && bytes.size() > 65527) {
        requiresAck = true;
    }

    uint8_t* bufferRaw = new uint8_t[sizeof(QUIC_BUFFER) + bytes.size()];
    if (!bufferRaw) {
        LOGGER.info("Could not create buffer");
        return false;
    }

    std::memcpy(bufferRaw + sizeof(QUIC_BUFFER), bytes.data(), bytes.size());
    QUIC_BUFFER* buffer;
    buffer = (QUIC_BUFFER*)bufferRaw;
    buffer->Buffer = bufferRaw + sizeof(QUIC_BUFFER);
    buffer->Length = static_cast<uint32_t>(bytes.size());

    if (requiresAck) {
        auto status =
            quicCtx_->api()->StreamSend(socketCtx_->stream, buffer, 1, QUIC_SEND_FLAG_NONE, buffer);
        if (QUIC_FAILED(status)) {
            LOGGER.warn("StreamSend failed: {}", QuicStatusToString(status));
            return false;
        }
    } else {
        auto status = quicCtx_->api()->DatagramSend(
            socketCtx_->connection, buffer, 1, QUIC_SEND_FLAG_NONE, socketCtx_.get());
        if (QUIC_FAILED(status)) {
            LOGGER.warn("DatagramSend failed: {}", QuicStatusToString(status));
            return false;
        }
    }

    return true;
}

std::vector<uint8_t> QuicSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> retval;
    // There's some data in the partialInputBuffer ?
    if (socketCtx_->partialInputBuffer.second != 0) {
        if (socketCtx_->partialInputBuffer.second - socketCtx_->partialInputBufferPosition >=
            static_cast<size_t>(length)) {
            // All the data requested is in the partialInputBuffer
            retval = std::vector<uint8_t>(socketCtx_->partialInputBuffer.first +
                                              socketCtx_->partialInputBufferPosition,
                                          socketCtx_->partialInputBuffer.first +
                                              socketCtx_->partialInputBufferPosition + length);

            socketCtx_->partialInputBufferPosition += length;
            if (socketCtx_->partialInputBufferPosition == socketCtx_->partialInputBuffer.second) {
                quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                       socketCtx_->partialInputBuffer.second);
                socketCtx_->partialInputBuffer = {nullptr, 0};
                socketCtx_->partialInputBufferPosition = 0;
            }

            return retval;
        } else {
            // In the partial buffer there's not all the data requested
            retval = std::vector<uint8_t>(
                socketCtx_->partialInputBuffer.first + socketCtx_->partialInputBufferPosition,
                socketCtx_->partialInputBuffer.first + socketCtx_->partialInputBuffer.second);

            quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                   socketCtx_->partialInputBuffer.second);
            socketCtx_->partialInputBuffer = {nullptr, 0};
            socketCtx_->partialInputBufferPosition = 0;
        }
    }

    do {
        auto bufOpt = socketCtx_->inputBuffer.pop();
        if (!bufOpt) {
            if (!socketCtx_->isConnectionOpen) {
                handleConnectionLost();
            }
            return retval;
        }

        socketCtx_->partialInputBuffer = bufOpt.value();

        if (length - retval.size() >= socketCtx_->partialInputBuffer.second) {
            retval.insert(retval.end(),
                          socketCtx_->partialInputBuffer.first,
                          socketCtx_->partialInputBuffer.first +
                              socketCtx_->partialInputBuffer.second);

            quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                   socketCtx_->partialInputBuffer.second);
            socketCtx_->partialInputBuffer = {nullptr, 0};
            socketCtx_->partialInputBufferPosition = 0;
        } else {
            size_t toRead = length - retval.size();
            retval.insert(retval.end(),
                          socketCtx_->partialInputBuffer.first,
                          socketCtx_->partialInputBuffer.first + toRead);
            socketCtx_->partialInputBufferPosition += toRead;
        }
    } while (retval.size() < static_cast<size_t>(length));

    return retval;
}

std::vector<uint8_t> QuicSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> retval;
    if (socketCtx_->partialInputBuffer.second != 0) {
        // There's some data in the partialInputBuffer
        if (socketCtx_->partialInputBuffer.second - socketCtx_->partialInputBufferPosition >=
            static_cast<size_t>(length)) {
            // All the data requested is in the partialInputBuffer
            retval = std::vector<uint8_t>(socketCtx_->partialInputBuffer.first +
                                              socketCtx_->partialInputBufferPosition,
                                          socketCtx_->partialInputBuffer.first +
                                              socketCtx_->partialInputBufferPosition + length);

            socketCtx_->partialInputBufferPosition += length;
            if (socketCtx_->partialInputBufferPosition == socketCtx_->partialInputBuffer.second) {
                quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                       socketCtx_->partialInputBuffer.second);
                socketCtx_->partialInputBuffer = {nullptr, 0};
                socketCtx_->partialInputBufferPosition = 0;
            }

            return retval;
        } else {
            // In the partial buffer there's not all the data requested
            retval = std::vector<uint8_t>(
                socketCtx_->partialInputBuffer.first + socketCtx_->partialInputBufferPosition,
                socketCtx_->partialInputBuffer.first + socketCtx_->partialInputBuffer.second);
            quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                   socketCtx_->partialInputBuffer.second);
            socketCtx_->partialInputBuffer = {nullptr, 0};
            socketCtx_->partialInputBufferPosition = 0;
        }
    }

    do {
        auto bufOpt = socketCtx_->inputBuffer.pop(timeout);
        if (!bufOpt) {
            if (!socketCtx_->isConnectionOpen) {
                handleConnectionLost();
            }
            return retval;
        }

        socketCtx_->partialInputBuffer = bufOpt.value();
        if (length - retval.size() >= socketCtx_->partialInputBuffer.second) {
            retval.insert(retval.end(),
                          socketCtx_->partialInputBuffer.first,
                          socketCtx_->partialInputBuffer.first +
                              socketCtx_->partialInputBuffer.second);

            quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream,
                                                   socketCtx_->partialInputBuffer.second);
            socketCtx_->partialInputBuffer = {nullptr, 0};
            socketCtx_->partialInputBufferPosition = 0;
        } else {
            size_t toRead = length - retval.size();
            retval.insert(retval.end(),
                          socketCtx_->partialInputBuffer.first,
                          socketCtx_->partialInputBuffer.first + toRead);
            socketCtx_->partialInputBufferPosition += toRead;
        }
    } while (retval.size() < static_cast<size_t>(length));

    quicCtx_->api()->StreamReceiveComplete(socketCtx_->stream, retval.size());
    return retval;
}

bool QuicSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onConnectionLost()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

QUIC_STATUS QuicSocket::quicOpen() {
    LOGGER.trace("quicOpen()");
    try {
        quicCtx_.reset(new QuicContext);
    } catch (const std::runtime_error& ex) {
        LOGGER.warn("Failed to create QUIC context: {}", ex.what());
        return QUIC_STATUS_INTERNAL_ERROR;
    }

    quicCtx_->api()->SetCallbackHandler(
        socketCtx_->connection, (void*)connectionCallback, socketCtx_.get());

    return quicCtx_->api()->ConnectionSetConfiguration(socketCtx_->connection,
                                                       socketCtx_->configuration);
}

void QuicSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
