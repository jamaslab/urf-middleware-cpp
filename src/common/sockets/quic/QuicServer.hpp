#pragma once

#include <chrono>
#include <memory>
#include <functional>
#include <string>
#include <vector>

#include "common/sockets/ITransportSocketServer.hpp"

#include "common/sockets/quic/QuicContext.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>

namespace urf {
namespace middleware {
namespace sockets {

struct QuicServerContext {
   HQUIC listener = nullptr;
   HQUIC configuration = nullptr;
   common::containers::ThreadSafeQueue<HQUIC> connectionQueue;
   common::containers::ThreadSafeQueue<QUIC_STATUS> connectionStatusQueue;
};

class QuicServer : public ITransportSocketServer {
 public:
    QuicServer() = delete;
    QuicServer(uint16_t port, const std::string& certificateFilePath, const std::string& privateKeyFilePath, const std::string& privateKeyPassword = "");
    QuicServer(const QuicServer&) = delete;
    QuicServer(QuicServer&&) = delete;
    ~QuicServer() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;

    std::shared_ptr<ITransportSocket> acceptConnection() override;
    bool onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) override;

 private:
    uint16_t port_;
    std::string certificateFilePath_;
    std::string privateKeyFilePath_;
    std::string privateKeyPassword_;
    std::unique_ptr<QuicContext> quicCtx_;
    std::unique_ptr<QuicServerContext> serverCtx_;

    std::atomic<bool> isOpen_;
    std::vector<std::shared_ptr<ITransportSocket> > connectedClients_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
