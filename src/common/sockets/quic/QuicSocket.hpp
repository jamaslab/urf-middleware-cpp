#pragma once

#include <atomic>
#include <chrono>
#include <string>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/quic/QuicServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

struct QuicSocketContext {
    HQUIC connection;
    HQUIC configuration;
    HQUIC stream;
    std::atomic<bool> isConnectionOpen;
    std::atomic<bool> isStreamStarted;

    common::containers::ThreadSafeQueue<std::pair<uint8_t*, size_t>> inputBuffer;
    std::pair<uint8_t*, size_t> partialInputBuffer;
    size_t partialInputBufferPosition;

    QuicSocketContext();
};

class QuicSocket : public ITransportSocket {
 public:
    QuicSocket() = delete;
    QuicSocket(const std::string hostname, uint16_t port);
    QuicSocket(const QuicSocket&) = delete;
    QuicSocket(QuicSocket&&) = delete;
    ~QuicSocket() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;
    bool allowsFragmentedRead() override { return true; }

    bool write(const std::vector<uint8_t>& bytes, bool requiresAck) override;
    std::vector<uint8_t> read(int length) override;
    std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) override;

    bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) override;

 private:
    friend class QuicServer;
    QuicSocket(uint16_t port, HQUIC connection, HQUIC configuration);
    QUIC_STATUS quicOpen();
    void handleConnectionLost();

    enum Roles { Client, Server };

private:
    Roles role_;
    std::atomic<bool> isOpen_;
    uint16_t port_;
    std::string hostname_;

    std::unique_ptr<QuicContext> quicCtx_;
    std::unique_ptr<QuicSocketContext> socketCtx_;

    std::vector<std::function<void(ITransportSocket*)>> connectionLostHandlers_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
