#include <algorithm>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <utility>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/PacketSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("PacketSocket");
}

namespace urf {
namespace middleware {
namespace sockets {

PacketSocket::PacketSocket(std::shared_ptr<ITransportSocket> socket)
    : separator_()
    , socket_(socket)
    , outputBuffer_()
    , deserializeBody_(true)
    , syncLost_(false) {
    LOGGER.trace("CTor");
    std::string separatorStr = "~~~~~~";
    separator_ = std::vector<uint8_t>(separatorStr.begin(), separatorStr.end());
}

PacketSocket::~PacketSocket() {
    LOGGER.trace("DTor");
    socket_->close();
}

bool PacketSocket::open() {
    LOGGER.trace("open");
    return socket_->open();
}

bool PacketSocket::close() {
    LOGGER.trace("close");
    return socket_->close();
}

bool PacketSocket::isOpen() {
    return socket_->isOpen();
}

bool PacketSocket::write(const messages::ControlPacket& control,
                         const messages::Message& packet,
                         bool requiresAck) {
    if (!isOpen()) {
        LOGGER.warn("PacketSocket is not open");
        return false;
    }

    std::scoped_lock lock(outputBufferMtx_);
    outputBuffer_.clear();
    std::ostream os(&outputBuffer_);
    os.write(reinterpret_cast<const char*>(separator_.data()), separator_.size());
    control.serialize(os);
    packet.serialize(os);

    return socket_->write(outputBuffer_.ref(), requiresAck);
}

std::optional<std::tuple<messages::ControlPacket, messages::Message>> PacketSocket::read() {
    if (!isOpen()) {
        LOGGER.warn("PacketSocket is not open");
        return std::nullopt;
    }

    if (!socket_->allowsFragmentedRead()) {
        return parsePacket(socket_->read(0));
    }

    bool wasSyncLost = syncLost_;
    if (syncLost_ && !resync(true, std::chrono::seconds(0))) {
        LOGGER.warn("Failed to resync");
        return std::nullopt;
    }

    if (!wasSyncLost) {
        auto syncBytes = socket_->read(static_cast<uint32_t>(separator_.size()));
        if (syncBytes != separator_) {
            LOGGER.warn("Failed to read sync bytes");
            syncLost_ = true;
            return std::nullopt;
        }
    }

    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(
                   std::chrono::high_resolution_clock::now().time_since_epoch())
                   .count();
    auto controlPacketLength = socket_->read(1);
    if (controlPacketLength.size() != 1) {
        LOGGER.warn("Failed to control header bytes");
        syncLost_ = true;
        return std::nullopt;
    }

    if (controlPacketLength[0] < 3) {
        LOGGER.warn("Something was wrong in control packet length");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::ControlPacket control;
    if (!control.deserialize(socket_->read(controlPacketLength[0]))) {
        LOGGER.warn("Failed to deserialize control packet");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::Header header;
    if (!header.deserialize(socket_->read(messages::Header::size))) {
        LOGGER.warn("Failed to deserialize header");
        syncLost_ = true;
        return std::nullopt;
    }

    if (header.length() == 0) {
        LOGGER.warn("Received length 0 in header");
        syncLost_ = true;
        return std::nullopt;
    }
    header.timestampReceived(now);

    auto packetBytes = socket_->read(header.length());
    if (packetBytes.size() != header.length()) {
        LOGGER.warn("Failed to read body bytes");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::Body body;
    if (deserializeBody_) {
        if (!body.deserialize(packetBytes)) {
            LOGGER.warn("Failed to deserialize body");
            return std::nullopt;
        }
    } else {
        body.setBytes(std::move(packetBytes));
    }

    return std::make_tuple(control, messages::Message(std::move(body), std::move(header)));
}

std::optional<std::tuple<messages::ControlPacket, messages::Message>>
PacketSocket::read(const std::chrono::milliseconds& timeout) {
    if (!isOpen()) {
        LOGGER.warn("PacketSocket is not open");
        return std::nullopt;
    }

    if (!socket_->allowsFragmentedRead()) {
        return parsePacket(socket_->read(0, timeout));
    }

    bool wasSyncLost = syncLost_;
    if (syncLost_ && !resync(false, timeout)) {
        LOGGER.warn("Failed to resync");
        return std::nullopt;
    }

    if (!wasSyncLost) {
        auto syncBytes = socket_->read(static_cast<uint32_t>(separator_.size()), timeout);
        if (syncBytes != separator_) {
            LOGGER.warn("Failed to read sync bytes");
            syncLost_ = true;
            return std::nullopt;
        }
    }

    auto now = std::chrono::duration_cast<std::chrono::milliseconds>(
                   std::chrono::high_resolution_clock::now().time_since_epoch())
                   .count();

    auto controlPacketLength = socket_->read(1);
    if (controlPacketLength.size() != 1) {
        LOGGER.warn("Failed to control header bytes");
        syncLost_ = true;
        return std::nullopt;
    }

    if (controlPacketLength[0] < 3) {
        LOGGER.warn("Something was wrong in control packet length");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::ControlPacket control;
    if (!control.deserialize(socket_->read(controlPacketLength[0], timeout))) {
        LOGGER.warn("Failed to deserialize control packet");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::Header header;
    if (!header.deserialize(socket_->read(messages::Header::size, timeout))) {
        LOGGER.warn("Failed to deserialize header");
        syncLost_ = true;
        return std::nullopt;
    }

    if (header.length() == 0) {
        LOGGER.warn("Received length 0 in header");
        syncLost_ = true;
        return std::nullopt;
    }
    header.timestampReceived(now);

    auto packetBytes = socket_->read(header.length(), timeout);
    if (packetBytes.size() != header.length()) {
        LOGGER.warn("Failed to read body bytes");
        syncLost_ = true;
        return std::nullopt;
    }

    messages::Body body;
    if (deserializeBody_) {
        if (!body.deserialize(packetBytes)) {
            LOGGER.warn("Failed to deserialize body");
            return std::nullopt;
        }
    } else {
        body.setBytes(std::move(packetBytes));
    }

    return std::make_tuple(control, messages::Message(std::move(body), std::move(header)));
}

std::shared_ptr<ITransportSocket> PacketSocket::transportSocket() {
    return socket_;
}

void PacketSocket::deserializeBody(bool value) {
    deserializeBody_ = value;
}

bool PacketSocket::resync(bool blocking, const std::chrono::milliseconds& timeout) {
    std::vector<uint8_t> bf;
    int consecutiveFound = 0;

    auto start = std::chrono::high_resolution_clock::now();
    std::chrono::milliseconds timeToSleep = timeout;
    do {
        if (blocking) {
            bf = socket_->read(1);
        } else {
            bf = socket_->read(1, timeToSleep);
            auto now = std::chrono::high_resolution_clock::now();
            timeToSleep =
                timeout - std::chrono::duration_cast<std::chrono::milliseconds>(now - start);
        }

        if (bf.size() != 1) {
            return false;
        }

        if (!blocking && (timeToSleep.count() == 0)) {
            return false;
        }

        if (bf[0] == static_cast<uint8_t>('~')) {
            consecutiveFound++;
        } else {
            consecutiveFound = 0;
        }

    } while (consecutiveFound < 6);

    syncLost_ = false;
    return true;
}

std::optional<std::tuple<messages::ControlPacket, messages::Message>>
PacketSocket::parsePacket(const std::vector<uint8_t> bytes) {
    size_t cumulated = separator_.size() + 1;
    if (bytes.size() < cumulated) {
        LOGGER.warn("Failed read control packet length");
        return std::nullopt;
    }

    uint8_t controlPacketLength = bytes[separator_.size()];
    if (bytes.size() < cumulated + controlPacketLength) {
        LOGGER.warn("Missing control packet length");
        return std::nullopt;
    }

    messages::ControlPacket control;
    if (!control.deserialize(std::vector<uint8_t>(
            bytes.begin() + cumulated, bytes.begin() + cumulated + controlPacketLength))) {
        LOGGER.warn("Failed to deserialize control packet");
        syncLost_ = true;
        return std::nullopt;
    }

    cumulated += controlPacketLength;

    if (bytes.size() < cumulated + messages::Header::size) {
        LOGGER.warn("Missing header");
        return std::nullopt;
    }
    messages::Header header;
    if (!header.deserialize(bytes.data() + cumulated, messages::Header::size)) {
        LOGGER.warn("Failed to deserialize header");
        syncLost_ = true;
        return std::nullopt;
    }

    cumulated += messages::Header::size;

    if (bytes.size() < cumulated + header.length()) {
        LOGGER.warn("Missing header");
        return std::nullopt;
    }

    messages::Body body;
    if (deserializeBody_) {
        if (!body.deserialize(bytes.data() + cumulated, header.length())) {
            LOGGER.warn("Failed to deserialize body");
            return std::nullopt;
        }
    } else {
        body.setBytes(std::move(
            std::vector<uint8_t>(bytes.begin() + cumulated, bytes.begin() + cumulated + header.length())));
    }

    return std::make_tuple(control, messages::Message(std::move(body), std::move(header)));
}

} // namespace sockets
} // namespace middleware
} // namespace urf
