#include <memory>
#include <regex>
#include <string>

#include <iostream>

#include "common/sockets/TransportSocketFactory.hpp"
#include "common/sockets/ipc/IpcServer.hpp"
#include "common/sockets/ipc/IpcSocket.hpp"
#include "common/sockets/proc/ProcServer.hpp"
#include "common/sockets/proc/ProcSocket.hpp"

#include "common/sockets/tcp/TcpServer.hpp"
#include "common/sockets/tcp/TcpSocket.hpp"

#include "common/sockets/udp/UdpServer.hpp"
#include "common/sockets/udp/UdpSocket.hpp"

#ifdef WITH_MSQUIC
#    include "common/sockets/quic/QuicServer.hpp"
#    include "common/sockets/quic/QuicSocket.hpp"
#endif

namespace {

std::vector<std::string> split(const std::string& s, char seperator) {
    std::vector<std::string> output;
    std::string::size_type prev_pos = 0, pos = 0;

    while ((pos = s.find(seperator, pos)) != std::string::npos) {
        std::string substring(s.substr(prev_pos, pos - prev_pos));

        output.push_back(substring);

        prev_pos = ++pos;
    }

    output.push_back(s.substr(prev_pos, pos - prev_pos)); // Last word

    return output;
}

} // namespace

namespace urf {
namespace middleware {
namespace sockets {

std::shared_ptr<ITransportSocket> TransportSocketFactory::createClient(const std::string& socket) {
    auto info = TransportSocketFactory::getProtocolAndAddress(socket);

    if (info.first == "tcp") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));

        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid tcp port. Port should be between 1 and 65535");
        }

        return std::make_shared<TcpSocket>(ip_address, port);
#ifdef WITH_MSQUIC
    } else if (info.first == "quic") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));

        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid quic port. Port should be between 1 and 65535");
        }

        return std::make_shared<QuicSocket>(ip_address, port);
#endif
    } else if (info.first == "udp") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));

        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid udp port. Port should be between 1 and 65535");
        }

        return std::make_shared<UdpSocket>(ip_address, port);
    } else if (info.first == "proc") {
        return std::make_shared<ProcSocket>(info.second);
    }
#ifdef __linux__
    else if (info.first == "ipc") {
        return std::make_shared<IpcSocket>(info.second);
    }
#elif _WIN32 || _WIN64
    else if (info.first == "ipc") {
        auto port = std::stoi(info.second);
        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid ipc port. Port should be between 1 and 65535");
        }

        return std::make_shared<IpcSocket>(port);
    }
#endif

    throw std::invalid_argument("Uknown protocol. Available protocols are: tcp, ipc");
}

std::shared_ptr<ITransportSocketServer>
TransportSocketFactory::createServer(const std::string& socket) {
    auto info = TransportSocketFactory::getProtocolAndAddress(socket);

    if (info.first == "tcp") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));
        if (ip_address != "*") {
            throw std::invalid_argument("Tcp server address must be *. Format: tcp://*:<port>");
        }
        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid tcp port. Port should be between 1 and 65535");
        }

        return std::make_shared<TcpServer>(port);
#ifdef WITH_MSQUIC
    } else if (info.first == "quic") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));

        if (ip_address != "*") {
            throw std::invalid_argument("Tcp server address must be *. Format: tcp://*:<port>");
        }
        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid tcp port. Port should be between 1 and 65535");
        }

        auto splitted = split(info.second, '&');
        std::string keyPath, certPath, keyPassword;

        for (auto const& str : splitted) {
            if (str.find("key=") != std::string::npos) {
                keyPath = str.substr(str.find("key=") + 4);
            } else if (str.find("cert=") != std::string::npos) {
                certPath = str.substr(str.find("cert=") + 5);
            } else if (str.find("pwd=") != std::string::npos) {
                keyPassword = str.substr(str.find("pwd=") + 4);
            }
        }

        if (keyPath.empty()) {
            throw std::invalid_argument("Missing path to server private key in connection string. "
                                        "Add &key=<path_to_private_key>. Optional &pwd=<password> "
                                        "if private key is password protected.");
        }

        if (certPath.empty()) {
            throw std::invalid_argument(
                "Missing path to server certificate. Add &cert=<path_to_certificate>.");
        }

        return std::make_shared<QuicServer>(port, certPath, keyPath, keyPassword);
#endif
    } else if (info.first == "udp") {
        auto ip_address = info.second.substr(0, info.second.find(":"));
        auto port = std::stoi(info.second.substr(info.second.find(":") + 1));
        if (ip_address != "*") {
            throw std::invalid_argument("Udp server address must be *. Format: tcp://*:<port>");
        }
        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid udp port. Port should be between 1 and 65535");
        }

        return std::make_shared<UdpServer>(port);
    } else if (info.first == "proc") {
        return std::make_shared<ProcServer>(info.second);
    }
#ifdef __linux__
    else if (info.first == "ipc") {
        return std::make_shared<IpcServer>(info.second);
    }
#elif _WIN32 || _WIN64
    else if (info.first == "ipc") {
        auto port = std::stoi(info.second);
        if ((port < 1) || (port > 65535)) {
            throw std::invalid_argument("Invalid ipc port. Port should be between 1 and 65535");
        }

        return std::make_shared<IpcServer>(port);
    }
#endif

    throw std::invalid_argument("Uknown protocol. Available protocols are: tcp, ipc");
}

std::pair<std::string, std::string>
TransportSocketFactory::getProtocolAndAddress(const std::string& socket) {
    auto protocolPos = socket.find("://");
    if (protocolPos == std::string::npos) {
        throw std::invalid_argument(
            "Missing protocol. Format should be <protocol>://<protocol address>: " + socket);
    }

    auto protocol = socket.substr(0, protocolPos);
    auto protocol_address = socket.substr(protocolPos + 3);

    return {protocol, protocol_address};
}

} // namespace sockets
} // namespace middleware
} // namespace urf
