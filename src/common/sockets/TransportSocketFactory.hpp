#pragma once

#include <memory>
#include <string>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/ITransportSocketServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class TransportSocketFactory {
 public:
    static std::shared_ptr<ITransportSocket> createClient(const std::string& socket);
    static std::shared_ptr<ITransportSocketServer> createServer(const std::string& socket);

 private:
    static std::pair<std::string, std::string> getProtocolAndAddress(const std::string& socket);
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
