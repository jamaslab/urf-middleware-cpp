#include <algorithm>
#include <iostream>
#include <vector>

#include "urf/middleware/services/LocalServices.hpp"
#include "urf/middleware/sockets/Server.hpp"

#include "common/sockets/TransportSocketFactory.hpp"
#include "common/sockets/impl/ServerImpl.hpp"

namespace urf {
namespace middleware {
namespace sockets {

Server::Server(const std::string& serviceName, const std::string& socket)
    : Server(serviceName, std::vector<std::string>{socket}) { }

Server::Server(const std::string& serviceName, const std::vector<std::string>& serversList)
    : impl_() {
    std::vector<std::shared_ptr<ITransportSocketServer>> servers;
    for (auto serverString : serversList) {
        servers.push_back(TransportSocketFactory::createServer(serverString));
    }
    impl_.reset(new ServerImpl(serviceName, servers));

    services::LocalServices localServices;

    // proc servers are removed from the services to register as they are not reachable from outside the process
    auto serversToRegister = serversList;
    serversToRegister.erase(
        std::remove_if(serversToRegister.begin(),
                       serversToRegister.end(),
                       [](auto element) { return element.find("proc://") != std::string::npos; }),
        serversToRegister.end());

    localServices.registerService(serviceName, {{"connections", serversToRegister}});
}

Server::~Server() {
    services::LocalServices localServices;
    localServices.unregisterService(serviceName());
}

bool Server::open() {
    return impl_->open();
}

bool Server::close() {
    return impl_->close();
}

bool Server::isOpen() {
    return impl_->isOpen();
}

size_t Server::connectedClientsCount() {
    return impl_->connectedClientsCount();
}

std::string Server::serviceName() const {
    return impl_->serviceName();
}

std::optional<messages::Message> Server::pull() {
    return impl_->pull();
}

std::optional<messages::Message> Server::pull(const std::chrono::milliseconds& timeout) {
    return impl_->pull(timeout);
}

bool Server::push(const messages::Message& message, bool requiresAck) {
    return impl_->push(message, requiresAck);
}

bool Server::onPush(const std::function<void(const messages::Message&)>& handler) {
    return impl_->onPush(handler);
}

std::vector<messages::Message> Server::request(const messages::Message& message) {
    return impl_->request(message);
}
std::vector<messages::Message> Server::request(const messages::Message& message,
                                               const std::chrono::milliseconds& timeout) {
    return impl_->request(message, timeout);
}

std::future<std::vector<messages::Message>> Server::requestAsync(const messages::Message& message) {
    return impl_->requestAsync(message);
}
std::future<std::vector<messages::Message>>
Server::requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout) {
    return impl_->requestAsync(message, timeout);
}

std::optional<messages::Message> Server::receiveRequest() {
    return impl_->receiveRequest();
}

std::optional<messages::Message> Server::receiveRequest(const std::chrono::milliseconds& timeout) {
    return impl_->receiveRequest(timeout);
}

bool Server::respond(const messages::Message& message) {
    return impl_->respond(message);
}

bool Server::subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate) {
    return impl_->subscribe(topic, maxRate);
}

uint32_t Server::subscriptionsCount(const std::string& topic) {
    return impl_->subscriptionsCount(topic);
}

bool Server::unsubscribe(const std::string& topic) {
    return impl_->unsubscribe(topic);
}

bool Server::keepUpdateHistory(bool value) {
    return impl_->keepUpdateHistory(value);
}

std::optional<messages::Message> Server::receiveUpdate(const std::string& topic) {
    return impl_->receiveUpdate(topic);
}

std::optional<messages::Message> Server::receiveUpdate(const std::string& topic,
                                                       const std::chrono::milliseconds& timeout) {
    return impl_->receiveUpdate(topic, timeout);
}

bool Server::onUpdate(
    const std::string& topic,
    const std::function<void(const std::string&, const messages::Message&)>& handler) {
    return impl_->onUpdate(topic, handler);
}

bool Server::publish(const std::string& topic, const messages::Message& message, bool requiresAck) {
    return impl_->publish(topic, message, requiresAck);
}

std::vector<std::string> Server::availablePartnerTopics() {
    return impl_->availablePartnerTopics();
}

bool Server::addTopic(const std::string& topic) {
    return impl_->addTopic(topic);
}

bool Server::removeTopic(const std::string& topic) {
    return impl_->removeTopic(topic);
}

bool Server::automaticallyDeserializeBody(bool value) {
    return impl_->automaticallyDeserializeBody(value);
}

bool Server::serviceMetadata(const nlohmann::json& metadata) {
    services::LocalServices localServices;
    auto services = localServices.getServices();
    auto serviceData = services[serviceName()];

    serviceData["metadata"] = metadata;
    localServices.registerService(serviceName(), serviceData);

    return true;
}

nlohmann::json Server::serviceMetadata() const {
    services::LocalServices localServices;
    auto services = localServices.getServices();
    auto serviceData = services[serviceName()];

    if (serviceData.find("metadata") != serviceData.end()) {
        return serviceData["metadata"];
    } else {
        return nlohmann::json();
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
