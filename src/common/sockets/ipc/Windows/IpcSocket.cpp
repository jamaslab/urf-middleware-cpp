#include <string>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/ipc/IpcSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("IpcSocket");
}

namespace urf {
namespace middleware {
namespace sockets {

IpcSocket::IpcSocket(uint16_t port) :
    wsaData_(),
    socket_(INVALID_SOCKET),
    role_(Roles::Client),
    isOpen_(false),
    isBlocking_(true),
    port_(port) {
        LOGGER.trace("CTor");
    }

IpcSocket::IpcSocket(SOCKET socket, int port) :
    wsaData_(),
    socket_(socket),
    role_(Roles::Server),
    isOpen_(true),
    isBlocking_(true),
    port_(port) {
        LOGGER.trace("CTor");
    }

IpcSocket::~IpcSocket() {
    LOGGER.trace("DTor");
    closesocket(socket_);
    WSACleanup();
}

bool IpcSocket::open()  {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    auto retval = WSAStartup(MAKEWORD(2,2), &wsaData_);
    if (retval != 0) {
        LOGGER.warn("WSAStartup failed with error:  {}", retval);
        return false;
    }

    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the server address and port
    struct addrinfo *result;
    retval = getaddrinfo("127.0.0.1", std::to_string(port_).c_str(), &hints, &result);
    if (retval != 0 ) {
        LOGGER.warn("getaddrinfo failed with error: {}", retval);
        WSACleanup();
        return false;
    }

    socket_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (socket_ == INVALID_SOCKET) {
        LOGGER.warn("socket failed with error: {}", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    do {
        retval = connect(socket_, result->ai_addr, (int)result->ai_addrlen);
        if (retval == SOCKET_ERROR) {
            LOGGER.error("Failed to connect: {}", WSAGetLastError());
            closesocket(socket_);
            socket_ = INVALID_SOCKET;
            WSACleanup();
            return false;
        }
    } while (retval != 0);

    isOpen_ = true;
    return true;
}

bool IpcSocket::close()  {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    shutdown(socket_, SD_SEND);
    closesocket(socket_);
    isOpen_ = false;
    return true;
}

bool IpcSocket::isOpen()  {
    return isOpen_;
}

bool IpcSocket::write(const std::vector<uint8_t>& buffer, bool requiresAck)  {
    if (!isOpen_) {
        return false;
    }

    size_t sent = 0;
    do {
        int retval = ::send(socket_, reinterpret_cast<const char*>(buffer.data()+sent), static_cast<int>(buffer.size()-sent), 0);
        if (retval == -1) {
            if (errno == EAGAIN) {
                continue;
            } else {
                // handleConnectionLost();
                return false;
            }
        }
        sent += static_cast<size_t>(retval);
    } while (sent < buffer.size());
    return true;
}

std::vector<uint8_t> IpcSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (!isBlocking_) {
        setNonBlockingFlag(false);
    }

    return blockingRead(length);
}

std::vector<uint8_t> IpcSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (isBlocking_) {
        setNonBlockingFlag(true);
    }
    return nonBlockingRead(length, timeout);
}

bool IpcSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onPartnerDisconnect()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

bool IpcSocket::setNonBlockingFlag(bool value) {
    LOGGER.trace("Changing non blocking flag to {}", value);
    u_long iMode = value ? 1 : 0;

    if (ioctlsocket(socket_, FIONBIO, &iMode) != 0) {
        char buf[256];
        errno = strerror_s(buf, 256, errno);
        LOGGER.warn("Could not set O_NONBLOCK flag: {}", buf);
        return false;
    }
    isBlocking_ = !value;

    return true;
}

std::vector<uint8_t> IpcSocket::nonBlockingRead(int length, const std::chrono::milliseconds& timeout) {
    int received = 0;
    std::vector<uint8_t> buffer;
    if (timeout.count() == 0) {  // fully non blocking read
        char* cbuf = new char[length];
        int retval = ::recv(socket_, cbuf, length-received, 0);
        if (retval != -1) {
            buffer = std::vector<uint8_t>(cbuf, cbuf+retval);
        }
        delete[] cbuf;
        return buffer;
    }

    struct pollfd fd;
    fd.fd = socket_;
    fd.events = POLLRDNORM;
    auto start = std::chrono::high_resolution_clock::now();
    do {
        auto now = std::chrono::high_resolution_clock::now();
        if ((now - start) > timeout) {
            break;
        }

        if (!isOpen_) {
            break;
        }

        auto retval = WSAPoll(&fd, 1, static_cast<int>((timeout - std::chrono::duration_cast<std::chrono::milliseconds>(now - start)).count()));
        if (retval == -1) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            break;
        } else if (retval == 0) {
            LOGGER.info("Read timeout");
            break;
        }

        char* cbuf = new char[length];
        retval = ::recv(socket_, cbuf, length-received, 0);
        if (retval == -1) {
            if (errno == EAGAIN) {
                delete[] cbuf;
                continue;
            } else {
                delete[] cbuf;
                break;
            }
        }

        if (retval == 0) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            delete[] cbuf;
            break;
        }

        std::copy(cbuf, cbuf+retval, std::back_inserter(buffer));
        received += retval;
        delete[] cbuf;
    } while (received < length);

    return buffer;
}

std::vector<uint8_t> IpcSocket::blockingRead(int length) {
    std::vector<uint8_t> buffer;
    buffer.resize(length);
    int retval = ::recv(socket_, reinterpret_cast<char*>(buffer.data()), length, MSG_WAITALL);

    if (retval <= 0) {
        if (retval == -1)
            LOGGER.warn("Error during blocking read: {}", WSAGetLastError());
        handleConnectionLost();
        return std::vector<uint8_t>();
    }
    return buffer;
}

void IpcSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
