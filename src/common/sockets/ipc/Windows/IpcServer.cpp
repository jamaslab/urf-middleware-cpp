#define MAX_PENDING_CONNECTIONS 5

#include <cstring>
#include <memory>
#include <string>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/ipc/IpcServer.hpp"
#include "common/sockets/ipc/IpcSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("IpcServer");
}

namespace urf {
namespace middleware {
namespace sockets {

IpcServer::IpcServer(uint16_t port) :
    port_(port),
    data_(),
    socket_(INVALID_SOCKET),
    isOpen_(false),
    connectedClients_() {
        LOGGER.trace("CTor");
}

IpcServer::~IpcServer() {
    LOGGER.trace("DTor");
    closesocket(socket_);
    WSACleanup();
}

bool IpcServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    auto retval = WSAStartup(MAKEWORD(2,2), &data_);
    if (retval != 0) {
        LOGGER.warn("WSAStartup failed with error: {}", retval);
        return false;
    }

    struct addrinfo *result = NULL;
    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_PASSIVE;

    // Resolve the server address and port
    retval = getaddrinfo("127.0.0.1", std::to_string(port_).c_str(), &hints, &result);
    if (retval != 0 ) {
        LOGGER.warn("getaddrinfo failed with error: {}", retval);
        WSACleanup();
        return false;
    }

    // Create a SOCKET for connecting to server
    socket_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (socket_ == INVALID_SOCKET) {
        LOGGER.warn("socket failed with error: {}", WSAGetLastError());
        freeaddrinfo(result);
        WSACleanup();
        return false;
    }

    // Setup the TCP listening socket
    retval = bind(socket_, result->ai_addr, (int)result->ai_addrlen);
    if (retval == SOCKET_ERROR) {
        LOGGER.warn("bind failed with error: {}", WSAGetLastError());
        freeaddrinfo(result);
        closesocket(socket_);
        WSACleanup();
        return false;
    }

    freeaddrinfo(result);

    retval = listen(socket_, SOMAXCONN);
    if (retval == SOCKET_ERROR) {
        LOGGER.warn("listen failed with error: {}", WSAGetLastError());
        closesocket(socket_);
        WSACleanup();
        return false;
    }

    isOpen_ = true;
    return true;
}

bool IpcServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (size_t i=0; i < connectedClients_.size(); i++) {
        connectedClients_[i]->close();
    }

    connectedClients_.clear();
    closesocket(socket_);

    isOpen_ = false;
    return true;
}

bool IpcServer::isOpen() {
    LOGGER.trace("isOpen");
    return isOpen_;
}

std::shared_ptr<ITransportSocket> IpcServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    auto client = accept(socket_, NULL, NULL);
    if (client == INVALID_SOCKET) {
        LOGGER.warn("Could not accept socket: {}", WSAGetLastError());
        return nullptr;
    }

    struct sockaddr_in client_addr;
    int addrlen = sizeof(client_addr);
    if(getsockname(client, (struct sockaddr*)&client_addr, &addrlen) != 0) {
        LOGGER.warn("Could not retrieve client information: {}", WSAGetLastError());
        return nullptr;
    }

    char str[INET_ADDRSTRLEN];
    InetNtop(AF_INET, &(client_addr), str, INET_ADDRSTRLEN);
    uint16_t client_port = static_cast<uint16_t>(client_addr.sin_port);
    std::shared_ptr<ITransportSocket> sockPtr(new IpcSocket(client, client_port));
    LOGGER.info("Client connected from {} on port {}", client_port, port_);
    connectedClients_.push_back(sockPtr);
    return sockPtr;
}

bool IpcServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) {
    return true;
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
