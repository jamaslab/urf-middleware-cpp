#pragma once

#include <atomic>
#include <chrono>
#include <string>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/ipc/IpcServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class IpcSocket : public ITransportSocket {
 public:
    IpcSocket() = delete;
#ifdef __linux__
    explicit IpcSocket(const std::string& name);
#elif _WIN32 || _WIN64
    explicit IpcSocket(uint16_t port);
#endif
    IpcSocket(const IpcSocket&) = delete;
    IpcSocket(IpcSocket&&) = delete;
    ~IpcSocket() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;
    bool allowsFragmentedRead() override { return true; }

    bool write(const std::vector<uint8_t>& bytes, bool requiresAck) override;
    std::vector<uint8_t> read(int length) override;
    std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) override;

    bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) override;

 private:
    friend class IpcServer;

#ifdef __linux__
    std::string name_;
    int socketFd_;
    IpcSocket(int socketFd, const std::string& name);
    int getSocketError();
#elif _WIN32 || _WIN64
    IpcSocket(SOCKET socket, int port);
    WSADATA wsaData_;
    SOCKET socket_;
#endif

    enum Roles { Client, Server };

    Roles role_;
    std::atomic<bool> isOpen_;

    bool isBlocking_;
    int port_;

    bool setNonBlockingFlag(bool value);
    std::vector<uint8_t> nonBlockingRead(int length, const std::chrono::milliseconds& timeout);
    std::vector<uint8_t> blockingRead(int length);

    std::vector<std::function<void(ITransportSocket*)>> connectionLostHandlers_;
    void handleConnectionLost();
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
