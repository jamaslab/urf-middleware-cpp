#pragma once

#include <chrono>
#include <memory>
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include "common/sockets/ITransportSocketServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class IpcServer : public ITransportSocketServer {
 public:
    IpcServer() = delete;
#ifdef __linux__
    explicit IpcServer(const std::string& name);
#elif _WIN32 || _WIN64
    explicit IpcServer(uint16_t port);
#endif
    IpcServer(const IpcServer&) = delete;
    IpcServer(IpcServer&&) = delete;
    ~IpcServer() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;

    std::shared_ptr<ITransportSocket> acceptConnection() override;
    bool onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) override;

 private:
#ifdef __linux__
    std::string name_;
    int serverFd_;
    int getSocketError();
    bool isInTimedWait();
#elif _WIN32 || _WIN64
    uint16_t port_;
    WSADATA data_;
    SOCKET socket_;
#endif

    bool isOpen_;
    std::vector<std::shared_ptr<ITransportSocket> > connectedClients_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
