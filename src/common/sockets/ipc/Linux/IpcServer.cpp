/* © Copyright CERN 2020.  All rights reserved. This software is released under a CERN proprietary software licence.
 * Any permission to use it shall be granted in writing. Requests shall be addressed to CERN through mail-KT@cern.ch
 *
 * Author: Giacomo Lunghi CERN EN/SMM/MRO
 *
 *  ==================================================================================================
*/

#define MAX_PENDING_CONNECTIONS 5

#include <arpa/inet.h>
#include <dirent.h>
#include <fcntl.h>
#include <memory>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>
#include <sys/stat.h>

#include <urf/common/logger/Logger.hpp>
#include "common/sockets/ipc/IpcSocket.hpp"
#include "common/sockets/ipc/IpcServer.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("IpcServer");
}

namespace urf {
namespace middleware {
namespace sockets {

IpcServer::IpcServer(const std::string& name) :
    name_("/tmp/ipc_unixdomain/"+name),
    serverFd_(0),
    isOpen_(false),
    connectedClients_() {
        LOGGER.trace("CTor");
}

IpcServer::~IpcServer() {
    LOGGER.trace("DTor");
    ::close(serverFd_);
}

bool IpcServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    DIR* dir = opendir("/tmp/ipc_unixdomain/");
    if (dir) {
        if (chmod("/tmp/ipc_unixdomain/", 0777) != 0) {
            LOGGER.warn("Could not change folder permissions: {}", strerror(errno));
            return false;
        }
        closedir(dir);
    } else if (errno == ENOENT) {
        if (mkdir("/tmp/ipc_unixdomain/", 0777) != 0) {
            LOGGER.warn("Could not create ipc folder: {}", strerror(errno));
            return false;
        }
    } else {
        LOGGER.warn("Could not open ipc directory");
        return false;
    }

    if ((serverFd_ = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        LOGGER.warn("Could not create socket: {}", strerror(errno));
        return false;
    }

    struct sockaddr_un servaddr;
    std::memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sun_family = AF_UNIX;
    strncpy(servaddr.sun_path, name_.c_str(), name_.length());
    if (bind(serverFd_, reinterpret_cast<sockaddr*>(&servaddr), sizeof(servaddr)) == -1) {
        LOGGER.warn("Failed to bind socket: {}", strerror(errno));
        return false;
    }

    if (listen(serverFd_, MAX_PENDING_CONNECTIONS) == -1) {
        LOGGER.warn("Failed to listen on socket: {}", strerror(errno));
        return false;
    }

    if (chmod(servaddr.sun_path, 0777) != 0) {
        LOGGER.warn("Could not change file permissions: {}", strerror(errno));
        return false;
    }

    isOpen_ = true;
    return true;
}

bool IpcServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (size_t i=0; i < connectedClients_.size(); i++) {
        connectedClients_[i]->close();
    }

    connectedClients_.clear();
    getSocketError();
    if (::shutdown(serverFd_, SHUT_RDWR) < 0) {
        if (errno != ENOTCONN && errno != EINVAL)
            LOGGER.warn("Could not make shutdown: {}", strerror(errno));
    }
    if (::close(serverFd_) < 0)
        LOGGER.warn("Could not close server: {}", strerror(errno));

    unlink(name_.c_str());
    isOpen_ = false;
    return true;
}

bool IpcServer::isOpen() {
    LOGGER.trace("isOpen");

    return isOpen_;
}

std::shared_ptr<ITransportSocket> IpcServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }
    struct sockaddr_un client;
    std::memset(&client, 0, sizeof(client));
    uint32_t len = sizeof(client);
    int socket = accept(serverFd_, reinterpret_cast<sockaddr*>(&client), &len);

    if (socket == -1) {
        LOGGER.warn("Could not accept socket: {}", strerror(errno));
        return nullptr;
    }

    LOGGER.info("Client connected on resource {}", name_);
    std::shared_ptr<ITransportSocket> sockPtr(new IpcSocket(socket, name_));
    connectedClients_.push_back(sockPtr);
    return sockPtr;
}

bool IpcServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>&) {
    return false;
}

int IpcServer::getSocketError() {
    int err = 1;
    socklen_t len = sizeof err;
    if (-1 == getsockopt(serverFd_, SOL_SOCKET, SO_ERROR, reinterpret_cast<char*>(&err), &len))
        LOGGER.error("Could not get socket error: {}", strerror(errno));
    if (err)
        errno = err;              // set errno to the socket SO_ERROR
    return err;
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf