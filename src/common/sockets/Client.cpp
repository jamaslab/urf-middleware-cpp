#include "urf/middleware/sockets/Client.hpp"

#include "common/sockets/impl/ClientImpl.hpp"
#include "common/sockets/TransportSocketFactory.hpp"

namespace urf {
namespace middleware {
namespace sockets {

Client::Client(const std::string& socket) :
    impl_(new ClientImpl(TransportSocketFactory::createClient(socket))) {
}

Client::~Client() { }

bool Client::open() {
    return impl_->open();
}

bool Client::close() {
    return impl_->close();
}

bool Client::isOpen() {
    return impl_->isOpen();
}

std::optional<messages::Message> Client::pull() {
    return impl_->pull();
}

std::optional<messages::Message> Client::pull(const std::chrono::milliseconds& timeout) {
    return impl_->pull(timeout);
}

bool Client::push(const messages::Message& message, bool requiresAck) {
    return impl_->push(message, requiresAck);
}

bool Client::onPush(const std::function<void(const messages::Message&)>& handler) {
    return impl_->onPush(handler);
}

std::vector<messages::Message> Client::request(const messages::Message& message) {
    return impl_->request(message);
}
std::vector<messages::Message> Client::request(const messages::Message& message, const std::chrono::milliseconds& timeout) {
    return impl_->request(message, timeout);
}

std::future<std::vector<messages::Message>> Client::requestAsync(const messages::Message& message) {
    return impl_->requestAsync(message);
}
std::future<std::vector<messages::Message>> Client::requestAsync(const messages::Message& message, const std::chrono::milliseconds& timeout) {
    return impl_->requestAsync(message, timeout);
}

std::optional<messages::Message> Client::receiveRequest() {
    return impl_->receiveRequest();
}

std::optional<messages::Message> Client::receiveRequest(const std::chrono::milliseconds& timeout) {
    return impl_->receiveRequest(timeout);
}

bool Client::respond(const messages::Message& message) {
    return impl_->respond(message);
}

bool Client::subscribe(const std::string& topic, const std::chrono::milliseconds& maxRate) {
    return impl_->subscribe(topic, maxRate);
}

uint32_t Client::subscriptionsCount(const std::string& topic) {
    return impl_->subscriptionsCount(topic);
}

bool Client::unsubscribe(const std::string& topic) {
    return impl_->unsubscribe(topic);
}

bool Client::keepUpdateHistory(bool value) {
    return impl_->keepUpdateHistory(value);
}

std::optional<messages::Message> Client::receiveUpdate(const std::string& topic) {
    return impl_->receiveUpdate(topic);
}

std::optional<messages::Message> Client::receiveUpdate(const std::string& topic, const std::chrono::milliseconds& timeout) {
    return impl_->receiveUpdate(topic, timeout);
}

bool Client::onUpdate(const std::string& topic, const std::function<void(const std::string&, const messages::Message&)>& handler) {
    return impl_->onUpdate(topic, handler);
}

bool Client::publish(const std::string& topic, const messages::Message& message, bool requiresAck) {
    return impl_->publish(topic, message, requiresAck);
}

std::vector<std::string> Client::availablePartnerTopics() {
    return impl_->availablePartnerTopics();
}

bool Client::addTopic(const std::string& topic) {
    return impl_->addTopic(topic);
}

bool Client::removeTopic(const std::string& topic)  {
    return impl_->removeTopic(topic);
}

bool Client::automaticallyDeserializeBody(bool value) {
    return impl_->automaticallyDeserializeBody(value);
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
