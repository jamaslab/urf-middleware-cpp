#pragma once

#include <atomic>
#include <chrono>
#include <string>

#include "common/sockets/ITransportSocket.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>

namespace urf {
namespace middleware {
namespace sockets {

class ProcSocket : public ITransportSocket {
 public:
    ProcSocket() = delete;
    explicit ProcSocket(const std::string& name);
    ProcSocket(const std::string& name, ProcSocket* client);
    ProcSocket(const ProcSocket&) = delete;
    ProcSocket(ProcSocket&&) = delete;
    ~ProcSocket() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;
    bool allowsFragmentedRead() override { return true; }

    bool write(const std::vector<uint8_t>& bytes, bool requiresAck) override;
    std::vector<uint8_t> read(int length) override;
    std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) override;

    bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) override;

 private:
    void handleConnectionLost();

    enum Roles { Client, Server };

 private:
    std::string name_;
    Roles role_;
    std::atomic<bool> isOpen_;

    std::atomic<ProcSocket*> partnerSocket_;
    common::containers::ThreadSafeQueue<std::vector<uint8_t>> inputBuffer_;
    std::vector<uint8_t> partialInputBuffer_;
    size_t partialInputBufferPosition_;

    std::vector<std::function<void(ITransportSocket*)>> connectionLostHandlers_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
