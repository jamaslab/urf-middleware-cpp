#include "common/sockets/proc/ProcServer.hpp"
#include "common/sockets/proc/ProcSocket.hpp"

#include <urf/common/logger/Logger.hpp>

namespace {
auto LOGGER = urf::common::getLoggerInstance("ProcServer");
}

namespace urf {
namespace middleware {
namespace sockets {

std::mutex ProcServer::activeServersMtx_;
std::unordered_map<std::string, ProcServer*> ProcServer::activeServers_;

ProcServer::ProcServer(const std::string& name)
    : isOpen_(false)
    , name_(name)
    , connectedClients_()
    , connectingClients_() {
    LOGGER.trace("CTor");
}

ProcServer::~ProcServer() {
    LOGGER.trace("DTor");
    close();
}

bool ProcServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already opened");
        return false;
    }

    std::scoped_lock lock(activeServersMtx_);
    if (activeServers_.count(name_) != 0) {
        LOGGER.warn("Resource is busy");
        return false;
    }

    activeServers_.insert({name_, this});
    connectingClients_ = common::containers::ThreadSafeQueue<ProcSocket*>();
    isOpen_ = true;
    return true;
}

bool ProcServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Not opened");
        return false;
    }

    connectingClients_.dispose();
    for (auto client : connectedClients_) {
        client->close();
    }

    std::scoped_lock lock(activeServersMtx_);
    if (activeServers_.count(name_) == 0) {
        LOGGER.warn("The requested server is not in the active servers map");
        return false;
    }

    activeServers_.erase(name_);
    isOpen_ = false;

    return true;
}

bool ProcServer::isOpen() {
    LOGGER.trace("isOpen()");
    return isOpen_;
}

std::shared_ptr<ITransportSocket> ProcServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    auto client = connectingClients_.pop();
    if (!client) {
        return nullptr;
    }

    std::shared_ptr<ProcSocket> sockPtr(new ProcSocket(name_, client.value()));
    connectedClients_.push_back(sockPtr);

    LOGGER.info("Client connected on {}", name_);
    return sockPtr;
}

bool ProcServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>&) {
    return false;
}

bool ProcServer::connect(std::string serverName, ProcSocket* client) {
    std::scoped_lock lock(activeServersMtx_);
    if (activeServers_.count(serverName) == 0) {
        LOGGER.warn("The requested server is not open");
        return false;
    }

    activeServers_[serverName]->connectingClients_.push(client);
    return true;
}

} // namespace sockets
} // namespace middleware
} // namespace urf
