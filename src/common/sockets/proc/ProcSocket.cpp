#include "common/sockets/proc/ProcSocket.hpp"
#include "common/sockets/proc/ProcServer.hpp"

#include <urf/common/logger/Logger.hpp>

#include <limits>

namespace {
auto LOGGER = urf::common::getLoggerInstance("ProcSocket");
}

namespace urf {
namespace middleware {
namespace sockets {

ProcSocket::ProcSocket(const std::string& name)
    : name_(name)
    , role_(Roles::Client)
    , isOpen_(false)
    , partnerSocket_(nullptr)
    , inputBuffer_()
    , partialInputBuffer_()
    , partialInputBufferPosition_(0)
    , connectionLostHandlers_() {
    LOGGER.trace("CTor");
}

ProcSocket::ProcSocket(const std::string& name, ProcSocket* client)
    : name_(name)
    , role_(Roles::Server)
    , isOpen_(true)
    , partnerSocket_(client)
    , inputBuffer_()
    , partialInputBuffer_()
    , partialInputBufferPosition_(0)
    , connectionLostHandlers_() {
    LOGGER.trace("CTor");
        partnerSocket_.load()->partnerSocket_ = this;
        for (int i=0; i < 20; i++) {
            if (partnerSocket_.load()->isOpen()) {
                break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
}

ProcSocket::~ProcSocket() {
    LOGGER.trace("DTor");
    if (isOpen_) {
        inputBuffer_.dispose();
        partnerSocket_.load()->inputBuffer_.dispose();
    }
}

bool ProcSocket::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    if (role_ == Roles::Client) {
        inputBuffer_ = common::containers::ThreadSafeQueue<std::vector<uint8_t>>();
        partialInputBuffer_ = std::vector<uint8_t>();
        partialInputBufferPosition_ = 0;
        partnerSocket_ = nullptr;

        if (!ProcServer::connect(name_, this)) {
            LOGGER.warn("Could not connect to server");
            return false;
        }

        for (int i=0; i < 20; i++) {
            if (partnerSocket_.load() != nullptr) {
                break;
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }

        if (partnerSocket_.load() == nullptr) {
            LOGGER.warn("Server did not reply on time");
            return false;
        }
    } else {
        LOGGER.warn("You can't reopen a server socket");
        return false;
    }

    isOpen_ = true;
    return true;
}

bool ProcSocket::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    inputBuffer_.dispose();
    partnerSocket_.load()->inputBuffer_.dispose();

    isOpen_ = false;
    return true;
}

bool ProcSocket::isOpen() {
    return isOpen_;
}

bool ProcSocket::write(const std::vector<uint8_t>& bytes, bool) {
    if (!isOpen_) {
        return false;
    }

    if (!partnerSocket_.load()->isOpen()) {
        handleConnectionLost();
        return false;
    }
    partnerSocket_.load()->inputBuffer_.push(bytes);
    return true;
}

std::vector<uint8_t> ProcSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> retval;
    if (partialInputBuffer_.size() != 0) {
        // There's some data in the partialInputBuffer
        if (partialInputBuffer_.size() + partialInputBufferPosition_ >= static_cast<size_t>(length)) {
            // All the data requested is in the partialInputBuffer
            retval = std::vector<uint8_t>(partialInputBuffer_.begin() + partialInputBufferPosition_,
                                          partialInputBuffer_.begin() +
                                              partialInputBufferPosition_ + length);

            partialInputBufferPosition_ += length;
            if (partialInputBufferPosition_ == partialInputBuffer_.size()) {
                partialInputBuffer_ = std::vector<uint8_t>();
                partialInputBufferPosition_ = 0;
            }
            return retval;
        } else {
            // In the partial buffer there's not all the data requested
            retval = std::vector<uint8_t>(partialInputBuffer_.begin() + partialInputBufferPosition_,
                                          partialInputBuffer_.end());
            partialInputBufferPosition_ = 0;
        }
    }

    do {
        if (!partnerSocket_.load()->isOpen()) {
            handleConnectionLost();
            return retval;
        }

        auto bufOpt = inputBuffer_.pop();
        if (inputBuffer_.isDisposed()) {
            if (isOpen_) {
                handleConnectionLost();
            }
            return retval;
        }

        if (!bufOpt) {
            return retval;
        }

        partialInputBuffer_ = bufOpt.value();

        if (length - retval.size() >= partialInputBuffer_.size()) {
            retval.insert(retval.end(), partialInputBuffer_.begin(), partialInputBuffer_.end());
        } else {
            retval.insert(retval.end(),
                          partialInputBuffer_.begin(),
                          partialInputBuffer_.begin() + length - retval.size());
            partialInputBufferPosition_ = length;
        }
    } while (retval.size() < static_cast<size_t>(length));

    return retval;
}

std::vector<uint8_t> ProcSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    std::vector<uint8_t> retval;
    if (partialInputBuffer_.size() != 0) {
        // There's some data in the partialInputBuffer
        if (partialInputBuffer_.size() + partialInputBufferPosition_ >= static_cast<size_t>(length)) {
            // All the data requested is in the partialInputBuffer
            retval = std::vector<uint8_t>(partialInputBuffer_.begin() + partialInputBufferPosition_,
                                          partialInputBuffer_.begin() +
                                              partialInputBufferPosition_ + length);

            partialInputBufferPosition_ += length;
            if (partialInputBufferPosition_ == partialInputBuffer_.size()) {
                partialInputBuffer_ = std::vector<uint8_t>();
                partialInputBufferPosition_ = 0;
            }
            return retval;
        } else {
            // In the partial buffer there's not all the data requested
            retval = std::vector<uint8_t>(partialInputBuffer_.begin() + partialInputBufferPosition_,
                                          partialInputBuffer_.end());
            partialInputBufferPosition_ = 0;
        }
    }

    do {
        if (!partnerSocket_.load()->isOpen()) {
            handleConnectionLost();
            return retval;
        }

        auto bufOpt = inputBuffer_.pop(timeout);
        if (inputBuffer_.isDisposed()) {
            if (isOpen_) {
                handleConnectionLost();
            }
            return retval;
        }

        if (!bufOpt) {
            return retval;
        }

        partialInputBuffer_ = bufOpt.value();

        if (length - retval.size() >= partialInputBuffer_.size()) {
            retval.insert(retval.end(), partialInputBuffer_.begin(), partialInputBuffer_.end());
        } else {
            retval.insert(retval.end(),
                          partialInputBuffer_.begin(),
                          partialInputBuffer_.begin() + length - retval.size());
            partialInputBufferPosition_ = length;
        }
    } while (retval.size() < static_cast<size_t>(length));

    return retval;
}

bool ProcSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onConnectionLost()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

void ProcSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
