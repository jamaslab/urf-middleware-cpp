#pragma once

#include <chrono>
#include <memory>
#include <mutex>
#include <string>
#include <vector>

#include "common/sockets/ITransportSocketServer.hpp"
#include "common/sockets/proc/ProcSocket.hpp"

#include <urf/common/containers/ThreadSafeQueue.hpp>

namespace urf {
namespace middleware {
namespace sockets {

class ProcServer : public ITransportSocketServer {
 public:
    ProcServer() = delete;
    explicit ProcServer(const std::string& name);
    ProcServer(const ProcServer&) = delete;
    ProcServer(ProcServer&&) = delete;
    ~ProcServer() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;

    std::shared_ptr<ITransportSocket> acceptConnection() override;
    bool onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) override;

    static bool connect(std::string serverName, ProcSocket* client);

 private:
    bool isOpen_;
    std::string name_;
    std::vector<std::shared_ptr<ProcSocket> > connectedClients_;

    common::containers::ThreadSafeQueue<ProcSocket*> connectingClients_;

    static std::mutex activeServersMtx_;
    static std::unordered_map<std::string, ProcServer*> activeServers_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
