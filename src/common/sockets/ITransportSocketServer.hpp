#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <string>

#include "common/sockets/ITransportSocket.hpp"

namespace urf {
namespace middleware {
namespace sockets {

/**
 * @brief Generic socket server implementation
 * Generic socket server implementation.
 * All the implementations of this interface must respect the same behaviour.
 * This behaviour is defined in the SocketTests file in the test folder.
 * For a complete description of the various behaviours, please refer to the README.md file for this
 * module.
 */
class ITransportSocketServer {
 public:
    /**
     * @brief Destroy the ITransportSocketServer object
     */
    virtual ~ITransportSocketServer() = default;

    /**
     * @brief Opens the server
     * @return true the server is correctly open
     * @return false otherwise
     */
    virtual bool open() = 0;
    /**
     * @brief Closes the server
     * Closing the server will automatically close all the accepted ITransportSocket
     * @return true the server is correctly closed
     * @return false otherwise
     */
    virtual bool close() = 0;

    /**
     * @brief Returns if the server is open
     * @return true the server is open
     * @return false otherwise
     */
    virtual bool isOpen() = 0;

    /**
     * @brief Accepts an incoming connection
     * Accepts an incoming connection. The operation is fully blocking.
     * Closing the server will force this method to return
     * @return std::shared_ptr<ITransportSocket> if an incoming connection was correctly accepted. The socket is already open
     * @return boost::none otherwise
     */
    virtual std::shared_ptr<ITransportSocket> acceptConnection() = 0;

    /**
     * @brief Handler called on client connection
     * The handler is called on client socket connection.
     * @param handler handler function to invoke
     * @return true if the handler has been correctly added, false otherwise
     */
    virtual bool onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) = 0;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
