#pragma once

#include <chrono>
#include <functional>
#include <memory>
#include <vector>

namespace urf {
namespace middleware {
namespace sockets {

/**
 * @brief Generic socket communication interface
 * Generic socket communication interface.
 * All the implementations of this interface must respect the same behaviour.
 * This behaviour is defined in the SocketTests file in the test folder.
 * For a complete description of the various behaviours, please refer to the README.md file for this
 * module.
 */
class ITransportSocket {
 public:
    /**
     * @brief Destroy the ITransportSocket object
     */
    virtual ~ITransportSocket() = default;

    /**
     * @brief Open the socket
     * @return true if the socket was correctly open
     * @return false otherwise
     */
    virtual bool open() = 0;
    /**
     * @brief Close the socket
     * The partner socket must receive a notification that this socket was closed at the next
     * read/write operation
     * @return true if the socket was correctly closed
     * @return false otherwise
     */
    virtual bool close() = 0;

    /**
     * @brief Return if the socket is open
     *
     * @return true if is open
     * @return false otherwise
     */
    virtual bool isOpen() = 0;

    /**
     * @brief Return true of it's possible to retrieve partial parts of a packet
     * In general, UDP does not allow fetching directly a partial packet
     *
     * @return true if it allows partial packet fetching
     * @return false otherwise
     */
    virtual bool allowsFragmentedRead() = 0;

    /**
     * @brief Writes a buffer on the socket.
     * Writes a buffer on the socket. The entire buffer is written.
     * @param bytes array of bytes to write
     * @param requiresAck flag that allows to send unreliable messages if the transport protocol allows it.
     * @return true wrote the entire buffer
     * @return false otherwise
     */
    virtual bool write(const std::vector<uint8_t>& bytes, bool requiresAck = true) = 0;

    /**
     * @brief Read the specified number of bytes from the socket
     * Returns an empty string if something went wrong.
     * This operation is fully blocking.
     * Closing the socket from another thread interrupts the read operation.
     * @param length number of bytes to read
     * @return std::vector<uint8_t> the read bytes
     */
    virtual std::vector<uint8_t> read(int length) = 0;

    /**
     * @brief Read the specified number of bytes from the socket with a timeout
     * Returns an empty string if something went wrong.
     * This operation is non-blocking and returns when the timeout expired.
     * If the timeout expired, the received bytes so far are returned.
     * Closing the socket from another thread interrupts the read operation
     * @param length number of bytes to read
     * @param timeout timeout
     * @return std::vector<uint8_t> the read bytes
     */
    virtual std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) = 0;

    /**
     * @brief Handler called on partner disconnect
     * The handler is called on partner socket disconnection.
     * @param handler handler function to invoke
     * @return true if the handler has been correctly added, false otherwise
     */
    virtual bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) = 0;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
