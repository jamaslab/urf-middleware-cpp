#include <string>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/tcp/TcpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("TcpSocket");
}

namespace urf {
namespace middleware {
namespace sockets {

TcpSocket::TcpSocket(const std::string& hostname, int port)
    : wsaData_()
    , socket_(INVALID_SOCKET)
    , role_(Roles::Client)
    , isOpen_(false)
    , isBlocking_(true)
    , hostname_(hostname)
    , port_(port) {
    LOGGER.trace("CTor");
}

TcpSocket::TcpSocket(SOCKET socket, const std::string& hostname, int port)
    : wsaData_()
    , socket_(socket)
    , role_(Roles::Server)
    , isOpen_(true)
    , isBlocking_(true)
    , hostname_(hostname)
    , port_(port) {
    LOGGER.trace("CTor");
}

TcpSocket::~TcpSocket() {
    LOGGER.trace("DTor");
    closesocket(socket_);
    WSACleanup();
}

bool TcpSocket::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    auto retval = WSAStartup(MAKEWORD(2, 2), &wsaData_);
    if (retval != 0) {
        LOGGER.warn("WSAStartup failed with error:  {}", retval);
        return false;
    }

    struct hostent* ent = gethostbyname(hostname_.c_str());

    if (ent == NULL) {
        auto dw = WSAGetLastError();
        if (dw == WSAHOST_NOT_FOUND) {
            LOGGER.warn("Could not find host");
            return false;
        }

        if (dw == WSANO_DATA) {
            LOGGER.warn("No data record is found");
            return false;
        }

        LOGGER.warn("Something wrong with the hostname: {}", dw);
    }

    struct in_addr ip_addr;
    ip_addr.s_addr = *(u_long*)ent->h_addr_list[0];

    struct addrinfo hints;
    ZeroMemory(&hints, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;

    // Resolve the address and port
    struct addrinfo* result;
    retval = getaddrinfo(inet_ntoa(ip_addr), std::to_string(port_).c_str(), &hints, &result);
    if (retval != 0) {
        LOGGER.warn("getaddrinfo failed with error: {}", retval);
        WSACleanup();
        return false;
    }

    socket_ = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
    if (socket_ == INVALID_SOCKET) {
        LOGGER.warn("socket failed with error: {}", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    do {
        retval = connect(socket_, result->ai_addr, (int)result->ai_addrlen);
        if (retval == SOCKET_ERROR) {
            LOGGER.error("Failed to connect: {}", WSAGetLastError());
            closesocket(socket_);
            socket_ = INVALID_SOCKET;
            WSACleanup();
            return false;
        }
    } while (retval != 0);

    isOpen_ = true;
    return true;
}

bool TcpSocket::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    shutdown(socket_, SD_SEND);
    closesocket(socket_);
    isOpen_ = false;
    return true;
}

bool TcpSocket::isOpen() {
    return isOpen_;
}

bool TcpSocket::write(const std::vector<uint8_t>& buffer, bool requiresAck) {
    if (!isOpen_) {
        return false;
    }

    size_t sent = 0;
    do {
        int retval = ::send(socket_,
                            reinterpret_cast<const char*>(buffer.data() + sent),
                            static_cast<int>(buffer.size() - sent),
                            0);
        if (retval == -1) {
            if (errno == EAGAIN) {
                continue;
            } else {
                // handleConnectionLost();
                return false;
            }
        }
        sent += static_cast<size_t>(retval);
    } while (sent < buffer.size());
    return true;
}

std::vector<uint8_t> TcpSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (!isBlocking_) {
        setNonBlockingFlag(false);
    }

    return blockingRead(length);
}

std::vector<uint8_t> TcpSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (isBlocking_) {
        setNonBlockingFlag(true);
    }
    return nonBlockingRead(length, timeout);
}

bool TcpSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onPartnerDisconnect()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

bool TcpSocket::setNonBlockingFlag(bool value) {
    LOGGER.trace("Changing non blocking flag to {}", value);
    u_long iMode = value ? 1 : 0;

    if (ioctlsocket(socket_, FIONBIO, &iMode) != 0) {
        LOGGER.warn("Could not set O_NONBLOCK flag: {}", strerror(errno));
        return false;
    }
    isBlocking_ = !value;

    return true;
}

std::vector<uint8_t> TcpSocket::nonBlockingRead(int length,
                                                const std::chrono::milliseconds& timeout) {
    int received = 0;
    std::vector<uint8_t> buffer;
    if (timeout.count() == 0) { // fully non blocking read
        char* cbuf = new char[length];
        int retval = ::recv(socket_, cbuf, length - received, 0);
        if (retval != -1) {
            buffer = std::vector<uint8_t>(cbuf, cbuf + retval);
        }
        delete[] cbuf;
        return buffer;
    }

    struct pollfd fd;
    fd.fd = socket_;
    fd.events = POLLRDNORM;
    auto start = std::chrono::high_resolution_clock::now();
    do {
        auto now = std::chrono::high_resolution_clock::now();
        if ((now - start) > timeout) {
            break;
        }

        if (!isOpen_) {
            break;
        }

        auto retval = WSAPoll(
            &fd,
            1,
            (timeout - std::chrono::duration_cast<std::chrono::milliseconds>(now - start)).count());
        if (retval == -1) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            break;
        } else if (retval == 0) {
            LOGGER.info("Read timeout");
            break;
        }

        char* cbuf = new char[length];
        retval = ::recv(socket_, cbuf, length - received, 0);
        if (retval == -1) {
            if (errno == EAGAIN) {
                delete[] cbuf;
                continue;
            } else {
                delete[] cbuf;
                break;
            }
        }

        if (retval == 0) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            delete[] cbuf;
            break;
        }

        std::copy(cbuf, cbuf + retval, std::back_inserter(buffer));
        received += retval;
        delete[] cbuf;
    } while (received < length);

    return buffer;
}

std::vector<uint8_t> TcpSocket::blockingRead(int length) {
    std::vector<uint8_t> buffer;
    buffer.resize(length);
    int retval = ::recv(socket_, reinterpret_cast<char*>(buffer.data()), length, MSG_WAITALL);

    if (retval <= 0) {
        if (retval == -1)
            LOGGER.warn("Error during blocking read: {}", WSAGetLastError());
        handleConnectionLost();
        return std::vector<uint8_t>();
    }
    return buffer;
}

void TcpSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

} // namespace sockets
} // namespace middleware
} // namespace urf
