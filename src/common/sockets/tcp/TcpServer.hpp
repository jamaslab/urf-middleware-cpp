#pragma once

#include <chrono>
#include <memory>
#include <functional>
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include "common/sockets/ITransportSocketServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class TcpServer : public ITransportSocketServer {
 public:
    TcpServer() = delete;
    explicit TcpServer(uint16_t port);
    TcpServer(const TcpServer&) = delete;
    TcpServer(TcpServer&&) = delete;
    ~TcpServer() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;

    std::shared_ptr<ITransportSocket> acceptConnection() override;
    bool onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>& handler) override;

 private:
    uint16_t port_;

#ifdef __linux__
    int serverFd_;
    int getSocketError();
    bool isInTimedWait();
#elif _WIN32 || _WIN64
    WSADATA data_;
    SOCKET socket_;
#endif

    bool isOpen_;
    std::vector<std::shared_ptr<ITransportSocket> > connectedClients_;
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
