#include <string>

#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/tcp/TcpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("TcpSocket");
}

namespace urf {
namespace middleware {
namespace sockets {

TcpSocket::TcpSocket(const std::string& hostname, int port) :
    socketFd_(0),
    role_(Roles::Client),
    isOpen_(false),
    isBlocking_(true),
    hostname_(hostname),
    port_(port) {
        LOGGER.trace("CTor");
    }

TcpSocket::TcpSocket(int socketFd, const std::string& hostname, int port)  :
    socketFd_(socketFd),
    role_(Roles::Server),
    isOpen_(true),
    isBlocking_(true),
    hostname_(hostname),
    port_(port) {
        LOGGER.trace("CTor");
    }

TcpSocket::~TcpSocket() {
    LOGGER.trace("DTor");
    ::shutdown(socketFd_, SHUT_RDWR);
    ::close(socketFd_);
}

bool TcpSocket::open()  {
    LOGGER.trace("open()");
    if (isOpen_) {
        LOGGER.warn("Already open");
        return false;
    }

    // Ignoring SIGPIPE signal that is triggered when closing during write
    signal(SIGPIPE, SIG_IGN);

    struct sockaddr_in servaddr;
    socketFd_ = socket(AF_INET, SOCK_STREAM, 0);
    if (socketFd_ < 0) {
        LOGGER.warn("Could not open socket: {}", strerror(errno));
        return false;
    }

    auto server = gethostbyname(hostname_.c_str());
    if (server == NULL) {
        LOGGER.warn("Could not get hostname: {}", strerror(errno));
        return false;
    }

    std::memset(reinterpret_cast<char*>(&servaddr), 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    bcopy(reinterpret_cast<char*>(server->h_addr),
        reinterpret_cast<char*>(&servaddr.sin_addr.s_addr),
        server->h_length);
    servaddr.sin_port = htons(port_);

    int one = 1;
    if (setsockopt(socketFd_, SOL_TCP, TCP_NODELAY, &one, sizeof(one)) != 0) {
        LOGGER.warn("Could not set TCP_NODELAY flag: {}", strerror(errno));
        return false;
    }

    int retval;
    do {
        retval = connect(socketFd_, reinterpret_cast<sockaddr*>(&servaddr), sizeof(servaddr));
        if (retval == -1 && errno != EINPROGRESS) {
            LOGGER.warn("Could not connect to server: {}", strerror(errno));
            return false;
        }
    } while (retval != 0);

    isOpen_ = true;
    return true;
}

bool TcpSocket::close()  {
    LOGGER.trace("close()");
    if (!isOpen_) {
        LOGGER.warn("Socket not open");
        return false;
    }

    getSocketError();
    if (::shutdown(socketFd_, SHUT_RDWR) < 0) {
        if (errno != ENOTCONN && errno != EINVAL)
            LOGGER.warn("Could not make shutdown: {}", strerror(errno));
    }

    if (::close(socketFd_) < 0)
        LOGGER.warn("Could not close socket: {}", strerror(errno));

    isOpen_ = false;
    return true;
}

bool TcpSocket::isOpen()  {
    return isOpen_;
}

bool TcpSocket::write(const std::vector<uint8_t>& buffer, bool)  {
    if (!isOpen_) {
        return false;
    }

    size_t sent = 0;
    do {
        int retval = ::send(socketFd_, reinterpret_cast<const char*>(buffer.data()+sent), buffer.size()-sent, 0);
        if (retval == -1) {
            if (errno == EAGAIN) {
                continue;
            } else {
                LOGGER.info("Connection with partner lost");
                // handleConnectionLost();
                return false;
            }
        }
        sent += static_cast<size_t>(retval);
    } while (sent < buffer.size());
    return true;
}

std::vector<uint8_t> TcpSocket::read(int length) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }

    if (!isBlocking_) {
        setNonBlockingFlag(false);
    }

    return blockingRead(length);
}

std::vector<uint8_t> TcpSocket::read(int length, const std::chrono::milliseconds& timeout) {
    if (!isOpen_) {
        return std::vector<uint8_t>();
    }
    if (isBlocking_) {
        setNonBlockingFlag(true);
    }
    return nonBlockingRead(length, timeout);
}

bool TcpSocket::onConnectionLost(const std::function<void(ITransportSocket*)>& handler) {
    LOGGER.trace("onPartnerDisconnect()");

    connectionLostHandlers_.push_back(handler);
    return true;
}

bool TcpSocket::setNonBlockingFlag(bool value) {
    LOGGER.trace("Changing blocking flag to {}", value);
    if (value) {
        int flags = fcntl(socketFd_, F_GETFL);
        if (fcntl(socketFd_, F_SETFL, flags | O_NONBLOCK) != 0) {
            LOGGER.warn("Could not set O_NONBLOCK flag: {}", strerror(errno));
            return false;
        }
        isBlocking_ = false;
    } else {
        int flags = fcntl(socketFd_, F_GETFL);
        if (fcntl(socketFd_, F_SETFL, flags & (~O_NONBLOCK)) != 0) {
            LOGGER.warn("Could not reset O_NONBLOCK flag: {}", strerror(errno));
            return false;
        }
        isBlocking_ = true;
    }

    return true;
}

std::vector<uint8_t> TcpSocket::nonBlockingRead(int length, const std::chrono::milliseconds& timeout) {
    int received = 0;
    std::vector<uint8_t> buffer;
    if (timeout.count() == 0) {  // fully non blocking read
        char* cbuf = new char[length];
        int retval = ::recv(socketFd_, cbuf, length-received, MSG_DONTWAIT);
        if (retval != -1) {
            buffer = std::vector<uint8_t>(cbuf, cbuf+retval);
        }
        delete[] cbuf;
        return buffer;
    }

    struct pollfd fd;
    fd.fd = socketFd_;
    fd.events = POLLIN;
    auto start = std::chrono::high_resolution_clock::now();
    do {
        auto now = std::chrono::high_resolution_clock::now();
        if ((now - start) > timeout) {
            break;
        }

        if (!isOpen_) {
            break;
        }

        auto retval = ::poll(&fd, 1, (timeout - std::chrono::duration_cast<std::chrono::milliseconds>(now - start)).count());
        if (retval == -1) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            break;
        } else if (retval == 0) {
            LOGGER.info("Read timeout");
            break;
        }

        char* cbuf = new char[length];
        retval = ::recv(socketFd_, cbuf, length-received, MSG_DONTWAIT);
        if (retval == -1) {
            if (errno == EAGAIN) {
                delete[] cbuf;
                continue;
            } else {
                delete[] cbuf;
                break;
            }
        }

        if (retval == 0) {
            LOGGER.warn("Connection was lost with partner socket");
            handleConnectionLost();
            delete[] cbuf;
            break;
        }

        std::copy(cbuf, cbuf+retval, std::back_inserter(buffer));
        received += retval;
        delete[] cbuf;
    } while (received < length);

    return buffer;
}

std::vector<uint8_t> TcpSocket::blockingRead(int length) {
    std::vector<uint8_t> buffer;
    buffer.resize(length);    
    int retval = ::recv(socketFd_, buffer.data(), length, MSG_WAITALL);

    if (retval <= 0) {
        if (retval == -1)
            LOGGER.warn("Error during blocking read: {}", strerror(errno));
        handleConnectionLost();
        return std::vector<uint8_t>();
    }
    return buffer;
}

int TcpSocket::getSocketError() {
    int err = 1;
    socklen_t len = sizeof err;
    if (-1 == getsockopt(socketFd_, SOL_SOCKET, SO_ERROR, reinterpret_cast<char*>(&err), &len))
        LOGGER.error("Could not get socket error: {}", strerror(errno));
    if (err)
        errno = err;              // set errno to the socket SO_ERROR
    return err;
}

void TcpSocket::handleConnectionLost() {
    LOGGER.trace("handleConnectionLost()");
    close();
    for (auto h : connectionLostHandlers_) {
        h(this);
    }
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
