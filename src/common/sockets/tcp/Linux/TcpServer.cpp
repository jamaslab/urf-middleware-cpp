#define MAX_PENDING_CONNECTIONS 100

#include <cstring>
#include <memory>
#include <string>

#include <arpa/inet.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/tcp/TcpServer.hpp"
#include "common/sockets/tcp/TcpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("TcpServer");
}

namespace urf {
namespace middleware {
namespace sockets {

TcpServer::TcpServer(uint16_t port) :
    port_(port),
    serverFd_(0),
    isOpen_(false),
    connectedClients_() {
        LOGGER.trace("CTor");
}

TcpServer::~TcpServer() {
    LOGGER.trace("DTor");
    ::shutdown(serverFd_, SHUT_RDWR);
    ::close(serverFd_);
}

bool TcpServer::open() {
    LOGGER.trace("open()");
    if (isOpen_) {
        return false;
    }

    if ((serverFd_ = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        LOGGER.warn("Could not create socket: {}", strerror(errno));
        return false;
    }

    struct sockaddr_in servaddr;
    std::memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = 0;
    servaddr.sin_port = htons(port_);

    int one = 1;
    if (setsockopt(serverFd_, SOL_TCP, TCP_NODELAY, &one, sizeof(one)) != 0) {
        LOGGER.warn("Could not set TCP_NODELAY flag: {}", strerror(errno));
        return false;
    }

    if (setsockopt(serverFd_, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one)) < 0){
        LOGGER.warn("Could not set SO_REUSEADDR flag: {}", strerror(errno));
        return false;
    }

    struct linger sl;
    sl.l_onoff = 1;
    sl.l_linger = 0;
    if (setsockopt(serverFd_, SOL_SOCKET, SO_LINGER, &sl, sizeof(sl)) != 0) {
        LOGGER.warn("Could not set SO_LINGER flag: {}", strerror(errno));
        return false;
    }

    if (bind(serverFd_, reinterpret_cast<sockaddr*>(&servaddr), sizeof(servaddr)) == -1) {
        LOGGER.warn("Failed to bind socket: {}", strerror(errno));
        return false;
    }

    if (listen(serverFd_, MAX_PENDING_CONNECTIONS) == -1) {
        LOGGER.warn("Failed to listen on socket: {}", strerror(errno));
        return false;
    }

    isOpen_ = true;
    return true;
}

bool TcpServer::close() {
    LOGGER.trace("close()");
    if (!isOpen_) {
        return false;
    }

    for (size_t i=0; i < connectedClients_.size(); i++) {
        connectedClients_[i]->close();
    }

    connectedClients_.clear();
    getSocketError();
    if (::shutdown(serverFd_, SHUT_RDWR) < 0) {
        if (errno != ENOTCONN && errno != EINVAL)
            LOGGER.warn("Could not make shutdown: {}", strerror(errno));
    }
    if (::close(serverFd_) < 0)
        LOGGER.warn("Could not close server: {}", strerror(errno));

    isOpen_ = false;
    return true;
}

bool TcpServer::isOpen() {
    LOGGER.trace("isOpen()");
    return isOpen_;
}

std::shared_ptr<ITransportSocket> TcpServer::acceptConnection() {
    LOGGER.trace("acceptConnection()");
    if (!isOpen_) {
        LOGGER.warn("Socket is not open");
        return nullptr;
    }

    struct sockaddr_in client;
    uint32_t len = sizeof(client);
    std::memset(&client, 0, sizeof(client));
    int socket = accept(serverFd_, reinterpret_cast<sockaddr*>(&client), &len);

    if (socket == -1) {
        LOGGER.warn("Could not accept socket: {}", strerror(errno));
        return nullptr;
    }

    char str[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(client.sin_addr), str, INET_ADDRSTRLEN);
    uint16_t client_port = static_cast<uint16_t>(client.sin_port);
    std::shared_ptr<ITransportSocket> sockPtr(new TcpSocket(socket,
        std::string(str), client_port));
    LOGGER.info("Client connected from {}:{} on port {}", std::string(str, INET_ADDRSTRLEN), client_port, port_);
    connectedClients_.push_back(sockPtr);
    return sockPtr;
}

bool TcpServer::onClientConnect(const std::function<std::shared_ptr<ITransportSocket>()>&) {
    return true;
}

int TcpServer::getSocketError() {
    int err = 1;
    socklen_t len = sizeof err;
    if (-1 == getsockopt(serverFd_, SOL_SOCKET, SO_ERROR, reinterpret_cast<char*>(&err), &len))
        LOGGER.error("Could not get socket error: {}", strerror(errno));
    if (err)
        errno = err;              // set errno to the socket SO_ERROR
    return err;
}

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
