#pragma once

#include <atomic>
#include <chrono>
#include <string>
#include <vector>

#if defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/tcp/TcpServer.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class TcpSocket : public ITransportSocket {
 public:
    TcpSocket() = delete;
    TcpSocket(const std::string& hostname, int port);
    TcpSocket(const TcpSocket&) = delete;
    TcpSocket(TcpSocket&&) = delete;
    ~TcpSocket() override;

    bool open() override;
    bool close() override;

    bool isOpen() override;
    bool allowsFragmentedRead() override { return true; }

    bool write(const std::vector<uint8_t>& bytes, bool requiresAck) override;
    std::vector<uint8_t> read(int length) override;
    std::vector<uint8_t> read(int length, const std::chrono::milliseconds& timeout) override;

    bool onConnectionLost(const std::function<void(ITransportSocket*)>& handler) override;

 private:
    friend class TcpServer;

#ifdef __linux__
    int socketFd_;
    TcpSocket(int socketFd, const std::string& hostname, int port);
    int getSocketError();
#elif _WIN32 || _WIN64
    TcpSocket(SOCKET socket, const std::string& hostname, int port);
    WSADATA wsaData_;
    SOCKET socket_;
#endif

    enum Roles { Client, Server };

    Roles role_;
    std::atomic<bool> isOpen_;

    bool isBlocking_;

    std::string hostname_;
    int port_;

    bool setNonBlockingFlag(bool value);
    std::vector<uint8_t> nonBlockingRead(int length, const std::chrono::milliseconds& timeout);
    std::vector<uint8_t> blockingRead(int length);

    std::vector<std::function<void(ITransportSocket*)>> connectionLostHandlers_;
    void handleConnectionLost();
};

}  // namespace sockets
}  // namespace middleware
}  // namespace urf
