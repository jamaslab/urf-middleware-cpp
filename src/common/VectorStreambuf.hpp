#pragma once

#include <ostream>
#include <vector>

namespace urf {
namespace middleware {

class VectorStreambuf : public std::streambuf {
 public:
    VectorStreambuf(size_t initialSize = 0);
    VectorStreambuf(const std::vector<uint8_t>& buffer);
    ~VectorStreambuf() override = default;

    void reserve(size_t size);
    size_t size();
    uint8_t* data();
    void clear();
    std::vector<uint8_t>& ref();

 protected:
    std::streamsize xsputn(const char_type* s, std::streamsize n) override;
    std::streambuf::int_type overflow(std::streambuf::int_type ch) override;
    std::streamsize xsgetn (char* s, std::streamsize n) override;
    int underflow() override;
    int uflow() override;
    std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way, std::ios_base::openmode which = std::ios_base::in | std::ios_base::out) override;

 private:
    std::vector<uint8_t> buffer_;
    size_t putOffPos_;
    size_t getOffPos_;
};

}  // namespace middleware
}  // namespace urf
