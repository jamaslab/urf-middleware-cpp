#pragma once

#include <bitset>
#include <cstring>
#include <vector>

#include "urf/middleware/messages/ISerializable.hpp"

namespace urf {
namespace middleware {
namespace messages {

class ControlPacket {
 public:
    enum class Patterns {
        None = '\0',
        Push = 'p',
        Pull = 'p',
        Request = 'r',
        Response = 'y',
        Subscribe = 's',
        Unsubscribe = 'u',
        TopicList = 't',
        Publish = 'h',
        Timestamp = 'm'
    };

    ControlPacket();
    ControlPacket(Patterns pattern, Patterns subpattern = Patterns::None);
    ControlPacket(Patterns pattern, const std::string& topic);
    ControlPacket(Patterns pattern, uint8_t requestId, Patterns subpattern = Patterns::None);
    ControlPacket(const ControlPacket&) = default;
    ControlPacket(ControlPacket&&) = default;
    ~ControlPacket() = default;

    void setPattern(Patterns pattern);
    void setSubpattern(Patterns pattern);
    void setTopic(const std::string& topic);
    void setRequestId(uint8_t id);

    bool hasTopic() const;
    std::string getTopic() const;
    Patterns getPattern() const;
    Patterns getSubpattern() const;
    uint8_t getRequestId() const;

    std::vector<uint8_t> serialize() const;
    std::ostream& serialize(std::ostream& os) const;
    bool deserialize(const std::vector<uint8_t>& buffer);

    ControlPacket& operator=(const ControlPacket&) = default;
    ControlPacket& operator=(ControlPacket&&) = default;

    static const uint8_t topicMaxLength;

 private:
    std::vector<uint8_t> buffer_;
};

} // namespace messages
} // namespace middleware
} // namespace urf
