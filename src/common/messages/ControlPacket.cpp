#include <bitset>
#include <cstring>
#include <vector>

#include <iostream>

#include "common/messages/ControlPacket.hpp"

namespace urf {
namespace middleware {
namespace messages {

const uint8_t ControlPacket::topicMaxLength = 250;

ControlPacket::ControlPacket()
    : ControlPacket(Patterns::None, Patterns::None) { }

ControlPacket::ControlPacket(ControlPacket::Patterns pattern, ControlPacket::Patterns subpattern)
    : buffer_({static_cast<uint8_t>(pattern), static_cast<uint8_t>(subpattern), '}'}) { }

ControlPacket::ControlPacket(Patterns pattern, const std::string& topic) {
    if ((pattern == Patterns::Request) || (pattern == Patterns::Response)) {
        buffer_.resize(4 + topic.size());
        std::memcpy(buffer_.data() + 3, topic.c_str(), topic.size());
    } else {
        buffer_.resize(3 + topic.size());
        std::memcpy(buffer_.data() + 2, topic.c_str(), topic.size());
    }
    buffer_[0] = static_cast<uint8_t>(pattern);
    buffer_[1] = static_cast<uint8_t>(pattern);
    buffer_[buffer_.size() - 1] = '}';
}

ControlPacket::ControlPacket(Patterns pattern, uint8_t requestId, Patterns subpattern)
    : buffer_({static_cast<uint8_t>(pattern), static_cast<uint8_t>(subpattern), requestId, '}'}) { }

void ControlPacket::setPattern(ControlPacket::Patterns pattern) {
    buffer_[0] = static_cast<uint8_t>(pattern);
}

void ControlPacket::setSubpattern(ControlPacket::Patterns pattern) {
    buffer_[1] = static_cast<uint8_t>(pattern);
}

void ControlPacket::setTopic(const std::string& topic) {
    if (topic.length() > topicMaxLength)
        return;

    if ((getPattern() == Patterns::Request) || (getPattern() == Patterns::Response)) {
        buffer_.resize(4 + topic.size());
        std::memcpy(buffer_.data() + 3, topic.c_str(), topic.size());
    } else {
        buffer_.resize(3 + topic.size());
        std::memcpy(buffer_.data() + 2, topic.c_str(), topic.size());
    }
    buffer_[buffer_.size() - 1] = '}';
}

void ControlPacket::setRequestId(uint8_t id) {
    // TODO: This is not really correct
    if (buffer_.size() == 3) {
        buffer_.push_back('}');
    }

    buffer_[2] = id;
}

bool ControlPacket::hasTopic() const {
    return buffer_.size() > 3;
}

std::string ControlPacket::getTopic() const {
    if (!hasTopic()) {
        return std::string();
    }

    if ((getPattern() == Patterns::Request) || (getPattern() == Patterns::Response)) {
        return std::string(buffer_.begin() + 3, buffer_.end() - 1);
    } else {
        return std::string(buffer_.begin() + 2, buffer_.end() - 1);
    }
}

ControlPacket::Patterns ControlPacket::getPattern() const {
    return static_cast<Patterns>(buffer_[0]);
}

ControlPacket::Patterns ControlPacket::getSubpattern() const {
    return static_cast<Patterns>(buffer_[1]);
}

uint8_t ControlPacket::getRequestId() const {
    if ((getPattern() != Patterns::Request) && (getPattern() != Patterns::Response)) {
        return 0;
    }

    return buffer_[2];
}

std::vector<uint8_t> ControlPacket::serialize() const {
    return buffer_;
}

std::ostream& ControlPacket::serialize(std::ostream& os) const {
    os.put(static_cast<uint8_t>(buffer_.size()));
    os.write(reinterpret_cast<const char*>(buffer_.data()), buffer_.size());
    return os;
}

bool ControlPacket::deserialize(const std::vector<uint8_t>& buffer) {
    if (buffer.size() < 3)
        return false;
    if (buffer[buffer.size() - 1] != '}')
        return false;
    buffer_ = buffer;
    return true;
}

} // namespace messages
} // namespace middleware
} // namespace urf
