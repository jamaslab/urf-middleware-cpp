#include <chrono>
#include <iostream>
#include <sstream>

#include "urf/middleware/messages/Message.hpp"

#include "common/VectorStreambuf.hpp"

namespace urf {
namespace middleware {
namespace messages {

Message::Message()
    : body_()
    , header_()
    , customHeader_(false)
    , bytes_(std::nullopt) { }

Message::Message(const nlohmann::json& data)
    : body_(data)
    , header_()
    , customHeader_(false)
    , bytes_(std::nullopt) { }

Message::Message(const Body& body, const Header& header)
    : body_(body)
    , header_(header)
    , customHeader_(true)
    , bytes_(std::nullopt) { }

Message::Message(Body&& body, Header&& header)
    : body_(std::move(body))
    , header_(std::move(header))
    , customHeader_(true)
    , bytes_(std::nullopt) { }

Body& Message::body() {
    return body_;
}

Header& Message::header() {
    customHeader_ = true;
    return header_;
}

const Body& Message::body() const {
    return body_;
}

const Header& Message::header() const {
    return header_;
}

void Message::body(const Body& body) {
    body_ = body;
}

void Message::body(Body&& body) {
    body_ = std::move(body);
}

void Message::header(const Header& header) {
    customHeader_ = true;
    header_ = header;
}

void Message::header(Header&& header) {
    customHeader_ = true;
    header_ = std::move(header);
}

std::vector<uint8_t> Message::serialize() const {
    VectorStreambuf streamBuf;
    std::ostream os(&streamBuf);

    Header localHeader;
    if (customHeader_) {
        localHeader = header_;
    }

    localHeader.serialize(os);
    body_.serialize(os);

    localHeader.length(static_cast<uint32_t>(streamBuf.size()) - Header::size);
    auto headerBytes = localHeader.serialize();
    std::memcpy(streamBuf.data(), headerBytes.data(), headerBytes.size());

    return streamBuf.ref();
}

std::ostream& Message::serialize(std::ostream& os) const {
    auto initialPosition = os.tellp();

    Header localHeader;
    if (customHeader_) {
        localHeader = header_;
    }

    localHeader.serialize(os);
    body_.serialize(os);

    // We need to re-write the header because we need to input the length and recalculate the CRC
    localHeader.length(static_cast<size_t>(os.tellp()) - Header::size - initialPosition);
    os.seekp(initialPosition, std::ios_base::beg);
    localHeader.serialize(os);
    os.seekp(0, std::ios_base::end);

    return os;
}

bool Message::deserialize() {
    if (!bytes_) {
        return false;
    }
    return deserialize(bytes_.value());
}

bool Message::deserialize(const std::vector<uint8_t>& buffer) {
    if (!header_.deserialize(buffer.data(), Header::size)) {
        return false;
    }

    if (header_.length() + Header::size != buffer.size()) {
        return false;
    }

    if (!body_.deserialize(buffer.data() + Header::size, buffer.size() - Header::size)) {
        return false;
    }

    return true;
}

nlohmann::json& Message::operator[](const std::string& key) {
    return body_.data()[key];
}

const nlohmann::json& Message::operator[](const std::string& key) const {
    return body()[key];
}

nlohmann::json& Message::operator[](size_t index) {
    return body_.data()[index];
}

const nlohmann::json& Message::operator[](size_t index) const {
    return body_.data()[index];
}

bool Message::setBytes(std::vector<uint8_t>&& bytes) {
    bytes_ = std::move(bytes);
    return true;
}

} // namespace messages
} // namespace middleware
} // namespace urf
