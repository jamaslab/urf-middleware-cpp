#include <chrono>

#ifdef __linux__
#    include <arpa/inet.h>
#elif _WIN32 || _WIN64
#    include <winsock2.h>
#endif

#if IS_BIG_ENDIAN
#    define htonll(x) (x)
#    define ntohll(x) (x)
#else
#    define htonll(x) (((uint64_t)htonl((x)&0xFFFFFFFF) << 32) | htonl((x) >> 32))
#    define ntohll(x) (((uint64_t)ntohl((x)&0xFFFFFFFF) << 32) | ntohl((x) >> 32))
#endif

#define BIT_SET(a, b) ((a) |= (1ULL << (b)))
#define BIT_CLEAR(a, b) ((a) &= ~(1ULL << (b)))

#include "urf/middleware/messages/Header.hpp"

namespace urf {
namespace middleware {
namespace messages {

constexpr uint8_t CURRENT_VERSION = 0x01;

Header::Header()
    : Header(0, 0) { }

Header::Header(
    uint32_t length, uint8_t writerId, uint64_t timestamp, bool isCompressed, bool requiresAck)
    : bytes_()
    , timestampReceived_(0)
    , delay_(0) {
    bytes_.resize(size);

    this->length(length);
    this->writerId(writerId);
    this->timestamp(timestamp);
    bytes_[0] = CURRENT_VERSION & 0x03;
    this->isCompressed(isCompressed);
    this->requiresAck(requiresAck);
}

bool Header::isCompressed() const {
    return (bytes_[0] & 0x04) != 0;
}

bool Header::requiresAck() const {
    return (bytes_[0] & 0x08) != 0;
}

uint32_t Header::length() const {
    uint32_t data;
    unpack(bytes_, 1, data);
    return ntohl(data);
}

uint8_t Header::writerId() const {
    return bytes_[13];
}

uint64_t Header::timestamp() const {
    uint64_t data;
    unpack<uint64_t>(bytes_, 5, data);
    return ntohll(data);
}

uint64_t Header::timestampReceived() const {
    return timestampReceived_;
}

uint8_t Header::version() const {
    return bytes_[0] & 0x03;
}

uint64_t Header::delay() const {
    return delay_;
}

bool Header::isValid() const {
    return calculateChecksum() == ((bytes_[0] >> 4) & 0x0F);
}

void Header::isCompressed(bool compressed) {
    if (compressed) {
        BIT_SET(bytes_[0], 2);
    } else {
        BIT_CLEAR(bytes_[0], 2);
    }
}

void Header::requiresAck(bool requiresAck) {
    if (requiresAck) {
        BIT_SET(bytes_[0], 3);
    } else {
        BIT_CLEAR(bytes_[0], 3);
    }
}

void Header::length(uint32_t length) {
    auto tmpLength = htonl(length);
    std::memcpy(bytes_.data() + 1, &tmpLength, sizeof(tmpLength));
}

void Header::writerId(uint8_t writerId) {
    bytes_[13] = writerId;
}

void Header::timestamp(uint64_t timestamp) {
    auto tmpTimestamp = htonll(timestamp);
    std::memcpy(bytes_.data() + 5, &tmpTimestamp, sizeof(tmpTimestamp));
}

void Header::timestampReceived(uint64_t timestamp) {
    timestampReceived_ = timestamp;
}

void Header::delay(uint64_t delay) {
    delay_ = delay;
}

std::vector<uint8_t> Header::serialize() {
    if (timestamp() == 0) {
        timestamp(std::chrono::duration_cast<std::chrono::milliseconds>(
                      std::chrono::high_resolution_clock::now().time_since_epoch())
                      .count());
    }

    bytes_[0] = (calculateChecksum() << 4) + (bytes_[0] & 0x0F);
    return bytes_;
}

std::ostream& Header::serialize(std::ostream& os) {
    serialize();
    os.write(reinterpret_cast<const char*>(bytes_.data()), bytes_.size());
    return os;
}

bool Header::deserialize() {
    return (bytes_.size() == size) && isValid();
}

bool Header::deserialize(const std::vector<uint8_t>& buffer) {
    if (buffer.size() != size) {
        return false;
    }

    bytes_ = buffer;
    return isValid();
}

bool Header::deserialize(const uint8_t* data, size_t length) {
    if (length != size) {
        return false;
    }

    bytes_ = std::vector<uint8_t>(data, data+length);
    return isValid();
}

bool Header::setBytes(std::vector<uint8_t>&& bytes) {
    if (bytes.size() != size) {
        return false;
    }
    bytes_ = std::move(bytes);
    return true;
}

uint8_t Header::calculateChecksum() const {
    uint16_t sum = bytes_[0] & 0x0F;
    for (size_t i = 1; i < size; i++) {
        sum += bytes_[i];
    }

    return static_cast<uint8_t>(sum % 0x0F);
}

} // namespace messages
} // namespace middleware
} // namespace urf
