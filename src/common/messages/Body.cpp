#include <urf/common/logger/Logger.hpp>

#include "urf/middleware/messages/Body.hpp"

#include <cn-cbor/cn-cbor.h>

#include <nlohmann/detail/output/output_adapters.hpp>

#include <iostream>
#include <sstream>

#include "common/VectorStreambuf.hpp"

namespace urf {
namespace middleware {
namespace messages {

namespace {
auto LOGGER = urf::common::getLoggerInstance("Body");

const char* err_name[] = {
    "CN_CBOR_NO_ERROR",
    "CN_CBOR_ERR_OUT_OF_DATA",
    "CN_CBOR_ERR_NOT_ALL_DATA_CONSUMED",
    "CN_CBOR_ERR_ODD_SIZE_INDEF_MAP",
    "CN_CBOR_ERR_BREAK_OUTSIDE_INDEF",
    "CN_CBOR_ERR_MT_UNDEF_FOR_INDEF",
    "CN_CBOR_ERR_RESERVED_AI",
    "CN_CBOR_ERR_WRONG_NESTING_IN_INDEF_STRING",
    "CN_CBOR_ERR_OUT_OF_MEMORY",
    "CN_CBOR_ERR_FLOAT_NOT_SUPPORTED",
};

nlohmann::json cn_cbor_to_nlohmann(const cn_cbor* cbor) {
    switch (cbor->type) {
        case CN_CBOR_FALSE:
            return false;
        case CN_CBOR_TRUE:
            return true;
        case CN_CBOR_NULL:
            return NULL;
        case CN_CBOR_UINT:
            return cbor->v.uint;
        case CN_CBOR_INT:
            return cbor->v.sint;
        case CN_CBOR_BYTES:
            {
                auto vec = std::vector<uint8_t>(cbor->v.bytes, cbor->v.bytes + cbor->length);
                return nlohmann::json::binary(std::move(vec));
            }
        case CN_CBOR_TEXT:
            return std::string(cbor->v.str, cbor->length);
        case CN_CBOR_ARRAY:
            {
                nlohmann::json::array_t data;
                auto child = cbor->first_child;
                while(child != NULL) {
                    data.push_back(cn_cbor_to_nlohmann(child));
                    child = child->next;
                }
                return data;
            }
        case CN_CBOR_MAP:
            {
                nlohmann::json::object_t data;
                auto map_child = cbor->first_child;
                while (map_child != NULL) {
                    std::string key = std::string(map_child->v.str, map_child->length);
                    map_child = map_child->next;
                    data[key] = cn_cbor_to_nlohmann(map_child);
                    map_child = map_child->next;
                }
                return data;
            }
        case CN_CBOR_DOUBLE:
            return cbor->v.dbl;
        case CN_CBOR_FLOAT:
            return cbor->v.f;
        case CN_CBOR_INVALID:
            throw std::runtime_error("An error has occured while parsing");
        default:
            throw std::runtime_error("Received an unsupported type: " + cbor->type);
            return nlohmann::json();
    }
}

} // namespace

Body::Body()
    : data_()
    , bytes_(std::nullopt) { }

Body::Body(const nlohmann::json& data)
    : data_(data)
    , bytes_(std::nullopt) { }

nlohmann::json& Body::data() {
    return data_;
}

const nlohmann::json& Body::data() const {
    return data_;
}

std::vector<uint8_t> Body::serialize() const {
    if (bytes_) {
        return bytes_.value();
    }

    VectorStreambuf streamBuf;
    std::ostream stream(&streamBuf);
    nlohmann::detail::output_adapter outAdapter(stream);
    nlohmann::json::to_cbor(data_, outAdapter);
    return streamBuf.ref();
}

std::ostream& Body::serialize(std::ostream& os) const {
    if (bytes_) {
        os.write(reinterpret_cast<const char*>(bytes_.value().data()), bytes_.value().size());
        return os;
    }

    nlohmann::detail::output_adapter outAdapter(os);
    nlohmann::json::to_cbor(data_, outAdapter);
    return os;
}

bool Body::deserialize() {
    if (!bytes_) {
        return false;
    }
    return deserialize(bytes_.value());
}

bool Body::deserialize(const std::vector<uint8_t>& buffer) {
    struct cn_cbor_errback back;
    const cn_cbor* ret = cn_cbor_decode(buffer.data(), buffer.size(), &back);
    if (!ret) {
        LOGGER.error("Failed to deserialize body: {} at {}", err_name[back.err], back.pos);
        return false;
    }

    try {
        data_ = cn_cbor_to_nlohmann(ret);
    } catch (const std::exception& ex) {
        LOGGER.error("Failed to deserialize body: {}", ex.what());
        return false;
    }

    return true;
}

bool Body::deserialize(const uint8_t* data, size_t length) {
    struct cn_cbor_errback back;
    cn_cbor* ret = cn_cbor_decode(data, length, &back);
    if (!ret) {
        LOGGER.error("Failed to deserialize body: {} at {}", err_name[back.err], back.pos);
        return false;
    }

    try {
        data_ = cn_cbor_to_nlohmann(ret);
    } catch (const std::exception& ex) {
        LOGGER.error("Failed to deserialize body: {}", ex.what());
        cn_cbor_free(ret);
        return false;
    }

    cn_cbor_free(ret);
    return true;
}

Header Body::getHeader() const {
    return Header(static_cast<uint32_t>(serialize().size()), 0);
}

nlohmann::json& Body::operator[](const std::string& key) {
    return data_[key];
}

const nlohmann::json& Body::operator[](const std::string& key) const {
    return data_[key];
}

nlohmann::json& Body::operator[](size_t index) {
    return data_[index];
}

const nlohmann::json& Body::operator[](size_t index) const {
    return data_[index];
}

bool Body::setBytes(std::vector<uint8_t>&& bytes) {
    bytes_ = std::move(bytes);
    return true;
}

std::ostream& operator<<(std::ostream& os, const Body& b) {
    os << b.data_.dump();
    return os;
}

} // namespace messages
} // namespace middleware
} // namespace urf
