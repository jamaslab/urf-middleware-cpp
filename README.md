The Unified Robotic Framework middleware implements a common middleware for robotic applications.

## Requirements
- CMake 3.10
- Python 3.6+
- C++17 compiler
    - GCC 7 or higher
    - Microsoft Visual C++ (MSVC) 2017 or higher

## Supported protocols
Currently, the following protocols are supported:

| Protocol                             | Connection string                                                         | Supported platforms |
| -------------------------------------| --------------------------------------------------------------------------| --------------------|
| Interprocess Communication (Linux)   | Server: ```ipc://<ipc_filename>``` <br> Client ```ipc://<ipc_filename>``` | Linux               |
| Interprocess Communication (Windows) | Server: ```ipc://<port>``` <br> Client ```ipc://<port>```                 | Windows             |
| TCP                                  | Server: ```tcp://*:<port>``` <br> Client ```tcp://<ip_address>:<port>```  | Linux, Windows      |
| QUIC                                 | Server: ```quic://*:<port>&key=<path_to_private_key>&cert=<path_to_certificate>``` <br> Client ```quic://<ip_address>:<port>```| Linux, Windows      |

## Installation
#### Conan
Install conan on your system:
```
$ pip3 install conan
```
For additional information for system wide installation on different platform, please read https://docs.conan.io/en/latest/installation.html

#### Install dependencies
For installing all the dependencies, launch from the root folder of the repository the following command:

```
$ conan install . -if build/conan -s build_type=<Debug|Release|RelWithDebInfo> --build=*
```

This will install locally all the necessary dependencies for the selected build type and build the missing ones.

#### Build the project
For building the project, launch from the root folder of the repository the following command:
```
$ conan build . -if build/conan
```
The command will build the entire repository as well as execute all the automated tests.

#### Install package
Finally, the package can be installed in the system so that it can be used by other projects:
```
$ conan create . -s build_type=<Debug|Release|RelWithDebInfo>
```
The command will build the entire repository as well as execute all the automated tests and finally install the package.

## Usage
After installation, the package ```urf_middleware/<version>``` can be added to your conanfile requirements.
You can add the package to your CMakeLists as follows:
```
find_package(urf_middleware_cpp REQUIRED)
```
The following variables will be set:
- ```${urf_middleware_cpp_LIBRARIES}``` containing the path to the libraries
- ```${urf_middleware_cpp_INCLUDE_DIRS}``` containing the path to the header files

You can then link the package to your target as follows:
```
target_link_libraries(target_name ${urf_middleware_cpp_LIBRARIES})
target_include_directories(target_name PUBLIC ${urf_middleware_cpp_INCLUDE_DIRS})
```

### QUIC Certificates
QUIC requires a public/private key pair for its server instantiation. The certificates must be generated in advance in a known folder and their path must be included in the Server connection string.
Here, the commands necessary for the generation of the key pair are provided.

1. Create a file ca.conf containing the following configuration (remember to fill in the distinguished_name properties):
    ```
    # OpenSSL CA configuration file
    [ ca ]
    default_ca = CA_default

    [ CA_default ]
    default_days = 365
    database = index.txt
    serial = serial.txt
    default_md = sha256
    copy_extensions = copy
    unique_subject = no

    # Used to create the CA certificate.
    [ req ]
    prompt=no
    distinguished_name = distinguished_name
    x509_extensions = extensions

    [ distinguished_name ]
    organizationName = <fill_in_here>
    commonName = <fill_in_here>

    [ extensions ]
    keyUsage = critical,digitalSignature,nonRepudiation,keyEncipherment,keyCertSign
    basicConstraints = critical,CA:true,pathlen:1

    # Common policy for nodes and users.
    [ signing_policy ]
    organizationName = supplied
    commonName = optional

    # Used to sign node certificates.
    [ signing_node_req ]
    keyUsage = critical,digitalSignature,keyEncipherment
    extendedKeyUsage = serverAuth,clientAuth

    # Used to sign client certificates.
    [ signing_client_req ]
    keyUsage = critical,digitalSignature,keyEncipherment
    extendedKeyUsage = clientAuth
    ```
2. Generate an rsa key
    ```
    openssl genrsa -out <key_name>.key 2048
    ```
    or if a password protected key is needed:
    ```
    openssl genrsa -aes256 -passout pass:<password> -out <key_name>.key 2048
    ```
3. Generate the certificate
    ```
    openssl req -new -x509 -key <key_name>.key -out <certificate_name>.cert -config ca.cnf
    ```