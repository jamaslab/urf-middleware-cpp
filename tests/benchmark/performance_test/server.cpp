#include <iostream>

#include <urf/middleware/sockets/Server.hpp>

using urf::middleware::sockets::Server;

int main(int argc, char* argv[]) {
    Server server("test_server", std::string(argv[1]));

    if (!server.open()) {
        std::cout << "Could not open server" << std::endl;
        return -1;
    }

    while (true) {
        auto pulledMsg = server.pull();

        if (pulledMsg)
            server.push(pulledMsg.value());
    }

    return 0;
}