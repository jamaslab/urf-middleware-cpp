#include <fstream>
#include <iostream>

#include <urf/middleware/sockets/Client.hpp>

using urf::middleware::messages::Message;
using urf::middleware::sockets::Client;

int main(int argc, char* argv[]) {
    std::vector<std::shared_ptr<Client>> clients;
    int clientsCount = atoi(argv[1]);

    for (int i = 0; i < clientsCount; i++) {
        clients.push_back(std::make_shared<Client>(std::string(argv[2])));

        if (!clients[i]->open()) {
            std::cout << "Could not open client" << std::endl;
            return -1;
        }
    }

    // std::vector<uint8_t> bytes;
    // bytes.resize(64000);
    // msg["bytes"] = nlohmann::json::binary_t(bytes);

    std::vector<std::future<bool>> futures;

    std::mutex mtx;

    std::ofstream myfile;
    myfile.open("outfile.csv");

    for (int i = 0; i < clientsCount; i++) {
        auto future = std::async(std::launch::async, [=, &mtx, &myfile] {
            Message msg;
            msg["hello"] = "world!";
            msg["id"] = i;

            for (int k = 0; k < 1000; k++) {
                auto start = std::chrono::high_resolution_clock::now();
                clients[i]->push(msg);
                auto pulled = clients[i]->pull();
                auto end = std::chrono::high_resolution_clock::now();

                std::scoped_lock lock(mtx);
                if (!pulled) {
                    myfile << k << ", " << -1 << std::endl;
                } else {
                    if (pulled.value()["id"] != i) {
                        myfile << k << ", " << -2 << std::endl;
                    } else {
                        myfile << k << ", "
                               << std::chrono::duration_cast<std::chrono::microseconds>(end - start)
                                      .count() << std::endl;
                    }
                }
            }

            return true;
        });

        futures.emplace_back(std::move(future));
    }

    for (int i = 0; i < clientsCount; i++) {
        futures[i].get();
    }

    myfile.close();

    return 0;
}