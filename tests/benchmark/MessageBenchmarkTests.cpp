#include <benchmark/benchmark.h>

#include <urf/middleware/messages/Message.hpp>

#include <iostream>

using urf::middleware::messages::Message;
using urf::middleware::messages::Body;

#define MB_20 20971520

static void BM_BodySerialization(benchmark::State& state) {
    // Perform setup here
    Body msg;
    msg["tmp"] = 123;
    std::vector<uint8_t> bigVector;
    bigVector.resize(MB_20);
    msg["buff"] = nlohmann::json::binary(bigVector);

    for (auto _ : state) {
        // This code gets timed
        msg.serialize();
    }
}

static void BM_MessageSerialization(benchmark::State& state) {
    // Perform setup here
    Message msg;
    msg["tmp"] = 123;
    std::vector<uint8_t> bigVector;
    bigVector.resize(MB_20);
    msg["buff"] = nlohmann::json::binary(bigVector);

    for (auto _ : state) {
        // This code gets timed
        msg.serialize();
    }
}

static void BM_MessageDoubleSerialization(benchmark::State& state) {
    // Perform setup here
    Message msg;
    msg["tmp"] = 123;
    std::vector<uint8_t> bigVector;
    bigVector.resize(MB_20);
    msg["buff"] = nlohmann::json::binary(bigVector);

    for (auto _ : state) {
        // This code gets timed
        msg.serialize();
        msg.serialize();
    }
}


static void BM_BodyDeserialization(benchmark::State& state) {
    // Perform setup here
    Body msg;
    msg["tmp"] = 123;
    std::vector<uint8_t> bigVector;
    bigVector.resize(MB_20);
    msg["buff"] = nlohmann::json::binary(bigVector);
    auto bytes = msg.serialize();

    Body newMsg;
    for (auto _ : state) {
        // This code gets timed
        newMsg.deserialize(bytes);
    }

}

static void BM_MessageDeserialization(benchmark::State& state) {
    // Perform setup here
    Message msg;
    msg["tmp"] = 123;
    std::vector<uint8_t> bigVector;
    bigVector.resize(MB_20);
    msg["buff"] = nlohmann::json::binary(bigVector);
    auto bytes = msg.serialize();

    Message newMsg;
    for (auto _ : state) {
        // This code gets timed
        newMsg.deserialize(bytes);
    }
}

// Register the function as a benchmark
BENCHMARK(BM_BodySerialization)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_MessageSerialization)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_MessageDoubleSerialization)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_BodyDeserialization)->Unit(benchmark::kMillisecond);
BENCHMARK(BM_MessageDeserialization)->Unit(benchmark::kMillisecond);
