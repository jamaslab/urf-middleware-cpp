#include <iostream>
#include <thread>

#include <urf/middleware/services/LocalServices.hpp>

using urf::middleware::services::LocalServices;

int main(int argc, char* argv[]) {
    if (argc !=2) {
        std::cout << "Provide service name";
        return -1;
    }

    LocalServices srv;
    srv.registerService(std::string(argv[1]));

    while(true) {
        srv.refreshService(argv[1]);
        std::cout << srv.getServices().dump() << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds(1));
    }
}