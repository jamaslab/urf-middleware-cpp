#include <iostream>
#include <future>

#include <urf/middleware/sockets/Server.hpp>

using urf::middleware::sockets::Server;
using urf::middleware::messages::Message;

int main(int argc, char* argv[]) {
    if (argc < 3) {
        std::cout << "Usage:" << std::endl;
        std::cout << "test_server [service_name] [server string (multiple server strings accepted]" << std::endl;
        return -1;
    }


    std::vector<std::string> serverStrings;
    for (int i=0; i < argc-2; i++) {
        serverStrings.push_back(argv[2+i]);
    }

    Server server(argv[1], serverStrings);
    server.addTopic("topic1");
    if (!server.open()) {
        std::cout << "Failed to open server" << std::endl;
        return -1;
    }
    server.subscribe("*");

    server.onUpdate("*", [](const std::string& topic, const Message& message) {
        auto localCopy = message;
        std::cout << "Received " << localCopy.header().length() << " bytes on topic " << topic << std::endl;
    });

    auto futurePull = std::async(std::launch::async, [&server]() {
        while (server.isOpen()) {
            auto message = server.pull();
            if (!message)  {
                continue;
            }

            std::cout << "Received " << message.value().header().length() << " bytes from pull" << std::endl;
            std::cout << "Content: " << message.value().body().data().dump() << std::endl;
        }
    });

    auto requestResponse = std::async(std::launch::async, [&server]() {
        int id = 0;
        while (server.isOpen()) {
            id++;
            auto messageOpt = server.receiveRequest();
            if (!messageOpt) continue;
            auto message = messageOpt.value();
            message["response"] = "hello!";
            message["id"] = id;
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
            server.respond(message);

            std::cout << "Received " << message.header().length() << " bytes from request" << std::endl;
        }
    });

    std::cout << "Server started" << std::endl;

    while (true) {
        Message message;
        message["hello"] = "this is a push!";
        server.push(message);
        message["hello"] = "this is a publish!";
        server.publish("topic1", message);
        std::this_thread::sleep_for(std::chrono::milliseconds(25));
    }

    std::cout << "Closing server" << std::endl;
    server.close();
}