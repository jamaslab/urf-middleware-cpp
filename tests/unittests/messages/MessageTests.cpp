#include <urf/middleware/messages/Message.hpp>

#include <gtest/gtest.h>
#include <iostream>

using urf::middleware::messages::Body;
using urf::middleware::messages::Header;
using urf::middleware::messages::Message;

TEST(MessageTests, correctlyInitializeEmptyMessage) {
    Message msg;
    ASSERT_TRUE(msg.body().data().empty());
}

TEST(MessageTests, correctlyInitializeWithExistingBodyAndHeader) {
    Body body({{"hello", "world!"}, {"id", 1}});
    auto header = body.getHeader();
    Message msg(body, header);
    ASSERT_EQ(msg["hello"], "world!");
    ASSERT_EQ(msg["id"], 1);
    ASSERT_EQ(msg.header().length(), header.length());
}

TEST(MessageTests, correctlySerializeDeserialize) {
    Body body({{"hello", "world!"}, {"id", 1}});
    auto header = body.getHeader();
    Message msg(body, header);
    ASSERT_EQ(msg["hello"], "world!");
    ASSERT_EQ(msg["id"], 1);

    ASSERT_TRUE(msg.deserialize(msg.serialize()));

    ASSERT_TRUE(msg.setBytes(msg.serialize()));
    ASSERT_TRUE(msg.deserialize());

    ASSERT_EQ(msg["hello"], "world!");
    ASSERT_EQ(msg["id"], 1);
}

TEST(MessageTests, correctlyUseParenthesisOperators) {
    Message msg;
    msg["hello"] = "world!";
    ASSERT_EQ(msg["hello"], "world!");
    msg["id"] = 1;
    ASSERT_EQ(msg["id"], 1);
}

TEST(MessageTests, correctlyUseParenthesisNumericOperators) {
    Message msg;
    msg["hello"] = "world!";
    ASSERT_EQ(msg["hello"], "world!");
    msg["id"] = 1;
    ASSERT_EQ(msg["id"], 1);
}