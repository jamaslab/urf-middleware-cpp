#include <urf/middleware/messages/Body.hpp>

#include <gtest/gtest.h>

using urf::middleware::messages::Body;

TEST(BodyTests, SerializeDeserializeSequence) {
    Body b;
    b.data()["hello"] = "world";
    b.data()["int"] = 1;
    b.data()["float"] = 0.234;
    b.data()["int_vector"] = std::vector<int>({1,2,3});
    b.data()["float_vector"] = std::vector<float>({0.1f,0.2f,0.3f});
    b.data()["true"] = true;
    b.data()["false"] = false;

    auto serialized = b.serialize();
    auto header = b.getHeader();

    ASSERT_EQ(header.length(), serialized.size());

    Body back_body;
    ASSERT_TRUE(back_body.deserialize(serialized));

    ASSERT_EQ(back_body.data()["hello"], "world");
}

TEST(BodyTests, FailsToDeserializeBadCbor) {
    std::string json = "{\"asd\": }";
    std::vector<uint8_t> bytes(json.begin(), json.end());
    Body back_body;
    ASSERT_FALSE(back_body.deserialize(bytes));
}

TEST(BodyTests, serializeReturnsSetBytesIfSets) {
    Body body;
    ASSERT_TRUE(body.setBytes({0x00, 0x01, 0x02, 0x03}));
    ASSERT_EQ(body.serialize().size(), 4);
}

TEST(BodyTests, cantDeserializeIfNotSetBytes) {
    Body body;
    ASSERT_FALSE(body.deserialize());
    ASSERT_TRUE(body.setBytes({0x00, 0x01, 0x02, 0x03}));
    ASSERT_FALSE(body.deserialize());
}

TEST(BodyTests, correctlyUseParenthesisOperator) {
    Body body;
    body.data()["hello"] = "world!";
    ASSERT_EQ(body["hello"], "world!");

    body["world!"] = "hello";
    ASSERT_EQ(body.data()["world!"], "hello");
}

TEST(BodyTests, correctInlineInitialization) {
    Body body({{"hello", "world!"}, {"id", 1}});
    ASSERT_EQ(body["hello"], "world!");
    ASSERT_EQ(body["id"], 1);
}

TEST(BodyTests, correctlyUseParenthesisNumericOperator) {
    Body body;
    body.data()[0] = "world!";
    ASSERT_EQ(body[0], "world!");

    body[1] = "hello";
    ASSERT_EQ(body.data()[1], "hello");
}
