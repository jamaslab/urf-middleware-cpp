#include <chrono>
#include <iostream>

#include <gtest/gtest.h>

#include <urf/middleware/messages/Header.hpp>

using urf::middleware::messages::Header;

TEST(HeaderTests, CorrectlyDefaultConstructHeader) {
    Header header;

    ASSERT_EQ(header.timestamp(), 0);
    ASSERT_EQ(header.writerId(), 0);
    ASSERT_EQ(header.length(), 0);
    ASSERT_FALSE(header.isCompressed());
    ASSERT_TRUE(header.requiresAck());
    ASSERT_EQ(header.version(), 0x01);
}

TEST(HeaderTests, CorrectlyConstructHeader) {
    Header header(12, 2, 21445, true, false);

    ASSERT_EQ(header.timestamp(), 21445);
    ASSERT_EQ(header.writerId(), 2);
    ASSERT_EQ(header.length(), 12);
    ASSERT_TRUE(header.isCompressed());
    ASSERT_FALSE(header.requiresAck());
}

TEST(HeaderTests, correctlyComputeChecksum) {
    Header header(12, 2, 21445, true, false);

    auto bytes = header.serialize();

    uint16_t sum = 0;
    for (size_t i = 1; i < bytes.size(); i++) {
        sum += bytes[i];
    }
    sum += bytes[0] & 0xF;

    ASSERT_EQ(static_cast<uint8_t>((bytes[0] >> 4) & 0x0F), sum % 0x0F);
}

TEST(HeaderTests, CorrectlySetGet) {
    Header header;

    header.timestamp(1234);
    ASSERT_EQ(header.timestamp(), 1234);

    header.requiresAck(false);
    ASSERT_EQ(header.requiresAck(), false);

    header.length(1234);
    ASSERT_EQ(header.length(), 1234);

    header.writerId(123);
    ASSERT_EQ(header.writerId(), 123);

    header.isCompressed(true);
    ASSERT_EQ(header.isCompressed(), true);

    header.delay(10);
    ASSERT_EQ(header.delay(), 10);
}

TEST(HeaderTests, CorrectlySerializeDeserialize) {
    Header header(12, 2, 21445, true, false);
    auto buffer = header.serialize();

    ASSERT_EQ(buffer.size(), 14);
    Header backHeader;
    ASSERT_TRUE(backHeader.deserialize(buffer));

    ASSERT_EQ(header.timestamp(), backHeader.timestamp());
    ASSERT_EQ(header.writerId(), backHeader.writerId());
    ASSERT_EQ(header.length(), backHeader.length());
    ASSERT_EQ(header.isCompressed(), backHeader.isCompressed());
    ASSERT_EQ(header.requiresAck(), backHeader.requiresAck());
}

TEST(HeaderTests, ReturnsFalseOnBadDeserialize) {
    std::vector<uint8_t> buffer;
    buffer.resize(12);

    Header backHeader;
    ASSERT_FALSE(backHeader.deserialize(buffer));
}

TEST(HeaderTests, serializeReturnsSetBytesIfSets) {
    Header header;
    std::vector<uint8_t> bytes;
    bytes.resize(14);
    ASSERT_TRUE(header.setBytes(std::move(bytes)));
    ASSERT_EQ(header.serialize().size(), 14);
}

TEST(HeaderTests, correctlyPlaceCurrentTimestamp) {
    Header header, back_header;

    ASSERT_TRUE(back_header.deserialize(header.serialize()));

    auto timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    ASSERT_NEAR(timestamp, back_header.timestamp(), 100);
}

TEST(HeaderTests, failsDeserializeWithWrongCheckusm) {
    Header header(12, 2, 21445, true, false);
    auto bytes = header.serialize();
    bytes[0] = 0x01;

    ASSERT_FALSE(header.deserialize(bytes));
}
