#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>
#include <string>

#include "common/sockets/PacketSocket.hpp"

#include "mocks/TransportSocketMock.hpp"

using testing::_;
using testing::Return;
using testing::Invoke;
using testing::AtLeast;
using testing::NiceMock;

using urf::middleware::messages::Message;
using urf::middleware::messages::Body;
using urf::middleware::messages::ControlPacket;
using urf::middleware::sockets::PacketSocket;
using urf::middleware::sockets::TransportSocketMock;

class PacketSocketShould : public ::testing::Test {
 protected:
    PacketSocketShould() :
        sut_(),
        socket_(new NiceMock<TransportSocketMock>),
        bufferToRead_(),
        writtenBuffer_() {
    }

    void SetUp() override {
        ON_CALL(*socket_, open()).WillByDefault(Return(true));
        ON_CALL(*socket_, close()).WillByDefault(Return(true));
        ON_CALL(*socket_, isOpen()).WillByDefault(Return(true));
        ON_CALL(*socket_, allowsFragmentedRead()).WillByDefault(Return(true));
        ON_CALL(*socket_, write(_, _)).WillByDefault(Invoke(
            [this](const std::vector<uint8_t>& buffer, bool){
                    writtenBuffer_ = buffer;
                    return true;
        }));

        ON_CALL(*socket_, read(_)).WillByDefault(Invoke([this](int length){
            if (bufferToRead_.size() < static_cast<size_t>(length)) {
                return std::vector<uint8_t>();
            }
            std::vector<uint8_t> str(bufferToRead_.begin(), bufferToRead_.begin()+length);
            bufferToRead_.erase(bufferToRead_.begin(), bufferToRead_.begin()+length);
            return str;
        }));

        ON_CALL(*socket_, read(_, _)).WillByDefault(
            Invoke([this](int length, const std::chrono::milliseconds&){
                if (bufferToRead_.size() < static_cast<size_t>(length)) {
                    return std::vector<uint8_t>();
                }

                std::vector<uint8_t> str(bufferToRead_.begin(), bufferToRead_.begin()+length);
                bufferToRead_.erase(bufferToRead_.begin(), bufferToRead_.begin()+length);
                return str;
        }));

        ON_CALL(*socket_, onConnectionLost(_)).WillByDefault(Return(true));
    }

    ~PacketSocketShould() {}

    void putToReadPacket(const urf::middleware::messages::Body& packet, bool clear = true) {
        if (clear) bufferToRead_.clear();
        std::string divider = "~~~~~~";
        bufferToRead_.insert(bufferToRead_.end(), divider.begin(), divider.end());
        ControlPacket control;
        auto controlBytes = control.serialize();
        bufferToRead_.push_back(controlBytes.size());
        bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end());
        auto header = packet.getHeader().serialize();
        bufferToRead_.insert(bufferToRead_.end(), header.begin(), header.end());
        auto body = packet.serialize();
        bufferToRead_.insert(bufferToRead_.end(), body.begin(), body.end());
    }


    std::unique_ptr<PacketSocket> sut_;
    std::shared_ptr<TransportSocketMock> socket_;

    std::vector<uint8_t> bufferToRead_;
    std::vector<uint8_t> writtenBuffer_;
};

TEST_F(PacketSocketShould, openCloseIsOpenWriteTest) {
    sut_.reset(new PacketSocket(socket_));

    ASSERT_TRUE(sut_->open());
    ASSERT_TRUE(sut_->isOpen());
    ASSERT_TRUE(sut_->close());
}

TEST_F(PacketSocketShould, writeCorrectBytes) {
    sut_.reset(new PacketSocket(socket_));
    Body json;
    json.data()["data"] = "tentative";
    std::vector<uint8_t> asdasd;
    asdasd.resize(20102141);
    json.data()["buff"] = nlohmann::json::binary(asdasd);

    ControlPacket control;
    ASSERT_TRUE(sut_->write(control, Message(json, json.getHeader())));
    ASSERT_TRUE(sut_->write(control, Message(json, json.getHeader())));
    ASSERT_EQ(writtenBuffer_.size(),
        6 + json.serialize().size() + control.serialize().size() + urf::middleware::messages::Header::size + 1);
}

TEST_F(PacketSocketShould, readCorrectBytes) {
    sut_.reset(new PacketSocket(socket_));
    Body json;
    json.data()["data"] = "tentative";

    putToReadPacket(json);
    auto retval = sut_->read();

    ASSERT_TRUE(retval);
    auto [ control, message ] = retval.value();
    ASSERT_EQ(message.body().serialize(), json.serialize());
}

TEST_F(PacketSocketShould, readCorrectBytesWithTimeout) {
    sut_.reset(new PacketSocket(socket_));
    Body json;
    json.data()["data"] = "tentative";

    putToReadPacket(json);
    auto retval = sut_->read(std::chrono::milliseconds(100));

    ASSERT_TRUE(retval);
    auto [ control, message ] = retval.value();
    ASSERT_EQ(message.body().serialize(), json.serialize());
}

TEST_F(PacketSocketShould, correctlyResyncAfterSomeBullshit) {
    sut_.reset(new PacketSocket(socket_));
    Body json;
    ControlPacket control;
    control.setTopic("Ciao");
    json.data()["data"] = "tentative";
    auto headerBytes = json.getHeader().serialize();
    auto bodyBytes = json.serialize();
    auto controlBytes = control.serialize();

    std::string bad_divider = "~~~~~";
    std::string good_divider = "~~~~~~";

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), bad_divider.begin(), bad_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    auto retval = sut_->read();
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read();
    ASSERT_TRUE(retval);
    auto tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end()-1);
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    retval = sut_->read();
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read();
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end()-3);
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    retval = sut_->read();
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read();
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end()-2);
    retval = sut_->read();
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read();
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());
}

TEST_F(PacketSocketShould, correctlyResyncAfterSomeBullshitNonBlocking) {
    sut_.reset(new PacketSocket(socket_));
    Body json;
    ControlPacket control;
    control.setTopic("Ciao");
    json.data()["data"] = "tentative";
    auto headerBytes = json.getHeader().serialize();
    auto bodyBytes = json.serialize();
    auto controlBytes = control.serialize();

    std::string bad_divider = "~~~~~";
    std::string good_divider = "~~~~~~";

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), bad_divider.begin(), bad_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    auto retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_TRUE(retval);
    auto tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end()-1);
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end()-3);
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end());
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());

    bufferToRead_.clear();
    bufferToRead_.insert(bufferToRead_.end(), good_divider.begin(), good_divider.end());
    bufferToRead_.insert(bufferToRead_.end(), controlBytes.begin(), controlBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), headerBytes.begin(), headerBytes.end());
    bufferToRead_.insert(bufferToRead_.end(), bodyBytes.begin(), bodyBytes.end()-2);
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_FALSE(retval);

    putToReadPacket(json, false);
    retval = sut_->read(std::chrono::milliseconds(100));
    ASSERT_TRUE(retval);
    tuple = retval.value();
    ASSERT_EQ(std::get<1>(tuple).body().serialize(), json.serialize());
}


