#include <future>
#include <memory>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/ITransportSocketServer.hpp"

#include "common/sockets/tcp/TcpServer.hpp"
#include "common/sockets/tcp/TcpSocket.hpp"

#include "common/sockets/ipc/IpcServer.hpp"
#include "common/sockets/ipc/IpcSocket.hpp"

#include "common/sockets/proc/ProcServer.hpp"
#include "common/sockets/proc/ProcSocket.hpp"

#ifdef WITH_MSQUIC
#include "common/sockets/quic/QuicServer.hpp"
#include "common/sockets/quic/QuicSocket.hpp"
#endif

#define MB_10 1048576

namespace {
auto LOGGER = urf::common::getLoggerInstance("TransportSocketShould");

std::string certificatesPath =
    std::string(__FILE__).substr(0, std::string(__FILE__).find("unittest")) + "utils/";

} // namespace

using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

using urf::middleware::sockets::ITransportSocket;
using urf::middleware::sockets::ITransportSocketServer;

struct TransportSocketStruct {
    std::shared_ptr<ITransportSocketServer> server;
    std::shared_ptr<ITransportSocketServer> server2;
    std::shared_ptr<ITransportSocket> client1;
    std::shared_ptr<ITransportSocket> client2;
};

class TransportSocketShould : public ::testing::TestWithParam<TransportSocketStruct> {
 protected:
    TransportSocketShould() { }

    void SetUp() override {
        server_ = GetParam().server;
        server2_ = GetParam().server2;
        client1_ = GetParam().client1;
        client2_ = GetParam().client2;
    }

    void TearDown() override {
        if (client1_)
            client1_->close();
        if (client2_)
            client2_->close();
        if (server_)
            server_->close();
        if (server2_)
            server2_->close();
    }

    ~TransportSocketShould() { }

    std::shared_ptr<ITransportSocketServer> server_;
    std::shared_ptr<ITransportSocketServer> server2_;
    std::shared_ptr<ITransportSocket> client1_;
    std::shared_ptr<ITransportSocket> client2_;
};

INSTANTIATE_TEST_SUITE_P(
    TcpSocket,
    TransportSocketShould,
    ::testing::Values(TransportSocketStruct{
        std::make_shared<urf::middleware::sockets::TcpServer>(65031),
        std::make_shared<urf::middleware::sockets::TcpServer>(65031),
        std::make_shared<urf::middleware::sockets::TcpSocket>("localhost", 65031),
        std::make_shared<urf::middleware::sockets::TcpSocket>("127.0.0.1", 65031),
    }));

INSTANTIATE_TEST_SUITE_P(ProcSocket,
                         TransportSocketShould,
                         ::testing::Values(TransportSocketStruct{
                             std::make_shared<urf::middleware::sockets::ProcServer>("temporary"),
                             std::make_shared<urf::middleware::sockets::ProcServer>("temporary"),
                             std::make_shared<urf::middleware::sockets::ProcSocket>("temporary"),
                             std::make_shared<urf::middleware::sockets::ProcSocket>("temporary"),
                         }));

#ifdef WITH_MSQUIC
INSTANTIATE_TEST_SUITE_P(
    QuicSocket,
    TransportSocketShould,
    ::testing::Values(TransportSocketStruct{
        std::make_shared<urf::middleware::sockets::QuicServer>(
            56851,
            certificatesPath + "test_certificate.cert",
            certificatesPath + "test_certificate.key"),
        std::make_shared<urf::middleware::sockets::QuicServer>(
            56851,
            certificatesPath + "test_certificate.cert",
            certificatesPath + "test_certificate.key"),
        std::make_shared<urf::middleware::sockets::QuicSocket>("127.0.0.1", 56851),
        std::make_shared<urf::middleware::sockets::QuicSocket>("localhost", 56851),
    }));
#endif

#ifdef __linux__
INSTANTIATE_TEST_CASE_P(IpcSocketLinux,
                        TransportSocketShould,
                        ::testing::Values(TransportSocketStruct{
                            std::make_shared<urf::middleware::sockets::IpcServer>("ipctest"),
                            std::make_shared<urf::middleware::sockets::IpcServer>("ipctest"),
                            std::make_shared<urf::middleware::sockets::IpcSocket>("ipctest"),
                            std::make_shared<urf::middleware::sockets::IpcSocket>("ipctest"),
                        }));
#elif _WIN32 || _WIN64
INSTANTIATE_TEST_CASE_P(IpcSocketWindows,
                        TransportSocketShould,
                        ::testing::Values(TransportSocketStruct{
                            std::make_shared<urf::middleware::sockets::IpcServer>(54321),
                            std::make_shared<urf::middleware::sockets::IpcServer>(54321),
                            std::make_shared<urf::middleware::sockets::IpcSocket>(54321),
                            std::make_shared<urf::middleware::sockets::IpcSocket>(54321),
                        }));
#endif

TEST_P(TransportSocketShould, openCloseServerSequence) {
    ASSERT_FALSE(server_->isOpen());
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->isOpen());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
    ASSERT_FALSE(server_->acceptConnection());

    ASSERT_TRUE(server_->open());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
}

TEST_P(TransportSocketShould, failsToOpenSecondServerOnSameResource) {
    ASSERT_TRUE(server_->open());
    ASSERT_FALSE(server2_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_TRUE(server2_->open());
    ASSERT_TRUE(server2_->close());
}

TEST_P(TransportSocketShould, correctlyConnectClient) {
    ASSERT_FALSE(client1_->open());
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    // Sleep necessary to ensure acceptConnection is called before open
    
    ASSERT_FALSE(client1_->isOpen());
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    auto serverSocket = waitForConnection.get();
    ASSERT_TRUE(serverSocket);
    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, acceptConnectionReturnsIfServerCloses) {
    std::atomic<bool> asyncStartFlag = false;
    ASSERT_TRUE(server_->open());
    auto waitForConnection = std::async(std::launch::async, [this, &asyncStartFlag] {
        asyncStartFlag = true;
        return server_->acceptConnection();
    });

    while (!asyncStartFlag) {
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(waitForConnection.get());
}

TEST_P(TransportSocketShould, clientConnectDisconnectSequence) {
    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    // Sleep necessary to ensure acceptConnection is called before open
    
    ASSERT_FALSE(client1_->isOpen());
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    ASSERT_TRUE(waitForConnection.get());
    
    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    
    LOGGER.info("Client 2 open");
    ASSERT_TRUE(client2_->open());
    ASSERT_TRUE(waitForConnection.get());

    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    LOGGER.info("Client 1 close");
    ASSERT_TRUE(client1_->close());
    LOGGER.info("Client 2 close");
    ASSERT_TRUE(client2_->close());
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());

    LOGGER.info("Second round");

    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    LOGGER.info("Client 1 open");
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(waitForConnection.get());
    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    LOGGER.info("Client 2 open");
    ASSERT_TRUE(client2_->open());
    ASSERT_TRUE(waitForConnection.get());

    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());

    LOGGER.info("Client 1 close");
    ASSERT_TRUE(client1_->close());
    LOGGER.info("Client 2 close");
    ASSERT_TRUE(client2_->close());
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());

    LOGGER.info("Server close");
    ASSERT_TRUE(server_->close());
    LOGGER.info("End");
}

TEST_P(TransportSocketShould, writeReadSuccessFromClient) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    ASSERT_TRUE(client1_->write(buffer));
    auto read = serverSock->read(4, std::chrono::milliseconds(100));
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);
    ASSERT_TRUE(serverSock->write(buffer));
    read = client1_->read(4, std::chrono::milliseconds(100));
    ASSERT_EQ(read.size(), 4);
    textBack = std::string(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, readTimeout) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto read = serverSock->read(4, std::chrono::milliseconds(5));
    ASSERT_EQ(read.size(), 0);
    read = client1_->read(4, std::chrono::milliseconds(5));
    ASSERT_EQ(read.size(), 0);

    ASSERT_TRUE(!client1_->isOpen() || client1_->close());
    ASSERT_TRUE(!server_->isOpen() || server_->close());
}

TEST_P(TransportSocketShould, acceptSocketReturnsIfServerGetsClosed) {
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(waitForConnection.get());
}

TEST_P(TransportSocketShould, correctlyUseBlockingAndNonBlockingRead) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    // ASSERT_TRUE(serverSock->write(buffer));
    // auto read = client1_->read(4, std::chrono::milliseconds(100));
    // ASSERT_EQ(read.size(), 4);
    ASSERT_TRUE(serverSock->write(buffer));
    auto read = client1_->read(4);
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, correctlyUseFullyNonBlockingRead) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    ASSERT_TRUE(serverSock->write(buffer));
    // Sometimes the underlying driver is async and the write is deffered so we sleep a bit
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    auto read = client1_->read(4, std::chrono::milliseconds(0));
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(serverSock->write(buffer));

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, readOperationReturnsIfSameSocketGetsClosedFullBlocking) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));
    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, writeUnreliableData) {
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    std::vector<uint8_t> data({1});
    ASSERT_TRUE(client1_->write(data, false));

    auto read = serverSocketOpt->read(1);

    ASSERT_EQ(read.size(), 1);
    ASSERT_EQ(read[0], 1);
}

TEST_P(TransportSocketShould, readOperationReturnsIfSameSocketGetsClosedTimeout) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4, std::chrono::seconds(10));
        return read.size() != 0;
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));
    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, readOperationReturnsPartialBufferIfSameSocketGetsClosedTimeout) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ci");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    serverSock->write(buffer);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4, std::chrono::seconds(10));
        return read.size();
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    ASSERT_TRUE(client1_->close());
    ASSERT_EQ(readReturn.get(), 2);
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, readWriteOperationReturnsIfServerSocketGetsClosed) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(serverSock->close());

    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, readWriteOperationReturnsIfClientSocketGetsClosed) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());

    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, connectionLostEventFiredOnDisconnect) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    bool handlerCalled = false;
    serverSock->onConnectionLost([&handlerCalled](ITransportSocket*) { handlerCalled = true; });
    auto readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(handlerCalled);

    ASSERT_TRUE(server_->close());
}

TEST_P(TransportSocketShould, closeServerClosesConnectedClients) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
}

TEST_P(TransportSocketShould, DISABLED_performanceTest) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();

    std::chrono::time_point<std::chrono::high_resolution_clock> serverStartsWriting,
        clientStartsReading, serverStoppedWriting, clientStoppedReading;

    auto clientReads = std::async(
        std::launch::async, [this, &clientStartsReading, &clientStoppedReading] { // NOLINT
            clientStartsReading = std::chrono::high_resolution_clock::now();
            auto buffer = client1_->read(MB_10);
            clientStoppedReading = std::chrono::high_resolution_clock::now();
            return buffer.size() == MB_10;
        });

    auto serverWrites =
        std::async(std::launch::async,
                   [this, &serverSocketOpt, &serverStartsWriting, &serverStoppedWriting] { // NOLINT
                       std::vector<uint8_t> buffer;
                       buffer.resize(MB_10);
                       serverStartsWriting = std::chrono::high_resolution_clock::now();
                       if (!serverSocketOpt.get()->write(buffer)) {
                           return false;
                       }
                       serverStoppedWriting = std::chrono::high_resolution_clock::now();
                       return true;
                   });

    ASSERT_TRUE(clientReads.get());
    ASSERT_TRUE(serverWrites.get());

    LOGGER.info("Server took {} us to write {} MB",
                std::chrono::duration_cast<std::chrono::microseconds>(serverStoppedWriting -
                                                                      serverStartsWriting)
                    .count(), // NOLINT
                MB_10 / 1e6);
    LOGGER.info("Client took {} us to read {} MB",
                std::chrono::duration_cast<std::chrono::microseconds>(clientStoppedReading -
                                                                      clientStartsReading)
                    .count(), // NOLINT
                MB_10 / 1e6);
    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(serverSocketOpt.get()->close());
    ASSERT_TRUE(server_->close());
}
