#include <future>
#include <memory>
#include <vector>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/common/logger/Logger.hpp>

#include "common/sockets/ITransportSocket.hpp"
#include "common/sockets/ITransportSocketServer.hpp"

#include "common/sockets/udp/UdpServer.hpp"
#include "common/sockets/udp/UdpSocket.hpp"

namespace {
auto LOGGER = urf::common::getLoggerInstance("UdpTransportSocketShould");
} // namespace

using testing::_;
using testing::AtLeast;
using testing::Invoke;
using testing::NiceMock;
using testing::Return;

using urf::middleware::sockets::UdpServer;
using urf::middleware::sockets::UdpSocket;
using urf::middleware::sockets::ITransportSocket;

class UdpTransportSocketShould : public ::testing::Test {
 protected:
    UdpTransportSocketShould() { }

    void SetUp() override {
        server_ = std::make_shared<UdpServer>(8081);
        server2_ = std::make_shared<UdpServer>(8081);
        client1_ = std::make_shared<UdpSocket>("localhost", 8081);
        client2_ = std::make_shared<UdpSocket>("localhost", 8081);
    }

    void TearDown() override {
        if (client1_)
            client1_->close();
        if (client2_)
            client2_->close();
        if (server_)
            server_->close();
        if (server2_)
            server2_->close();
    }

    ~UdpTransportSocketShould() { }

    std::shared_ptr<UdpServer> server_;
    std::shared_ptr<UdpServer> server2_;
    std::shared_ptr<UdpSocket> client1_;
    std::shared_ptr<UdpSocket> client2_;
};

TEST_F(UdpTransportSocketShould, openCloseServerSequence) {
    ASSERT_FALSE(server_->isOpen());
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->isOpen());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
    ASSERT_FALSE(server_->acceptConnection());

    ASSERT_TRUE(server_->open());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
}

TEST_F(UdpTransportSocketShould, failsToOpenSecondServerOnSameResource) {
    ASSERT_TRUE(server_->open());
    ASSERT_FALSE(server2_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_TRUE(server2_->open());
    ASSERT_TRUE(server2_->close());
}

TEST_F(UdpTransportSocketShould, acceptConnectionReturnsIfServerCloses) {
    std::atomic<bool> asyncStartFlag = false;
    ASSERT_TRUE(server_->open());
    auto waitForConnection = std::async(std::launch::async, [this, &asyncStartFlag] {
        asyncStartFlag = true;
        return server_->acceptConnection();
    });

    while (!asyncStartFlag) {
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(waitForConnection.get());
}


TEST_F(UdpTransportSocketShould, correctlyConnectClient) {
    ASSERT_FALSE(client1_->open());
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    // Sleep necessary to ensure acceptConnection is called before open

    ASSERT_FALSE(client1_->isOpen());
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    auto serverSocket = waitForConnection.get();
    ASSERT_TRUE(serverSocket);
    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}


TEST_F(UdpTransportSocketShould, clientConnectDisconnectSequence) {
    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    // Sleep necessary to ensure acceptConnection is called before open

    ASSERT_FALSE(client1_->isOpen());
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    ASSERT_TRUE(waitForConnection.get());

    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    LOGGER.info("Client 2 open");
    ASSERT_TRUE(client2_->open());
    ASSERT_TRUE(waitForConnection.get());

    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    LOGGER.info("Client 1 close");
    ASSERT_TRUE(client1_->close());
    LOGGER.info("Client 2 close");
    ASSERT_TRUE(client2_->close());
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());

    LOGGER.info("Second round");

    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    LOGGER.info("Client 1 open");
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(waitForConnection.get());
    waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    LOGGER.info("Client 2 open");
    ASSERT_TRUE(client2_->open());
    ASSERT_TRUE(waitForConnection.get());

    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());

    LOGGER.info("Client 1 close");
    ASSERT_TRUE(client1_->close());
    LOGGER.info("Client 2 close");
    ASSERT_TRUE(client2_->close());
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());

    LOGGER.info("Server close");
    ASSERT_TRUE(server_->close());
    LOGGER.info("End");
}

TEST_F(UdpTransportSocketShould, writeReadSuccessFromClient) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    ASSERT_TRUE(client1_->write(buffer));
    auto read = serverSock->read(4);
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);
    ASSERT_TRUE(serverSock->write(buffer));
    read = client1_->read(4, std::chrono::milliseconds(100));
    ASSERT_EQ(read.size(), 4);
    textBack = std::string(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, readTimeout) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto read = serverSock->read(4, std::chrono::milliseconds(5));
    ASSERT_EQ(read.size(), 0);
    read = client1_->read(4, std::chrono::milliseconds(5));
    ASSERT_EQ(read.size(), 0);

    ASSERT_TRUE(!client1_->isOpen() || client1_->close());
    ASSERT_TRUE(!server_->isOpen() || server_->close());
}

TEST_F(UdpTransportSocketShould, acceptSocketReturnsIfServerGetsClosed) {
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(waitForConnection.get());
}

TEST_F(UdpTransportSocketShould, correctlyUseBlockingAndNonBlockingRead) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    ASSERT_TRUE(serverSock->write(buffer));
    auto read = client1_->read(4, std::chrono::milliseconds(100));
    ASSERT_EQ(read.size(), 4);
    ASSERT_TRUE(serverSock->write(buffer));
    read = client1_->read(4);
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, correctlyUseFullyNonBlockingRead) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });
    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ciao");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    ASSERT_TRUE(serverSock->write(buffer));
    // Sometimes the underlying driver is async and the write is deffered so we sleep a bit
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    auto read = client1_->read(4, std::chrono::milliseconds(0));
    ASSERT_EQ(read.size(), 4);
    std::string textBack(read.data(), read.data() + 4);
    ASSERT_EQ(textBack, text);

    ASSERT_TRUE(serverSock->write(buffer));

    ASSERT_TRUE(client1_->close());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, readOperationReturnsIfSameSocketGetsClosedFullBlocking) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));
    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, writeUnreliableData) {
    ASSERT_TRUE(server_->open());
    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    std::vector<uint8_t> data({1, 2, 3});
    ASSERT_TRUE(client1_->write(data, false));

    auto read = serverSocketOpt->read(1);

    ASSERT_EQ(read.size(), 3);
    ASSERT_EQ(read[0], 1);
}

TEST_F(UdpTransportSocketShould, readOperationReturnsIfSameSocketGetsClosedTimeout) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4, std::chrono::seconds(10));
        return read.size() != 0;
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));
    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, readOperationReturnsPartialBufferIfSameSocketGetsClosedTimeout) {
    std::mutex mtx;
    std::condition_variable cv;
    bool inread = false;
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    std::string text("ci");
    std::vector<uint8_t> buffer(text.begin(), text.end());
    serverSock->write(buffer);

    auto readReturn = std::async(std::launch::async, [this, &mtx, &cv, &inread] {
        {
            std::unique_lock<std::mutex> lock(mtx);
            cv.notify_all();
            inread = true;
        }
        auto read = client1_->read(4, std::chrono::seconds(10));
        return read.size();
    });

    std::unique_lock<std::mutex> lock(mtx);
    ASSERT_TRUE(cv.wait_for(lock, std::chrono::seconds(1), [&inread] { return inread; }));

    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    ASSERT_TRUE(client1_->close());
    ASSERT_EQ(readReturn.get(), 2);
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, readWriteOperationReturnsIfServerSocketGetsClosed) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(serverSock->close());

    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, readWriteOperationReturnsIfClientSocketGetsClosed) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());

    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, connectionLostEventFiredOnDisconnect) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    bool handlerCalled = false;
    serverSock->onConnectionLost([&handlerCalled](ITransportSocket*) { handlerCalled = true; });
    auto readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(client1_->close());
    ASSERT_FALSE(readReturn.get());
    ASSERT_TRUE(handlerCalled);

    ASSERT_TRUE(server_->close());
}

TEST_F(UdpTransportSocketShould, closeServerClosesConnectedClients) {
    ASSERT_TRUE(server_->open());

    auto waitForConnection =
        std::async(std::launch::async, [this] { return server_->acceptConnection(); });

    ASSERT_TRUE(client1_->open());
    auto serverSocketOpt = waitForConnection.get();
    ASSERT_TRUE(serverSocketOpt);
    auto serverSock = serverSocketOpt.get();

    auto readReturn = std::async(std::launch::async, [this] {
        auto read = client1_->read(4);
        return read.size() != 0;
    });

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4);
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
    readReturn = std::async(std::launch::async, [this, &serverSock] {
        auto read = serverSock->read(4, std::chrono::milliseconds(100));
        return read.size() != 0;
    });
    ASSERT_FALSE(readReturn.get());
}
