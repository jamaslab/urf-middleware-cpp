#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include <memory>
#include <string>

#include "common/sockets/TransportSocketFactory.hpp"

using testing::_;
using testing::Return;
using testing::Invoke;
using testing::AtLeast;
using testing::NiceMock;

using urf::middleware::sockets::TransportSocketFactory;

namespace {
std::string certificatesPath =
    std::string(__FILE__).substr(0, std::string(__FILE__).find("unittest")) + "utils/";
}

TEST(TransportSocketFactoryShould, throwsExceptionIfMissingProtocol) {
    ASSERT_THROW(TransportSocketFactory::createClient("sdfadf"), std::invalid_argument);
    ASSERT_THROW(TransportSocketFactory::createServer("sdfadf"), std::invalid_argument);
}

TEST(TransportSocketFactoryShould, throwsExceptionIfUknownProtocol) {
    ASSERT_THROW(TransportSocketFactory::createClient("urf://asdasd.asd"), std::invalid_argument);
    ASSERT_THROW(TransportSocketFactory::createServer("urf://asdasd.asd"), std::invalid_argument);
}

TEST(TransportSocketFactoryShould, correctlyCreatesTcpClientAndServer) {
    ASSERT_NO_THROW(TransportSocketFactory::createClient("tcp://hostname:8080"));
    ASSERT_NO_THROW(TransportSocketFactory::createClient("tcp://192.168.0.1:8080"));
    ASSERT_NO_THROW(TransportSocketFactory::createServer("tcp://*:8080"));
}

TEST(TransportSocketFactoryShould, correctlyCreatesProcClientAndServer) {
    ASSERT_NO_THROW(TransportSocketFactory::createClient("proc://tmp"));
    ASSERT_NO_THROW(TransportSocketFactory::createServer("proc://tmp"));
}

#ifdef WITH_MSQUIC
TEST(TransportSocketFactoryShould, correctlyCreatesQuicClientAndServer) {
    ASSERT_NO_THROW(TransportSocketFactory::createClient("quic://localhost:3333"));
    ASSERT_NO_THROW(TransportSocketFactory::createServer("quic://*:3333&key="+certificatesPath+"test_certificate.key&cert="+certificatesPath+"test_certificate.cert"));
}
#endif

#ifdef __linux__
TEST(TransportSocketFactoryShould, correctlyCreatesIpcClientAndServer) {
    ASSERT_NO_THROW(TransportSocketFactory::createClient("ipc://ipc_test"));
    ASSERT_NO_THROW(TransportSocketFactory::createServer("ipc://ipc_test"));
}
#elif _WIN32 || _WIN64
TEST(TransportSocketFactoryShould, correctlyCreatesIpcClientAndServer) {
    ASSERT_NO_THROW(TransportSocketFactory::createClient("ipc://5431"));
    ASSERT_NO_THROW(TransportSocketFactory::createServer("ipc://5431"));
}
#endif
