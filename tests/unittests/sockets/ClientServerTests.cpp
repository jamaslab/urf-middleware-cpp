#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <condition_variable>
#include <iostream>
#include <memory>
#include <mutex>
#include <string>

#include <urf/middleware/sockets/Client.hpp>
#include <urf/middleware/sockets/Server.hpp>

#include <urf/middleware/services/LocalServices.hpp>

using urf::middleware::messages::Message;
using urf::middleware::services::LocalServices;
using urf::middleware::sockets::Client;
using urf::middleware::sockets::Server;

namespace {

std::string certificatesPath =
    std::string(__FILE__).substr(0, std::string(__FILE__).find("unittest")) + "utils/";

} // namespace

struct SocketsStringsStruct {
    std::vector<std::string> server_string;
    std::string service_name;
    std::string client1_string;
    std::string client2_string;
};

class ClientServerShould : public ::testing::TestWithParam<SocketsStringsStruct> {
 protected:
    ClientServerShould()
        : server_()
        , client1_()
        , client2_() { }

    ~ClientServerShould() { }

    void SetUp() override { }

    void TearDown() override {
        if (client1_) {
            client1_->close();
            client1_.reset(nullptr);
        }

        if (client2_) {
            client2_->close();
            client2_.reset(nullptr);
        }

        if (server_) {
            server_->close();
            server_.reset(nullptr);
        }
    }

    std::unique_ptr<Server> server_;
    std::unique_ptr<Client> client1_, client2_;
};

INSTANTIATE_TEST_CASE_P(ProcSocket,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{{"proc://test_proc"},
                                                               "test_server",
                                                               "proc://test_proc",
                                                               "proc://test_proc"}));

INSTANTIATE_TEST_CASE_P(TcpSocket,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{{"tcp://*:3001"},
                                                               "test_server",
                                                               "tcp://localhost:3001",
                                                               "tcp://localhost:3001"}));

INSTANTIATE_TEST_CASE_P(UdpSocket,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{{"udp://*:3001"},
                                                               "test_server",
                                                               "udp://localhost:3001",
                                                               "udp://localhost:3001"}));

#ifdef __linux__
INSTANTIATE_TEST_CASE_P(IpcSocketLinux,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{{"ipc://test_ipc_server_client"},
                                                               "test_server",
                                                               "ipc://test_ipc_server_client",
                                                               "ipc://test_ipc_server_client"}));

INSTANTIATE_TEST_CASE_P(MultipleChannelsLinux,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{
                            {"tcp://*:54321", "ipc://test_ipc_server_client"},
                            "test_server",
                            "tcp://localhost:54321",
                            "ipc://test_ipc_server_client"}));

#elif _WIN32 || _WIN64
INSTANTIATE_TEST_CASE_P(IpcSocketWindows,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{
                            {"ipc://3002"}, "test_server", "ipc://3002", "ipc://3002"}));

INSTANTIATE_TEST_CASE_P(MultipleChannelsWindows,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{{"tcp://*:3001", "ipc://3002"},
                                                               "test_server",
                                                               "tcp://localhost:3001",
                                                               "ipc://3002"}));
#endif

#ifdef WITH_MSQUIC
INSTANTIATE_TEST_CASE_P(QuicSocket,
                        ClientServerShould,
                        ::testing::Values(SocketsStringsStruct{
                            {"quic://*:3333&key=" + certificatesPath +
                             "test_certificate.key&cert=" + certificatesPath +
                             "test_certificate.cert"},
                            "quic_test_server",
                            "quic://localhost:3333",
                            "quic://127.0.0.1:3333"}));
#endif

TEST_P(ClientServerShould, allClientOperationsFailIfNotOpen) {
    client1_.reset(new Client(GetParam().client1_string));

    urf::middleware::messages::Message testMessage;

    ASSERT_FALSE(client1_->pull());
    ASSERT_FALSE(client1_->pull(std::chrono::milliseconds(100)));
    ASSERT_FALSE(client1_->push(testMessage));

    ASSERT_TRUE(client1_->request(testMessage).empty());
    ASSERT_TRUE(client1_->request(testMessage, std::chrono::milliseconds(100)).empty());

    ASSERT_TRUE(client1_->requestAsync(testMessage).get().empty());
    ASSERT_TRUE(client1_->requestAsync(testMessage, std::chrono::milliseconds(100)).get().empty());

    ASSERT_FALSE(client1_->receiveRequest());
    ASSERT_FALSE(client1_->receiveRequest(std::chrono::milliseconds(100)));
    ASSERT_FALSE(client1_->respond(testMessage));

    ASSERT_FALSE(client1_->subscribe("topic"));
    ASSERT_FALSE(client1_->unsubscribe("topic"));
    ASSERT_FALSE(client1_->receiveUpdate("topic"));
    ASSERT_FALSE(client1_->receiveUpdate("topic", std::chrono::milliseconds(100)));
    ASSERT_FALSE(client1_->publish("topic", testMessage));
    ASSERT_TRUE(client1_->availablePartnerTopics().empty());
}

TEST_P(ClientServerShould, allServerOperationsFailIfNotOpen) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));

    urf::middleware::messages::Message testMessage;

    ASSERT_FALSE(server_->pull());
    ASSERT_FALSE(server_->pull(std::chrono::milliseconds(100)));
    ASSERT_FALSE(server_->push(testMessage));

    ASSERT_TRUE(server_->request(testMessage).empty());
    ASSERT_TRUE(server_->request(testMessage, std::chrono::milliseconds(100)).empty());

    ASSERT_TRUE(server_->requestAsync(testMessage).get().empty());
    ASSERT_TRUE(server_->requestAsync(testMessage, std::chrono::milliseconds(100)).get().empty());

    ASSERT_FALSE(server_->receiveRequest());
    ASSERT_FALSE(server_->receiveRequest(std::chrono::milliseconds(100)));
    ASSERT_FALSE(server_->respond(testMessage));

    ASSERT_FALSE(server_->subscribe("topic"));
    ASSERT_FALSE(server_->unsubscribe("topic"));
    ASSERT_FALSE(server_->receiveUpdate("topic"));
    ASSERT_FALSE(server_->receiveUpdate("topic", std::chrono::milliseconds(100)));
    ASSERT_FALSE(server_->publish("topic", testMessage));
    ASSERT_TRUE(server_->availablePartnerTopics().empty());
}

TEST_P(ClientServerShould, serverOpenCloseSequence) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));

    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->isOpen());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
    ASSERT_FALSE(server_->isOpen());

    ASSERT_EQ(server_->serviceName(), GetParam().service_name);

    ASSERT_TRUE(server_->open());
    ASSERT_FALSE(server_->open());
    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(server_->close());
}

TEST_P(ClientServerShould, serverCorrectlyRegistersServicesAndUnregisters) {
    if (GetParam().server_string[0] == "proc://test_proc") return;
    
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    server_->addTopic("testtopic");
    LocalServices services;
    auto activeServices = services.getServices();
    ASSERT_NE(activeServices.find(GetParam().service_name), activeServices.end());

    for (size_t i = 0; i < GetParam().server_string.size(); i++) {
        ASSERT_EQ(activeServices[GetParam().service_name]["connections"][i],
                  GetParam().server_string[i]);
    }

    ASSERT_EQ(activeServices[GetParam().service_name]["topics"][0], "testtopic");
    server_->removeTopic("testtopic");

    activeServices = services.getServices();
    ASSERT_EQ(activeServices[GetParam().service_name]["topics"].size(), 0);

    server_.reset(nullptr);

    activeServices = services.getServices();
    ASSERT_EQ(activeServices.find(GetParam().service_name), activeServices.end());
}

TEST_P(ClientServerShould, clientOpenCloseSequence) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    while (server_->connectedClientsCount() != 1) {
    };
    ASSERT_TRUE(client2_->open());
    while (server_->connectedClientsCount() != 2) {
    };
    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    ASSERT_TRUE(client1_->close());
    while (server_->connectedClientsCount() != 1) {
    };
    ASSERT_TRUE(client2_->close());
    while (server_->connectedClientsCount() != 0) {
    };
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());
    ASSERT_FALSE(client1_->isOpen());

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->isOpen());
    while (server_->connectedClientsCount() != 1) {
    };
    ASSERT_TRUE(client2_->open());
    while (server_->connectedClientsCount() != 2) {
    };
    ASSERT_FALSE(client1_->open());
    ASSERT_FALSE(client2_->open());
    ASSERT_TRUE(client1_->close());
    while (server_->connectedClientsCount() != 1) {
    };
    ASSERT_TRUE(client2_->close());
    while (server_->connectedClientsCount() != 0) {
    };
    ASSERT_FALSE(client1_->close());
    ASSERT_FALSE(client2_->close());
    ASSERT_FALSE(client1_->isOpen());
}

TEST_P(ClientServerShould, serverDoesntOpenIfTransportDoesntOpen) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    Server server2(GetParam().service_name, GetParam().server_string);
    ASSERT_FALSE(server2.open());

    ASSERT_TRUE(server_->close());
}

TEST_P(ClientServerShould, clientDoesntOpenIfTransportDoesntOpen) {
    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_FALSE(client1_->open());
}

TEST_P(ClientServerShould, serverPushesToBothClients) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    for (int i = 0; i < 10; i++) {
        message["id"] = i;
        ASSERT_TRUE(server_->push(message));
    }

    for (int i = 0; i < 10; i++) {
        auto response = client1_->pull();
        ASSERT_TRUE(response);
        ASSERT_EQ(response.value()["hello"], "world!");
        ASSERT_EQ(response.value()["id"], i);
    }

    for (int i = 0; i < 10; i++) {
        auto response = client2_->pull(std::chrono::milliseconds(100));
        ASSERT_TRUE(response);
        ASSERT_EQ(response.value()["hello"], "world!");
        ASSERT_EQ(response.value()["id"], i);
    }

    ASSERT_FALSE(client1_->pull(std::chrono::milliseconds(10)));
    ASSERT_FALSE(client2_->pull(std::chrono::milliseconds(10)));
}

TEST_P(ClientServerShould, serverPullsFromBothClients) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    for (int i = 0; i < 10; i++) {
        ASSERT_TRUE(client1_->push(message));
        ASSERT_TRUE(client2_->push(message));
    }

    for (int i = 0; i < 10; i++) {
        auto response = server_->pull(std::chrono::milliseconds(10));
        ASSERT_TRUE(response);
        ASSERT_EQ(response.value()["hello"], "world!");
        response = server_->pull();
        ASSERT_TRUE(response);
        ASSERT_EQ(response.value()["hello"], "world!");
    }

    ASSERT_FALSE(server_->pull(std::chrono::milliseconds(10)));
}

TEST_P(ClientServerShould, clientPullReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        return client1_->pull();
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(future.get());
}

TEST_P(ClientServerShould, serverPullReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        return server_->pull();
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(future.get());
}

TEST_P(ClientServerShould, serverOnlyAnswersToRequestorClient) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    auto futureResponse = client1_->requestAsync(message);

    auto requestOpt = server_->receiveRequest();
    ASSERT_TRUE(requestOpt);
    auto request = requestOpt.value();
    ASSERT_EQ(request["hello"], "world!");
    request["hello"] = "me!";

    ASSERT_TRUE(server_->respond(request));

    auto response = futureResponse.get();
    ASSERT_EQ(response.size(), 1);
    ASSERT_EQ(response[0]["hello"], "me!");
}

TEST_P(ClientServerShould, correctlyHandleClientDisconnectBeforeResponse) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    auto futureResponse = client1_->requestAsync(message);

    auto requestOpt = server_->receiveRequest();
    ASSERT_TRUE(requestOpt);
    auto request = requestOpt.value();
    ASSERT_EQ(request["hello"], "world!");
    request["hello"] = "me!";

    ASSERT_TRUE(client1_->close());

    while (server_->connectedClientsCount() != 1) {
    };
    ASSERT_FALSE(server_->respond(request));

    auto response = futureResponse.get();
    ASSERT_EQ(response.size(), 0);
}

TEST_P(ClientServerShould, serverGetsResponsesFromAllClients) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    auto futureResponse = server_->requestAsync(message);

    auto requestOpt = client1_->receiveRequest();
    ASSERT_TRUE(requestOpt);
    auto request = requestOpt.value();
    ASSERT_EQ(request["hello"], "world!");
    request["hello"] = "client";
    ASSERT_TRUE(client1_->respond(request));

    requestOpt = client2_->receiveRequest();
    ASSERT_TRUE(requestOpt);
    request = requestOpt.value();
    ASSERT_EQ(request["hello"], "world!");
    request["hello"] = "client";
    ASSERT_TRUE(client2_->respond(request));

    ASSERT_EQ(futureResponse.get().size(), 2);
}

TEST_P(ClientServerShould, serverGetsResponseOnlyFromOneClientIfTheOtherDisconnects) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    auto futureResponse = server_->requestAsync(message);

    auto requestOpt = client1_->receiveRequest();
    ASSERT_TRUE(requestOpt);
    auto request = requestOpt.value();
    ASSERT_EQ(request["hello"], "world!");
    request["hello"] = "client";
    ASSERT_TRUE(client1_->respond(request));

    ASSERT_TRUE(client2_->close());

    ASSERT_EQ(futureResponse.get().size(), 1);
}

TEST_P(ClientServerShould, serverGetsEmptyResponseIfNoConnectedClients) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    auto futureResponse = server_->requestAsync(message);
    ASSERT_EQ(futureResponse.get().size(), 0);
}

TEST_P(ClientServerShould, clientReceiveRequestReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        return client1_->receiveRequest();
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(future.get());
}

TEST_P(ClientServerShould, clientRequestReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        urf::middleware::messages::Message message;
        return client1_->request(message);
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_TRUE(future.get().empty());
}

TEST_P(ClientServerShould, serverReceiveRequestReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        return server_->receiveRequest();
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_FALSE(future.get());
}

TEST_P(ClientServerShould, serverRequestReturnsIfServerCloses) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        urf::middleware::messages::Message message;
        return server_->request(message);
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    ASSERT_TRUE(server_->close());
    ASSERT_TRUE(future.get().empty());
}

TEST_P(ClientServerShould, onlyReceiveResponseWithCorrectRequestId) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    std::atomic<bool> started = false;
    auto future = std::async(std::launch::async, [=, &started]() {
        started = true;
        auto request = server_->receiveRequest(std::chrono::seconds(2));
        if (!request) {
            return false;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(500));
        auto response = request.value();

        response["first"] = "hello";
        server_->respond(response);

        request = server_->receiveRequest(std::chrono::seconds(2));
        if (!request) {
            return false;
        }

        response = request.value();
        response["second"] = "hello";
        server_->respond(response);
        return true;
    });

    if (!started) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
    }

    Message msg;
    msg["request"] = true;

    auto response = client1_->request(msg, std::chrono::milliseconds(100));
    ASSERT_TRUE(response.empty());
    response = client1_->request(msg, std::chrono::seconds(2));
    ASSERT_FALSE(response.empty());
    ASSERT_EQ(response[0]["second"], "hello");

    ASSERT_TRUE(server_->close());
    ASSERT_TRUE(future.get());
}

TEST_P(ClientServerShould, cantRespondIfDidntReceiveFirst) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    urf::middleware::messages::Message message;
    ASSERT_FALSE(client1_->respond(message));
    ASSERT_FALSE(server_->respond(message));
}

TEST_P(ClientServerShould, cantRequestTwiceIfFirstDidntReturn) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    urf::middleware::messages::Message message;

    ASSERT_FALSE(server_->receiveRequest(std::chrono::milliseconds(100)));
    ASSERT_FALSE(client1_->receiveRequest(std::chrono::milliseconds(100)));

    auto futureResponse1 = client1_->requestAsync(message, std::chrono::milliseconds(1000));
    ASSERT_TRUE(server_->receiveRequest(std::chrono::milliseconds(1000)));
    ASSERT_EQ(client1_->request(message).size(), 0);
    ASSERT_TRUE(server_->respond(message));
    ASSERT_EQ(futureResponse1.get().size(), 1);
}

TEST_P(ClientServerShould, clientCantSubscribeToNotExistingTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));
    
    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };

    ASSERT_FALSE(client1_->subscribe("topic"));
    ASSERT_TRUE(client1_->subscribe("topic1"));
    ASSERT_TRUE(client1_->unsubscribe("topic1"));
    ASSERT_TRUE(server_->removeTopic("topic1"));
    ASSERT_FALSE(client1_->subscribe("topic1"));
}

TEST_P(ClientServerShould, clientGetsAvailableTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };
    
    ASSERT_TRUE(client1_->availablePartnerTopics().empty());
    ASSERT_TRUE(server_->addTopic("topic"));
    ASSERT_EQ(client1_->availablePartnerTopics().size(), 1);
}

TEST_P(ClientServerShould, serverGetsAvailableTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };
    ASSERT_TRUE(client1_->addTopic("topic1"));
    ASSERT_TRUE(client2_->addTopic("topic2"));

    ASSERT_EQ(server_->availablePartnerTopics().size(), 2);

    ASSERT_TRUE(client2_->removeTopic("topic2"));
    ASSERT_TRUE(client2_->addTopic("topic1"));

    ASSERT_EQ(server_->availablePartnerTopics().size(), 1);
}

TEST_P(ClientServerShould, serverSubscribesToTopicsAtClientConnect) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    ASSERT_TRUE(server_->subscribe("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));
    ASSERT_TRUE(client1_->addTopic("topic"));
    ASSERT_TRUE(client2_->addTopic("topic"));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    ASSERT_EQ(client1_->subscriptionsCount("topic"), 1);
    ASSERT_EQ(client2_->subscriptionsCount("topic"), 1);

    urf::middleware::messages::Message message;
    message["hello"] = "world!";
    ASSERT_TRUE(client1_->publish("topic", message));

    auto update = server_->receiveUpdate("topic");
    ASSERT_TRUE(update);
    ASSERT_EQ(update.value()["hello"], "world!");
}

TEST_P(ClientServerShould, correctlyReceiveUpdatesFromBothClientsOnSameTopic) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    server_->keepUpdateHistory(true);
    ASSERT_TRUE(server_->subscribe("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));
    ASSERT_TRUE(client1_->addTopic("topic"));
    ASSERT_TRUE(client2_->addTopic("topic"));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };
    urf::middleware::messages::Message message;
    for (int i = 0; i < 10; i++) {
        message["client"] = 1;
        ASSERT_TRUE(client1_->publish("topic", message));
        message["client"] = 2;
        ASSERT_TRUE(client2_->publish("topic", message));
    }

    int received = 0;
    while (server_->receiveUpdate("topic", std::chrono::milliseconds(10))) {
        received++;
    }

    ASSERT_GT(received, 10);
}

TEST_P(ClientServerShould, correctlyReceiveUpdatesFromServer) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    ASSERT_TRUE(client1_->subscribe("topic"));
    ASSERT_TRUE(client2_->subscribe("topic"));

    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic", message));

    ASSERT_TRUE(client1_->receiveUpdate("topic"));
    ASSERT_TRUE(client2_->receiveUpdate("topic", std::chrono::milliseconds(100)));
}

TEST_P(ClientServerShould, correctlyReceiveFromServerUpdateAtTheRequestedFrequency) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() < 2) {
    };

    std::atomic<bool> stop = false;
    auto future = std::async(std::launch::async, [this, &stop]() {
        while (server_->subscriptionsCount("topic") < 2) {
        };
        while (!stop) {
            server_->publish("topic",
                             urf::middleware::messages::Message({{"hello", "world!"}, {"id", 1}}));
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        return true;
    });

    client1_->keepUpdateHistory(true);
    client2_->keepUpdateHistory(true);

    ASSERT_TRUE(client1_->subscribe("topic"));
    ASSERT_TRUE(client2_->subscribe("topic", std::chrono::milliseconds(100)));

    std::this_thread::sleep_for(std::chrono::seconds(1));

    stop = true;
    ASSERT_TRUE(future.get());

    int client1Count = 0, client2Count = 0;
    while (client1_->receiveUpdate("topic", std::chrono::milliseconds(1))) {
        client1Count++;
    }

    while (client2_->receiveUpdate("topic", std::chrono::milliseconds(1))) {
        client2Count++;
    }

    ASSERT_GT(client1Count, client2Count * 2);
}

TEST_P(ClientServerShould, updateReceiveFrequency) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() < 1) {
        std::this_thread::sleep_for(std::chrono::milliseconds(5));
    };

    std::atomic<bool> stop = false;
    auto future = std::async(std::launch::async, [this, &stop]() {
        while (server_->subscriptionsCount("topic") < 1) {
        };
        while (!stop) {
            server_->publish("topic",
                             urf::middleware::messages::Message({{"hello", "world!"}, {"id", 1}}));
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        return true;
    });

    client1_->keepUpdateHistory(true);

    ASSERT_TRUE(client1_->subscribe("topic", std::chrono::milliseconds(100)));

    std::this_thread::sleep_for(std::chrono::seconds(1));

    stop = true;
    ASSERT_TRUE(future.get());

    int client1Count = 0;
    while (client1_->receiveUpdate("topic", std::chrono::milliseconds(1))) {
        client1Count++;
    }

    stop = false;
    future = std::async(std::launch::async, [this, &stop]() {
        while (server_->subscriptionsCount("topic") < 1) {
        };
        while (!stop) {
            server_->publish("topic",
                             urf::middleware::messages::Message({{"hello", "world!"}, {"id", 1}}));
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
        return true;
    });

    ASSERT_TRUE(client1_->subscribe("topic", std::chrono::milliseconds(10)));

    std::this_thread::sleep_for(std::chrono::seconds(1));

    stop = true;
    ASSERT_TRUE(future.get());
    int client2Count = 0;
    while (client1_->receiveUpdate("topic", std::chrono::milliseconds(1))) {
        client2Count++;
    }
    ASSERT_LT(client1Count * 5, client2Count);
}

TEST_P(ClientServerShould, DISABLED_correctlySynchronizeTimestamp) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->subscribe("topic1"));

    ASSERT_TRUE(server_->publish("topic1", Message({{"asd", "asd"}, {"id", 1}})));

    auto update = client1_->receiveUpdate("topic1");
    ASSERT_TRUE(update);
    ASSERT_NEAR(update.value().header().delay(), 0, 1);
}

TEST_P(ClientServerShould, onlyCorrectTopicReceivesUpdates) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    ASSERT_TRUE(client1_->subscribe("topic"));
    ASSERT_TRUE(client2_->subscribe("topic"));

    ASSERT_EQ(server_->subscriptionsCount("topic"), 2);

    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic", message));

    ASSERT_TRUE(client1_->receiveUpdate("topic"));
    ASSERT_FALSE(client2_->receiveUpdate("topic2", std::chrono::milliseconds(100)));
}

TEST_P(ClientServerShould, clientDoesntReceivesUpdatesIfNotSubscribed) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    ASSERT_TRUE(client1_->subscribe("topic"));

    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic", message));

    ASSERT_TRUE(client1_->receiveUpdate("topic"));
    ASSERT_FALSE(client2_->receiveUpdate("topic", std::chrono::milliseconds(100)));
}

TEST_P(ClientServerShould, onPushCallback) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());
    while (server_->connectedClientsCount() != 1) {
    };

    std::atomic<bool> called = false;
    client1_->onPush([&called](const urf::middleware::messages::Message&) { called = true; });
    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->push(message));

    while (!called) {
    }
    ASSERT_TRUE(called);
}


TEST_P(ClientServerShould, onClientsPushCallback) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());

    client2_.reset(new Client(GetParam().client2_string));
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    std::atomic<bool> called = false;
    server_->onPush([&called](const urf::middleware::messages::Message&) { called = true; });
    urf::middleware::messages::Message message;
    ASSERT_TRUE(client1_->push(message));

    while (!called) {
    }
    ASSERT_TRUE(called);

    called = false;
    ASSERT_TRUE(client2_->push(message));
    while (!called) {
    }
    ASSERT_TRUE(called);
}

TEST_P(ClientServerShould, correctlyCallBackOnTopic) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
    };

    ASSERT_TRUE(client1_->subscribe("topic"));
    std::atomic<bool> called = false;
    client1_->onUpdate(
        "topic", [&called](const std::string& topic, const urf::middleware::messages::Message&) {
            ASSERT_EQ(topic, "topic");
            called = true;
        });
    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic", message));

    while (!called) {
    }
    ASSERT_TRUE(called);
}

TEST_P(ClientServerShould, correctlyCallBackOnTopicServerSide) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->addTopic("topic"));
    ASSERT_TRUE(client1_->open());

    client2_.reset(new Client(GetParam().client2_string));
    ASSERT_TRUE(client2_->addTopic("topic"));
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
    };

    ASSERT_TRUE(server_->subscribe("topic"));
    std::atomic<bool> called = false;
    server_->onUpdate(
        "topic", [&called](const std::string& topic, const urf::middleware::messages::Message&) {
            ASSERT_EQ(topic, "topic");
            called = true;
        });
    urf::middleware::messages::Message message;
    ASSERT_TRUE(client1_->publish("topic", message));

    while (!called) {
    }
    ASSERT_TRUE(called);
    called = false;
    ASSERT_TRUE(client2_->publish("topic", message));
    while (!called) {
    }
    ASSERT_TRUE(called);
}

TEST_P(ClientServerShould, clientCorrectlySubscribeToAllTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));
    ASSERT_TRUE(server_->addTopic("topic2"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
    };

    ASSERT_TRUE(client1_->subscribe("*"));
    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic1", message));
    ASSERT_TRUE(client1_->receiveUpdate("topic1"));

    ASSERT_TRUE(server_->publish("topic2", message));
    ASSERT_TRUE(client1_->receiveUpdate("topic2"));

    ASSERT_TRUE(client1_->unsubscribe("topic1"));
    ASSERT_TRUE(server_->publish("topic1", message));
    ASSERT_FALSE(client1_->receiveUpdate("topic1", std::chrono::milliseconds(100)));

    ASSERT_TRUE(client1_->unsubscribe("*"));
    ASSERT_TRUE(server_->publish("topic2", message));
    ASSERT_FALSE(client1_->receiveUpdate("topic2", std::chrono::milliseconds(100)));
}

TEST_P(ClientServerShould, receiveUpdatesAlsoFromNewTopicsWhenSubscribedToAll) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));
    ASSERT_TRUE(server_->addTopic("topic2"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
    };

    ASSERT_TRUE(client1_->subscribe("*"));
    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic1", message));
    ASSERT_TRUE(client1_->receiveUpdate("topic1"));

    ASSERT_TRUE(server_->publish("topic2", message));
    ASSERT_TRUE(client1_->receiveUpdate("topic2"));

    ASSERT_TRUE(server_->addTopic("topic3"));
    ASSERT_TRUE(server_->publish("topic3", message));
    ASSERT_TRUE(client1_->receiveUpdate("topic3"));
}

TEST_P(ClientServerShould, cantReceiveFromAllTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));
    ASSERT_TRUE(server_->addTopic("topic2"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
    };

    ASSERT_TRUE(client1_->subscribe("*"));
    urf::middleware::messages::Message message;
    ASSERT_TRUE(server_->publish("topic1", message));
    ASSERT_FALSE(client1_->receiveUpdate("*"));
    ASSERT_FALSE(client1_->receiveUpdate("*", std::chrono::milliseconds(100)));

    ASSERT_FALSE(server_->receiveUpdate("*"));
    ASSERT_FALSE(server_->receiveUpdate("*", std::chrono::milliseconds(100)));
    ASSERT_TRUE(client1_->receiveUpdate("topic1"));
}

TEST_P(ClientServerShould, serverReceivesFromAllTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    server_->keepUpdateHistory(true);
    ASSERT_TRUE(server_->subscribe("*"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));
    ASSERT_TRUE(client1_->addTopic("topic1"));
    ASSERT_TRUE(client2_->addTopic("topic2"));

    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());

    while (server_->connectedClientsCount() != 2) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    };
    urf::middleware::messages::Message message;
    for (int i = 0; i < 10; i++) {
        message["client"] = 1;
        ASSERT_TRUE(client1_->publish("topic1", message));
        message["client"] = 2;
        ASSERT_TRUE(client2_->publish("topic2", message));
    }

    int received1 = 0;
    while (server_->receiveUpdate("topic1", std::chrono::milliseconds(10))) {
        received1++;
    }

    int received2 = 0;
    while (server_->receiveUpdate("topic2", std::chrono::milliseconds(10))) {
        received2++;
    }

    ASSERT_GT(received1, 5);
    ASSERT_GT(received2, 5);

    ASSERT_TRUE(server_->unsubscribe("topic1"));

    for (int i = 0; i < 10; i++) {
        message["client"] = 1;
        ASSERT_TRUE(client1_->publish("topic1", message));
        message["client"] = 2;
        ASSERT_TRUE(client2_->publish("topic2", message));
    }

    received1 = 0;
    while (server_->receiveUpdate("topic1", std::chrono::milliseconds(10))) {
        received1++;
    }

    received2 = 0;
    while (server_->receiveUpdate("topic2", std::chrono::milliseconds(10))) {
        received2++;
    }

    ASSERT_EQ(received1, 0);
    ASSERT_GT(received2, 5);

    ASSERT_TRUE(server_->unsubscribe("*"));

    for (int i = 0; i < 10; i++) {
        message["client"] = 1;
        ASSERT_TRUE(client1_->publish("topic1", message));
        message["client"] = 2;
        ASSERT_TRUE(client2_->publish("topic2", message));
    }

    received1 = 0;
    while (server_->receiveUpdate("topic1", std::chrono::milliseconds(10))) {
        received1++;
    }

    received2 = 0;
    while (server_->receiveUpdate("topic2", std::chrono::milliseconds(10))) {
        received2++;
    }

    ASSERT_EQ(received1, 0);
    ASSERT_EQ(received2, 0);
}

TEST_P(ClientServerShould, clientOnUpdateHandlerCalledOnAllTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));
    ASSERT_TRUE(server_->addTopic("topic2"));

    client1_.reset(new Client(GetParam().client1_string));

    ASSERT_TRUE(client1_->open());

    while (server_->connectedClientsCount() != 1) {
    };

    ASSERT_TRUE(client1_->subscribe("*"));
    std::atomic<int> called = 0;
    client1_->onUpdate(
        "*", [&called](const std::string&, const urf::middleware::messages::Message& msg) {
            auto localCopy = msg;
            called += localCopy["id"].get<int>();
        });

    urf::middleware::messages::Message message;
    message["id"] = 1;
    ASSERT_TRUE(server_->publish("topic1", message));
    message["id"] = 2;
    ASSERT_TRUE(server_->publish("topic2", message));

    while (called < 3) {
    }
    ASSERT_EQ(called, 3);
}

TEST_P(ClientServerShould, registerServiceMetadata) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));

    nlohmann::json metadata;
    metadata["hello"] = "world!";

    server_->serviceMetadata(metadata);

    LocalServices services;
    auto activeServices = services.getServices();
    
    ASSERT_EQ(activeServices[GetParam().service_name]["metadata"]["hello"], "world!");
    ASSERT_EQ(server_->serviceMetadata()["hello"], "world!");
}

TEST_P(ClientServerShould, serverOnUpdateHandlerCalledOnAllTopics) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->subscribe("*"));

    client1_.reset(new Client(GetParam().client1_string));
    client2_.reset(new Client(GetParam().client2_string));

    ASSERT_TRUE(client1_->addTopic("topic1"));
    ASSERT_TRUE(client2_->addTopic("topic2"));
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client2_->open());
    while (server_->connectedClientsCount() != 2) {
    };

    std::atomic<int> called = 0;
    server_->onUpdate("*",
                      [&called](const std::string&, const urf::middleware::messages::Message& msg) {
                          auto localCopy = msg;
                          called += localCopy["id"].get<int>();
                      });

    urf::middleware::messages::Message message;
    message["id"] = 1;
    ASSERT_TRUE(client1_->publish("topic1", message));
    message["id"] = 2;
    ASSERT_TRUE(client2_->publish("topic2", message));

    while (called < 3) {
    }
    ASSERT_EQ(called, 3);
}

TEST_P(ClientServerShould, patternsCorrectlyWorkWithoutAutodeserializeBody) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->automaticallyDeserializeBody(false));
    ASSERT_TRUE(server_->addTopic("topic1"));

    client1_.reset(new Client(GetParam().client1_string));
    ASSERT_TRUE(client1_->open());
    ASSERT_TRUE(client1_->automaticallyDeserializeBody(false));
    ASSERT_TRUE(client1_->addTopic("topic2"));

    while (server_->connectedClientsCount() != 1) {
    };

    Message message;
    message["hello"] = "world!";

    ASSERT_TRUE(server_->push(message));
    auto pull = client1_->pull();
    ASSERT_TRUE(pull);
    ASSERT_TRUE(pull.value().body().deserialize());
    ASSERT_EQ(pull.value()["hello"], "world!");

    ASSERT_TRUE(client1_->push(message));
    pull = server_->pull();
    ASSERT_TRUE(pull);
    ASSERT_TRUE(pull.value().body().deserialize());
    ASSERT_EQ(pull.value()["hello"], "world!");

    auto future = server_->requestAsync(message, std::chrono::milliseconds(100));
    auto request = client1_->receiveRequest();
    ASSERT_TRUE(request);
    ASSERT_TRUE(client1_->respond(message));
    ASSERT_EQ(future.get().size(), 1);

    future = client1_->requestAsync(message, std::chrono::milliseconds(100));
    request = server_->receiveRequest();
    ASSERT_TRUE(request);
    ASSERT_TRUE(server_->respond(message));
    ASSERT_EQ(future.get().size(), 1);

    ASSERT_TRUE(server_->subscribe("topic2"));
    ASSERT_TRUE(server_->unsubscribe("topic2"));

    ASSERT_FALSE(client1_->subscribe("topic"));
    ASSERT_TRUE(client1_->subscribe("topic1"));
    ASSERT_TRUE(client1_->unsubscribe("topic1"));

    ASSERT_EQ(server_->availablePartnerTopics().size(), 1);
    ASSERT_EQ(client1_->availablePartnerTopics().size(), 1);
}

TEST_P(ClientServerShould, DISABLED_serverScalabilityTest) {
    server_.reset(new Server(GetParam().service_name, GetParam().server_string));
    ASSERT_TRUE(server_->open());
    ASSERT_TRUE(server_->addTopic("topic1"));

    std::vector<std::shared_ptr<Client>> clients_;
    for (int i = 0; i < 10; i++) {
        uint16_t clientsSize = 10;
        for (int i = 0; i < clientsSize; i++) {
            auto client = std::make_shared<Client>(GetParam().client1_string);
            ASSERT_TRUE(client->open());
            clients_.push_back(client);
        }

        std::cout << "Waiting to get 10 more clients" << std::endl;
        while (server_->connectedClientsCount() != clients_.size()) {
        };

        Message message;
        std::vector<uint8_t> bigVector;
        bigVector.resize(10000000);
        message["data"] = nlohmann::json::binary(bigVector);

        std::cout << "Let's push" << std::endl;
        auto start = std::chrono::high_resolution_clock::now();
        server_->push(message);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Push elapsed with " << clients_.size() << " clients: "
                  << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
                  << std::endl;
    }
}
