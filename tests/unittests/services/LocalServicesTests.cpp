#include <chrono>
#include <iostream>
#include <thread>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <urf/middleware/services/LocalServices.hpp>

using urf::middleware::services::LocalServices;

TEST(LocalServicesShould, correctlyRegisterService) {
    LocalServices obj1, obj2;
    ASSERT_TRUE(obj1.registerService("service1", {{"name", "service1"}, {"attempts", 1}}));

    ASSERT_EQ(obj2.getServices()["service1"]["name"], "service1");
    ASSERT_EQ(obj2.getServices()["service1"]["attempts"], 1);

    ASSERT_NE(obj2.getServices()["service1"]["lastUpdate"], 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    auto lastUpdate = obj2.getServices()["service1"]["lastUpdate"];

    ASSERT_TRUE(obj1.refreshService("service1"));
    ASSERT_NE(obj2.getServices()["service1"]["lastUpdate"], lastUpdate);
}