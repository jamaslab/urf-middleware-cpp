
#pragma once

#include <gmock/gmock.h>

#include "common/sockets/ITransportSocket.hpp"

namespace urf {
namespace middleware {
namespace sockets {

class TransportSocketMock : public ITransportSocket {
 public:
    MOCK_METHOD0(open, bool());
    MOCK_METHOD0(close, bool());
    MOCK_METHOD0(isOpen, bool());
    MOCK_METHOD0(allowsFragmentedRead, bool());
    MOCK_METHOD2(write, bool(const std::vector<uint8_t>&, bool));
    MOCK_METHOD1(read, std::vector<uint8_t>(int length));
    MOCK_METHOD2(read, std::vector<uint8_t>(int length, const std::chrono::milliseconds& timeout));
    MOCK_METHOD1(onConnectionLost, bool(const std::function<void(ITransportSocket*)>& handler));
};

} // namespace sockets
} // namespace middleware
} // namespace urf
