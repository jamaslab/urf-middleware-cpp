import os
from conans import ConanFile, CMake, tools

def get_file(filename):
    f = open(filename, "r")
    return f.read()

class UrfMiddlewareCppConan(ConanFile):
    _windows_import_paths = ["../Windows/{cf.settings.build_type}/bin/{cf.settings.build_type}"]

    name = "urf_middleware_cpp"
    version = "0.10.0"
    license = "MIT"
    author = "Giacomo Lunghi"
    url = "https://github.com/Jamaslab/urf_middleware_cpp"
    description = "Unified Robotic Framework Middleware"
    short_paths = True
    settings = "os", "compiler", "build_type", "arch"
    options = {
        "shared": [True, False],
        "msquic": [True, False],
        "build_tests": [True, False],
        "build_benchmark": [True, False],
        "build_it": [True, False]
    }
    import_paths = []
    default_options = {"shared": False, "msquic": False, "build_tests": False, "build_benchmark": False, "build_it": False}
    requires = ("nlohmann_json/3.9.1", "cn-cbor/1.0.0", "urf_common_cpp/1.10.0@uji-cirtesu-irslab+urobf+urf-common-cpp/stable")
    build_requires = ("cmake/3.25.0")
    generators = "cmake", "cmake_find_package", "virtualenv"
    exports_sources = ["environment/*", "src/*", "tests/*", "CMakeLists.txt", "LICENSE", "README.md"]

    def configure(self):
        if tools.os_info.is_linux and self.settings.compiler.libcxx == "libstdc++":
            raise Exception("This package is only compatible with libstdc++11. Run conan profile update settings.compiler.libcxx=libstdc++11 default")

    @property
    def default_user(self):
        return "uji-cirtesu-irslab+urobf+urf-middleware-cpp"

    @property
    def default_channel(self):
        return "stable"

    def requirements(self):
        if self.options.msquic:
            self.requires("msquic/2.0.3@uji-cirtesu-irslab+urobf+urf-externals/stable")

    def build_requirements(self):
        if self.options.build_tests:
            self.build_requires("gtest/1.10.0")

            if self.options.build_benchmark:
                self.build_requires("benchmark/1.6.0")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = os.path.join(self.recipe_folder, 'package/')
        cmake.definitions["CMAKE_MODULE_PATH"] = self.install_folder.replace("\\", "/")
        cmake.definitions["CMAKE_BUILD_TYPE"] = self.settings.build_type
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.shared

        cmake.definitions["WITH_MSQUIC"] = self.options.msquic
        cmake.definitions["BUILD_BENCHMARK"] = self.options.build_benchmark
        cmake.definitions["BUILD_IT"] = self.options.build_it

        if not self.options.shared:
            cmake.definitions['CMAKE_POSITION_INDEPENDENT_CODE'] = True

        cmake.parallel = False
        cmake.configure(
            build_folder='build/%s' % str(self.settings.os) + '/' + str(self.settings.build_type))
        cmake.build()
        if self.options.build_tests:
            cmake.test(output_on_failure=True)
            
        cmake.install()


    def imports(self):
        if tools.os_info.is_windows:
            import_paths = getattr(self, 'import_paths') + self._windows_import_paths
            for ipath in import_paths:
                self.copy("*.dll", str(ipath.format(cf=self)), "bin")
                self.copy("*.dll", str(ipath.format(cf=self)), "lib")
                self.copy("*.dylib", str(ipath.format(cf=self)), "lib")

    def package(self):
        self.copy("*.hpp", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        self.copy("*.h", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        if tools.os_info.is_linux:
            self.copy("*.so", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy("*.a", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
        elif tools.os_info.is_windows:
            self.copy("*.dll", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)
            self.copy("*.lib", dst=".", src=os.path.join(self.recipe_folder, 'package/'), keep_path=True)

    def package_info(self):
        self.cpp_info.libs = ['urf_middleware']
        self.cpp_info.includedirs = ['include/']
        self.cpp_info.libdirs = ['lib']

        self.env_info.PATH.append(os.path.join(self.package_folder, "lib"))
